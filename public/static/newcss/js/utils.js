var Utils = {
  //初始化根元素fontsize大小，用于rem适配不同手机屏幕
  initRootFontSize: function () {
    var docEl = document.documentElement;
    var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
    var recalc = function () {
      var clientWidth = docEl.clientWidth;
      if (!clientWidth) return;
      docEl.style.fontSize = 62.5 * (clientWidth / 375) + '%';
    };
    if (!document.addEventListener) return;
    window.addEventListener(resizeEvt, recalc, false);
    document.addEventListener('DOMContentLoaded', recalc, false);
  },
  showToast: function (msg) {
    var toast = document.createElement('div');
    toast.className = "toast";
    toast.innerHTML = msg;
    toast.id = "toast";
    document.body.appendChild(toast);
    toast.style.opacity = 1;
    setTimeout(function () {
      document.body.removeChild(document.getElementById('toast'));
    }, 1000)
  },
  showLoading: function () {
    var loading = document.createElement('div');
    loading.className = "loading-wrapper";
    loading.id = 'loading';
    loading.innerHTML = "<div class='uil-default-css' style='transform:scale(0.25);'><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(0deg) translate(0,-80px);transform:rotate(0deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(30deg) translate(0,-80px);transform:rotate(30deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(60deg) translate(0,-80px);transform:rotate(60deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(90deg) translate(0,-80px);transform:rotate(90deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(120deg) translate(0,-80px);transform:rotate(120deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(150deg) translate(0,-80px);transform:rotate(150deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(180deg) translate(0,-80px);transform:rotate(180deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(210deg) translate(0,-80px);transform:rotate(210deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(240deg) translate(0,-80px);transform:rotate(240deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(270deg) translate(0,-80px);transform:rotate(270deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(300deg) translate(0,-80px);transform:rotate(300deg) translate(0,-80px);border-radius:16px;position:absolute;'></div><div style='top:84px;left:84px;width:32px;height:32px;background:#000000;-webkit-transform:rotate(330deg) translate(0,-80px);transform:rotate(330deg) translate(0,-80px);border-radius:16px;position:absolute;'></div></div>";
    document.body.appendChild(loading);
  },
  hideLoading: function () {
    if (document.getElementById('loading')) {
      document.body.removeChild(document.getElementById('loading'));
    }
  },
  getParameter: function (name) {
    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1])
  },
    getSelected: function (id) {
        if (id === undefined) {
            console.error('请输入id')
            return undefined;
        }
        var rId = id;
        if (rId.indexOf('#') === 0) {
            rId = rId.substring(1);
        }
        var selectEl = $('#' + rId).parent().find('.cs-options>ul>.cs-selected')[0];
        var defaultSelected = $('#' + rId).find('option[selected]');
        if (selectEl) {
            return $(selectEl).data('value');
        } else if(defaultSelected){
            // 获取未操作时候的默认值
            return defaultSelected.attr('value');
        }
        console.error('不存在的id', id);
        return undefined;
    }
};

!function (e) {
  "use strict";

  function t(e) {
    return new RegExp("(^|\\s+)" + e + "(\\s+|$)")
  }

  function s(e, t) {
    var s = l(e, t) ? i : n;
    s(e, t)
  }

  var l, n, i;
  "classList" in document.documentElement ? (l = function (e, t) {
    return e.classList.contains(t)
  }, n = function (e, t) {
    e.classList.add(t)
  }, i = function (e, t) {
    e.classList.remove(t)
  }) : (l = function (e, s) {
    return t(s).test(e.className)
  }, n = function (e, t) {
    l(e, t) || (e.className = e.className + " " + t)
  }, i = function (e, s) {
    e.className = e.className.replace(t(s), " ")
  });
  var c = {
    hasClass: l,
    addClass: n,
    removeClass: i,
    toggleClass: s,
    has: l,
    add: n,
    remove: i,
    toggle: s
  };
  "function" == typeof define && define.amd ? define(c) : e.classie = c
}(window),
    function (e) {
      "use strict";

      function t(e, t) {
        if (!e) return !1;
        for (var s = e.target || e.srcElement || e || !1; s && s != t;) s = s.parentNode || !1;
        return s !== !1
      }

      function s(e, t) {
        for (var s in t) t.hasOwnProperty(s) && (e[s] = t[s]);
        return e
      }

      function l(e, t) {
        this.el = e, this.options = s({}, this.options), s(this.options, t), this._init()
      }

      l.prototype.options = {
        newTab: !0,
        stickyPlaceholder: !0,
        onChange: function () {
          return !1
        }
      }, l.prototype._init = function () {
        var e = this.el.querySelector("option[selected]");
        this.hasDefaultPlaceholder = e && e.disabled, this.selectedOpt = e || this.el.querySelector("option"), this._createSelectEl(), this.selOpts = [].slice.call(this.selEl.querySelectorAll("li[data-option]")), this.selOptsCount = this.selOpts.length, this.current = this.selOpts.indexOf(this.selEl.querySelector("li.cs-selected")) || -1, this.selPlaceholder = this.selEl.querySelector("span.cs-placeholder"), this._initEvents()
      }, l.prototype._createSelectEl = function () {
        var e = "",
            t = function (e) {
              var t = "",
                  s = "",
                  l = "";
              return !e.selectedOpt || this.foundSelected || this.hasDefaultPlaceholder || (s += "cs-selected ", this.foundSelected = !0), e.getAttribute("data-class") && (s += e.getAttribute("data-class")), e.getAttribute("data-link") && (l = "data-link=" + e.getAttribute("data-link")), "" !== s && (t = 'class="' + s + '" '), "<li " + t + l + ' data-option data-value="' + e.value + '"><span>' + e.textContent + "</span></li>"
            };
        [].slice.call(this.el.children).forEach(function (s) {
          if (!s.disabled) {
            var l = s.tagName.toLowerCase();
            "option" === l ? e += t(s) : "optgroup" === l && (e += '<li class="cs-optgroup"><span>' + s.label + "</span><ul>", [].slice.call(s.children).forEach(function (s) {
              e += t(s)
            }), e += "</ul></li>")
          }
        });
        var s = '<div class="cs-options"><ul>' + e + "</ul></div>";
        this.selEl = document.createElement("div"), this.selEl.className = this.el.className, this.selEl.tabIndex = this.el.tabIndex, this.selEl.innerHTML = '<span class="cs-placeholder">' + this.selectedOpt.textContent + "</span>" + s, this.el.parentNode.appendChild(this.selEl), this.selEl.appendChild(this.el)
      }, l.prototype._initEvents = function () {
        var e = this;
        this.selPlaceholder.addEventListener("click", function () {
          e._toggleSelect()
        }), this.selOpts.forEach(function (t, s) {
          t.addEventListener("click", function () {
            e.current = s, e._changeOption(), e._toggleSelect()
          })
        }), document.addEventListener("click", function (s) {
          var l = s.target;
          e._isOpen() && l !== e.selEl && !t(l, e.selEl) && e._toggleSelect()
        }), this.selEl.addEventListener("keydown", function (t) {
          var s = t.keyCode || t.which;
          switch (s) {
            case 38:
              t.preventDefault(), e._navigateOpts("prev");
              break;
            case 40:
              t.preventDefault(), e._navigateOpts("next");
              break;
            case 32:
              t.preventDefault(), e._isOpen() && "undefined" != typeof e.preSelCurrent && -1 !== e.preSelCurrent && e._changeOption(), e._toggleSelect();
              break;
            case 13:
              t.preventDefault(), e._isOpen() && "undefined" != typeof e.preSelCurrent && -1 !== e.preSelCurrent && (e._changeOption(), e._toggleSelect());
              break;
            case 27:
              t.preventDefault(), e._isOpen() && e._toggleSelect()
          }
        })
      }, l.prototype._navigateOpts = function (e) {
        this._isOpen() || this._toggleSelect();
        var t = "undefined" != typeof this.preSelCurrent && -1 !== this.preSelCurrent ? this.preSelCurrent : this.current;
        ("prev" === e && t > 0 || "next" === e && t < this.selOptsCount - 1) && (this.preSelCurrent = "next" === e ? t + 1 : t - 1, this._removeFocus(), classie.add(this.selOpts[this.preSelCurrent], "cs-focus"))
      }, l.prototype._toggleSelect = function () {
        this._removeFocus(), this._isOpen() ? (-1 !== this.current && (this.selPlaceholder.textContent = this.selOpts[this.current].textContent), classie.remove(this.selEl, "cs-active")) : (this.hasDefaultPlaceholder && this.options.stickyPlaceholder && (this.selPlaceholder.textContent = this.selectedOpt.textContent), classie.add(this.selEl, "cs-active"))
      }, l.prototype._changeOption = function () {
        "undefined" != typeof this.preSelCurrent && -1 !== this.preSelCurrent && (this.current = this.preSelCurrent, this.preSelCurrent = -1);
        var t = this.selOpts[this.current];
        this.selPlaceholder.textContent = t.textContent, this.el.value = t.getAttribute("data-value");
        var s = this.selEl.querySelector("li.cs-selected");
        s && classie.remove(s, "cs-selected"), classie.add(t, "cs-selected"), t.getAttribute("data-link") && (this.options.newTab ? e.open(t.getAttribute("data-link"), "_blank") : e.location = t.getAttribute("data-link")), this.options.onChange(this.el.value)
      }, l.prototype._isOpen = function () {
        return classie.has(this.selEl, "cs-active")
      }, l.prototype._removeFocus = function () {
        var e = this.selEl.querySelector("li.cs-focus");
        e && classie.remove(e, "cs-focus")
      }, e.SelectFx = l
    }(window),
    function () {
      [].slice.call(document.querySelectorAll("select.cs-select")).forEach(function (e) {
        new SelectFx(e)
      })
    }();

// textarea
$('textarea.textarea').bind('input propertychange', function () {
  if ($(this).val().length > 500) {
    $('.desc-count').html('<span class="font-red">' + $(this).val().length + '</span>/200');
    return;
  }
  $('.desc-count').html($(this).val().length + ' /500');
});

$('.title-bar .back-wrap').on('click', function () {
  history.back();
});

/** filters start **/
// 选择筛选项
$('.tab-bar').on('click', '.tab-item', function () {
  // 兄弟节点active状态取消
  $(this).siblings('.tab-item').forEach(function (item) {
    $(item).removeClass('active')
    $(item).find('i').removeClass('icon-triangle-up');
    $(item).find('i').addClass('icon-triangle-down');
  });
  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
    $(this).find('i').removeClass('icon-triangle-up');
    $(this).find('i').addClass('icon-triangle-down');
    $('.tab-con').hide();
  } else {
    $(this).addClass('active');
    $(this).find('i').addClass('icon-triangle-up');
    $('.tab-con').hide();
    $('.' + $(this).data('bar') + '.tab-con').css('display', 'block');
  }
})
// 工作城市——选择省份
$('.province').on('click', 'li', function () {
  $(this).siblings('li').forEach(function (item) {
    $(item).removeClass('active')
  });
  $(this).addClass('active')
  $('.countdown').hide();
  $('.' + $(this).data('province') + '.countdown').css('display', 'block')
})
// 工作城市——选择市
$('.countdown').on('click', 'li', function () {
  $(this).siblings('li').forEach(function (item) {
    $(item).removeClass('active')
  });
  $(this).addClass('active');
  //tab-bar重置
  initTabBar('city');
  hideTabContent('city');
  // todo 发送请求
  console.log($(this).data('province'), $(this).data('city'));
  var province  = $(this).data('province');
  var city = $(this).data('city');
})
// 选择工作经验
$('.tab-con__experience').on('click', '.btn', function () {
  $(this).siblings('.btn').forEach(function (item) {
    $(item).removeClass('active');
  });
  $(this).addClass('active');
  initTabBar('experience');
  hideTabContent('experience');
  // todo 发送请求
  console.log('experience', $(this).data('experience'));
  var experience = $(this).data('experience');

})
// 选择薪资待遇
$('.tab-con__salary').on('click', '.btn', function () {
  $(this).siblings('.btn').forEach(function (item) {
    $(item).removeClass('active');
  });
  $(this).addClass('active');
  initTabBar('salary');
  hideTabContent('salary');
  // todo 发送请求
  console.log('salary', $(this).data('salary'));
  var salary = $(this).data('salary');
})
// 选择学历
$('.tab-con__education').on('click', '.btn', function () {
  $(this).siblings('.btn').forEach(function (item) {
    $(item).removeClass('active');
  });
  $(this).addClass('active');
  initTabBar('education');
  hideTabContent('education');
  // todo 发送请求
  console.log('education', $(this).data('education'));
  var education = $(this).data('education');
})

// tab-bar重置
function initTabBar(itemClass) {
  var tabItem = $('.' + itemClass + '.tab-item');
  tabItem.removeClass('active');
  tabItem.find('i').removeClass('icon-triangle-up');
  tabItem.find('i').addClass('icon-triangle-down');
}

// 隐藏tab-content
function hideTabContent(contentClass) {
  setTimeout(function () {
    $('.' + contentClass + '.tab-con').css('display', 'none');
  }, 500);
}

// 点击筛选项蒙版——隐藏content，tab-bar重置
$('.tab-con').on('click', function (e) {
  if ($(e.target).hasClass('tab-con')) {
    $(this).css('display', 'none');
    initTabBar('experience');
    initTabBar('city');
    initTabBar('salary');
    initTabBar('education');
  }
})

/** filters end **/

/** job tab start **/
$('.job-tabbar').on('click', '.job-item', function () {
  // 兄弟节点active状态取消
  $(this).siblings('.job-item').forEach(function (item) {
    $(item).removeClass('active')
  });
  $(this).addClass('active');
  $('.job-tabcon').hide();
  $('.' + $(this).data('bar') + '.job-tabcon').css('display', 'block');
})
/** job tab end **/