
//初始化layui插件
if ( typeof layui != "undefined") {
    layui.use(['element','form','layer'], function(){
        var layer = layui.layer;
    });
    /*;!function(){
        var layer = layui.layer;
        var element = layui.element;
    }();*/
}

//iframe自适应高度
function reinitIframe(){
    var iframe = document.getElementById("main_box");
    try{
        var bHeight = iframe.contentWindow.document.body.scrollHeight;
        var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
        iframe.height = Math.max(bHeight, dHeight);
        iframe.width = '100%';
    }catch (ex){}
}
window.setInterval("reinitIframe()", 200);

//分页设置
function pageFun() {
    //样式
    var pagination = $('.pagination');
    pagination.find('.active').addClass('layui-laypage-em layui-disabled');
    pagination.find('li').addClass('pagelis');
    pagination.find('li:first').addClass('layui-laypage-prev page_first_last').find('span').html('上一页');
    pagination.find('li:first').find('a').html('上一页');
    pagination.find('li:last').addClass('layui-laypage-prev page_first_last page_last_right').find('a').html('下一页');
    pagination.find('li:last').find('span').html('下一页');
}
//最大化打开新窗口
function openPage(url){
    layer.full(layer.open({
        type: 2 //此处以iframe举例
        ,title: '添加角色'
        ,area: ['500px', '300px']
        ,shade: 0
        ,content: url
        ,maxmin: true
        ,yes: function(){
            $(that).click(); 
        }
        ,zIndex: layer.zIndex //重点1
        ,success: function(layero){
            layer.setTop(layero); //重点2
        }
    }));
}

// 验证手机号
function isPhone(phone) {
    var pattern = /^(((13[0-9]{1})|(14[0-9]{1})|(17[0]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/;
    return pattern.test(phone);
}
//刷新验证码
function changeCode(){
    var verifyimg = $(".verifyimg").attr("src");
    if( verifyimg.indexOf('?')>0){
        $(".verifyimg").attr("src", verifyimg+'&random='+Math.random());
    }else{
        $(".verifyimg").attr("src", verifyimg.replace(/\?.*$/,'')+'?'+Math.random());
    }
}
//开始倒计时
function startDownTime(num){
    if (num > 0) {
        num--;
        $('#code_btn').html(num+'秒后再发送').attr('disabled',true).removeClass('layui-btn').addClass('layui-btn-disabled');
        setTimeout("startDownTime("+num+")",1000);
    } else {
        $('#code_btn').html('获取验证码').attr('disabled',false).addClass('layui-btn').removeClass('layui-btn-disabled');
    }
}

