//$(function(){
    var ie6_pos_reset = function(dom){
        var _scrollHeight = $(document).scrollTop();
        var _windowHeight = $(window).height();
        var _windowWidth = $(window).width();
        var _h = dom.height();
        var _w = dom.width();
        dom.css({
            left:(_windowWidth - _w)/2,
            top:(_windowHeight - _h )/2 + _scrollHeight,
            position:'absolute'
        });
    }
    var no_ie6_pos_reset = function(dom){
        var _scrollHeight = $(document).scrollTop();
        var _windowHeight = $(window).height();
        var _windowWidth = $(window).width();
        var _h = dom.height();
        var _w = dom.width();
        dom.css({
            left:'50%',
            top:'50%',
            'margin-left':-_w/2,
            'margin-top':-_h/2,
            position:'fixed'
        });
    }
    var alert_fail = function (content){
        $('#fail_alert').remove();
        $('body').append('<div id="fail_alert" style="display:none" ><p>'+content+'</p></div>');
        var dom = $('#fail_alert');
        if ($.browser.msie && $.browser.version=="6.0"){
            ie6_pos_reset(dom);
        }else{
            no_ie6_pos_reset(dom);
        }
        dom.fadeIn();
        setTimeout(function(){dom.fadeOut()},2000);
    }
    window.alert_fail = alert_fail;
    var alert_success = function (content){
        $('#success_alert').remove();
        $('body').append('<div id="success_alert" style="display:none" ><p>'+content+'</p></div>');
        var dom = $('#success_alert');
        if ($.browser.msie && $.browser.version=="6.0"){
            ie6_pos_reset(dom);
        }else{
            no_ie6_pos_reset(dom);
        }
        dom.fadeIn();
        setTimeout(function(){dom.fadeOut()},2000);
    }
    window.alert_success = alert_success;
    //var alert_confirm = function (content,callback,width){
    //    require(['/static/js/jquery_confirm.js','/static/js/jquery_dialog.js'],function(){
    //        $.confirm({
    //            title:'ϵͳ��ʾ',
    //            html:'<p style="padding:20px 50px 0px 50px;line-height:20px;font-size:14px;text-align:left;">'+content+'</p>',
    //            ok_fun:callback,
    //            cancel_btn_name:'ȡ ��',
    //            confirm_btn_name:'ȷ ��',
    //            width:width?width:500
    //        });
    //    })
    //}
    //window.alert_confirm = alert_confirm;
var popup = {
    loading:function(){
        $('.pop_bg').fadeIn();
        document.getElementById('loading-icon').style.display = 'block';
    },
    removeLoading:function(){
        document.getElementById('pop_bg').style.display = 'none';
        document.getElementById('loading-icon').style.display = 'none';
    }
};