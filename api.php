<?php
/**
 * API入口
 * User: jund
 * Date: 2018/8/6
 * Time: 11:19
 */
namespace think;
// 加载基础文件
require __DIR__ . '/system/thinkphp/base.php';

// 支持事先使用静态方法设置Request对象和Config对象

// 执行应用并响应
Container::get('app')->path('app/')->bind('api')->run()->send();
