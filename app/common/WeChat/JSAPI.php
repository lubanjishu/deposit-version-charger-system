<?php
/*
|--------------------------------------------------------------------------
| 微信JS API接口
|--------------------------------------------------------------------------
| 
| @author Carmen
|
*/

namespace app\common\WeChat;
use app\common\WeChat\API;
class JSAPI
{
    private static $_defaultJSAPIlist = [
        'onMenuShareTimeline',      // 获取“分享到朋友圈”按钮点击状态及自定义分享内容接口
        'onMenuShareAppMessage',    // 获取“分享给朋友”按钮点击状态及自定义分享内容接口
    ];

    /**
     * 获取jsapi config
     * @return [type] [description]
     */
    public static function getConfig($url = null, $apiList = [])
    {
        $params = [
            'noncestr'     => API::createNoncestr(),
            'jsapi_ticket' => self::getJSAPIticket(),
            'timestamp'    => $_SERVER['REQUEST_TIME'],
            'url'          => $url ? $url : ('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])
        ];

        return [
            'appId'     => API::APP_ID,
            'timestamp' => $params['timestamp'],
            'nonceStr'  => $params['noncestr'],
            'signature' => self::createSignature($params),
            'jsApiList' => $apiList ? $apiList : self::$_defaultJSAPIlist
        ];
    }

    /**
     * 获得jsapi_ticket
     * @return string
     */
    public static function getJSAPIticket()
    {
        if (!$jsTicket = \Cache::get('WECHAT_JSSDK_JSAPI_TICKET'))
        {
            $requestUrl = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket';
            $params = [
                'access_token' => API::getAccessToken(),
                'type' => 'jsapi'
            ];

            $url      = $requestUrl . '?' . http_build_query($params);
            $result   = json_decode(\HttpResponse::get($url));
            $jsTicket = $result->ticket;

            // 缓存 JS Ticket
            \Cache::set('WECHAT_JSSDK_JSAPI_TICKET', $jsTicket, $result->expires_in);
        }
        return $jsTicket;
    }

    /**
     * 生成JS API 签名
     * @param  array    $params     参数
     * @return string
     */
    public static function createSignature($params)
    {
        ksort($params);
        $signature = '';
        foreach ($params as $k => $v)
        {
            $signature .= $k . '=' . $v . '&';
        }
        return sha1(rtrim($signature, '&'));
    }
}