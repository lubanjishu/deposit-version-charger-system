<?php
/*
|--------------------------------------------------------------------------
| 微信基础API
|--------------------------------------------------------------------------
| 
| @author Carmen
|
*/

namespace app\common\WeChat;

class API
{
	/**
	 * 配置
	 */
	const APP_ID     = 'wx8790bd1628f23628';				// 应用ID
	const APP_SECRET = 'dff27e6c9fa46a493c39531748a25ec5';	// 应用密钥
    //const APP_KEY    = 'f037b9e6a1d0d187bd36f9483e4e9813';	// API密钥
    const APP_KEY    = '05a7c61b57f714a09c32139ca7566b64';	// API密钥
	const MCHID  	 = '16050201251605020125';						// 商户号

	/**
	 * 网页授权获取用户基本信息
	 * @return object
	 */
	public static function getUserinfo()
	{
		$webAccessToken = self::getWebAccessToken(self::getAuthorizeCode('snsapi_userinfo'));
		if (!$webAccessToken)
			return false;

		$requestUrl = 'https://api.weixin.qq.com/sns/userinfo';
		$params = [
			'access_token' => $webAccessToken->access_token,
			'openid' => $webAccessToken->openid,
			'lang' => 'zh_CN'
		];

		$url = $requestUrl . '?' . http_build_query($params);
		$result = json_decode(\HttpResponse::get($url));

		return isset($result->errcode) ? false : $result;
	}

	/**
	 * 获取用户openid
	 * @return string
	 */
	public static function getUserOpenid()
	{
		$result = self::getWebAccessToken(self::getAuthorizeCode());
		return $result ? $result->openid : false;
	}

	/**
	 * 获取网页授权access_token
	 * 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 * @return object
	 */
	public static function getWebAccessToken($code)
	{
		$requestUrl = 'https://api.weixin.qq.com/sns/oauth2/access_token';
		$params = [
			'appid'  => self::APP_ID,
			'secret' => self::APP_SECRET,
			'code'   => $code,
			'grant_type' => 'authorization_code'
		];

		$url = $requestUrl . '?' . http_build_query($params);
		$result = json_decode(\HttpResponse::get($url));

		return isset($result->errcode) ? false : $result;
	}


    public static function getWxAccessToken(){
        if (!$accessToken = \Cache::get('WECHAT_API_ACCESS_TOKEN'))
        {
            $appid=API::APP_ID;
            $appsecret=API::APP_SECRET;
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$appsecret;
            $accessToken = self::makeRequest($url);
            $accessToken = json_decode($accessToken['result'],true);
            // 缓存 Access Token
            \Cache::set('WECHAT_API_ACCESS_TOKEN', $accessToken, 6000);
        }
        return $accessToken;
    }
    /**
     * 发起http请求
     * @param string $url 访问路径
     * @param array $params 参数，该数组多于1个，表示为POST
     * @param int $expire 请求超时时间
     * @param array $extend 请求伪造包头参数
     * @param string $hostIp HOST的地址
     * @return array    返回的为一个请求状态，一个内容
     */
    public static function makeRequest($url, $params = array(), $expire = 0, $extend = array(), $hostIp = '')
    {
        if (empty($url)) {
            return array('code' => '100');
        }

        $_curl = curl_init();
        $_header = array(
            'Accept-Language: zh-CN',
            'Connection: Keep-Alive',
            'Cache-Control: no-cache'
        );
        // 方便直接访问要设置host的地址
        if (!empty($hostIp)) {
            $urlInfo = parse_url($url);
            if (empty($urlInfo['host'])) {
                $urlInfo['host'] = substr(DOMAIN, 7, -1);
                $url = "http://{$hostIp}{$url}";
            } else {
                $url = str_replace($urlInfo['host'], $hostIp, $url);
            }
            $_header[] = "Host: {$urlInfo['host']}";
        }

        // 只要第二个参数传了值之后，就是POST的
        if (!empty($params)) {
            curl_setopt($_curl, CURLOPT_POSTFIELDS, http_build_query($params));
            curl_setopt($_curl, CURLOPT_POST, true);
        }

        if (substr($url, 0, 8) == 'https://') {
            curl_setopt($_curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($_curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        curl_setopt($_curl, CURLOPT_URL, $url);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($_curl, CURLOPT_USERAGENT, 'API PHP CURL');
        curl_setopt($_curl, CURLOPT_HTTPHEADER, $_header);

        if ($expire > 0) {
            curl_setopt($_curl, CURLOPT_TIMEOUT, $expire); // 处理超时时间
            curl_setopt($_curl, CURLOPT_CONNECTTIMEOUT, $expire); // 建立连接超时时间
        }

        // 额外的配置
        if (!empty($extend)) {
            curl_setopt_array($_curl, $extend);
        }

        $result['result'] = curl_exec($_curl);
        $result['code'] = curl_getinfo($_curl, CURLINFO_HTTP_CODE);
        $result['info'] = curl_getinfo($_curl);
        if ($result['result'] === false) {
            $result['result'] = curl_error($_curl);
            $result['code'] = -curl_errno($_curl);
        }

        curl_close($_curl);
        return $result;
    }
	/**
     * 获取access_token
     * @return string
     */
    public static function getAccessToken()
    {
        if (!$accessToken = \Cache::get('WECHAT_API_ACCESS_TOKEN'))
        {
        	$requestUrl = 'https://api.weixin.qq.com/cgi-bin/token';
            $params = [
                'grant_type' => 'client_credential',
                'appid'  => self::APP_ID,
                'secret' => self::APP_SECRET
            ];
            
			$url         = $requestUrl . '?' . http_build_query($params);
			$result      = curl($url);
			$accessToken = $result['access_token'];

            // 缓存 Access Token
            \Cache::set('WECHAT_API_ACCESS_TOKEN', $accessToken, 6000);
        }
        return $accessToken;
    }

	/**
	 * 用户同意授权，获取code
	 * @param  string 	$scope 	snsapi_base / snsapi_userinfo
	 * @return string
	 */
	public static function getAuthorizeCode($scope = 'snsapi_base')
	{
		if (isset($_GET['code']))
			return $_GET['code'];

		$requestUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize';
		$params = [
			'appid' => self::APP_ID,
			'redirect_uri' => 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
			'response_type' => 'code',
			'scope' => $scope
		];

		header('Location:' . $requestUrl . '?' . http_build_query($params) . '#wechat_redirect');
		exit();
	}

	/**
     * 生成签名
     * @param  array 	$params 	参数
     * @return string
     */
    public static function createSign($params)
    {
        ksort($params);
        $str = '';
        foreach ($params as $k => $v)
        {
            $str .= $k . '=' . $v . '&';
        }
        $str = $str . 'key=' . self::APP_KEY;
        $str = md5($str);
        return strtoupper($str);
    }

	/**
     * 产生随机字符串，不长于32位
     * @param  integer 	$length 	长度
     * @return string
     */
    public static function createNoncestr($length = 32) 
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";  
        $str = "";
        for ($i = 0; $i < $length; ++$i)
        {  
            $str.= substr($chars, mt_rand(0, strlen($chars) -1), 1);  
        }  
        return $str;
    }

    /**
     * array转xml
     * @param  array 	$params 	参数
     * @return string
     */
    public static function array2xml($params)
    {
        $xml = "<xml>";
        foreach ($params as $key => $val)
        {
            if (is_numeric($val))
            {
                $xml .= '<' . $key . '>' . $val . '</' . $key . '>';
            }
            else
            {
                $xml .= '<' . $key . '><![CDATA[' . $val . ']]></' . $key . '>';
            }
        }
        $xml .= '</xml>';

        return $xml; 
    }

    /**
     * xml转array
     * @param  string 	$xml    XML
     * @return array
     */
    public static function xml2array($xml)
    {
        $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);      
        return $array_data;
    }
}