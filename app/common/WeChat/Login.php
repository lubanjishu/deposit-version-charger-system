<?php
namespace app\common\WeChat;
class Login
{
	public function getWxUserInfo($appid, $secret, $code)
	{
		$get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$code.'&grant_type=authorization_code';
		$result = $this->doCurl($get_token_url);//获取openid和access_token
		if (empty($result) && !is_array($result)) return false;

		//根据openid和access_token查询用户信息  
		$access_token = $result['access_token'];
		$openid = $result['openid'];
		$get_user_info_url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';
		$userinfo = $this->doCurl($get_user_info_url);//获取用户的信息
		if (empty($userinfo) && !is_array($userinfo)) return false;
		return $userinfo;
	}

	public function doCurl($url)
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_HEADER,0);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);
		$res = curl_exec($ch);
		curl_close($ch);
		return json_decode($res,true);
	}
}
?>