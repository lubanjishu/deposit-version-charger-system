<?php
namespace app\common\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Cache;
class Menu extends controller
{
    //设置和获取菜单类型
    public function menuType($id) {
        $menuType = array(
            1=>'菜单',
            2=>'权限'
        );
        return $menuType[$id];
    }
    //设置和获取菜单状态
    public function menuStatus($id) {
        $menuStatus = array(
            1=>'显示',
            2=>'隐藏'
        );
        return $menuStatus[$id];
    }
    //设置和获取菜单归属
    public function menuAffiliation($id) {
        $menuStatus = array(
            1=>'系统后台',
            2=>'会员后台',
            3=>'商家后台'
        );
        return $menuStatus[$id];
    }
    //获取后台菜单图标
    public function getAdminMenuIcon($id) {
        $icon = array(
            12 => 'layui-icon-template-1',//文章管理
            14 => 'layui-icon-reply-fill',//商品管理
            15 => 'layui-icon-rmb',//提现管理
            19 => 'layui-icon-friends',//分销商管理
            16 => 'menu-withdrawal.png',//商品管理
            
            1 => 'menu-system.png',//系统管理
            3 => 'menu-members.png',//会员管理
            5 => 'menu-administrator.png',//管理员管理
            28 => 'menu-equipment.png',//设备管理
            18 => 'menu-financial.png',//财务统计
            15 => 'menu-withdrawal.png',//提现管理
            19 => 'menu-distributors.png',//分销商管理
            14 => 'menu-message.png',//留言管理
            51 => 'menu-account.png',//账户密码修改
             63 => 'menu-administrator.png',//管理员管理
        );

        return isset($icon[$id]) ? $icon[$id] : 'layui-icon-home';
    }
    //添加菜单
    public function addMenu($data = '', $reSql = false){
        if (!$data || !is_array($data)) return false;
        return db('menu')->fetchSql($reSql)->insert($data);
    }
    //编辑菜单
    public function editMenu($where = '', $data = '', $reSql = false){
        if (!$where || !$data || !is_array($data)) return false;
        if ($reSql) return db('menu')->fetchSql(true)->where($where)->update($data);
        return db('menu')->where($where)->update($data);
    }
    //获取菜单
    public function getOneMenu($fields = '*', $where = '',$reSql = false) {
        if ($reSql) return db('menu')->fetchSql(true)->field($fields)->where($where)->find();
        if ($where) {
            return db('menu')->field($fields)->where($where)->find();
        }
        return db('menu')->field($fields)->find();
    }
    //获取菜单集
    public function getAllMenu($fields = '*', $where = '', $reSql = false) {
        return db('menu')->field($fields)->where($where)->fetchSql($reSql)->select();
    }
    //获取菜单列表
    public function getMenuList($fields = '*', $where = '', $child_wh = '',$reSql = false) {
        $menuList = $this->getAllMenu($fields, $where, $reSql);
        if ($reSql) return $menuList;
        if (!empty($menuList) && is_array($menuList)) {
            foreach ($menuList as &$vlist) {
                if (isset($vlist['m_type'])) $vlist['m_type'] = $this->menuType($vlist['m_type']);
                if (isset($vlist['m_status'])) $vlist['m_status'] = $this->menuStatus($vlist['m_status']);
                if (isset($vlist['m_affiliation'])) $vlist['m_affiliation'] = $this->menuAffiliation($vlist['m_affiliation']);
                $child_where = $child_wh ? $child_wh.' AND m_parent_id='.$vlist['m_id'] : ' m_parent_id='.$vlist['m_id'];
                $vlist['m_child_menu'] = $this->getMenuList($fields, $child_where);
                $vlist['m_icon'] = $this->getAdminMenuIcon($vlist['m_id']);
            }
        }
        return $menuList;
    }
    //获取菜单集
    public function getAllMenuPage($fields = '*', $where = '', $child_wh = '', $totalPage = 2, $reSql = false) {
        return db('menu')->field($fields)->paginate($totalPage)->where($where)->fetchSql($reSql)->each(function($vlist, $key){
            global $fields,$child_wh;
            return $this->getAllMenuPageFun($vlist, $key, $fields, $child_wh);
        });
    }
    //获取菜单分页列表
    public function getMenuPage($fields = '*', $where = '', $child_wh = '', $totalPage = 2, $reSql = false) {
        if ($reSql) return db('menu')->fetchSql(true)->field($fields)->where($where)->paginate($totalPage);
        return db('menu')->field($fields)->where($where)->paginate($totalPage)->each(function($vlist, $key){
            global $fields,$child_wh;
            return $this->getAllMenuPageFun($vlist, $key, $fields, $child_wh);
        });
    }
    public function getAllMenuPageFun($vlist, $key, $fields, $child_wh) {
        if (isset($vlist['m_type'])) $vlist['m_type'] = $this->menuType($vlist['m_type']);
        if (isset($vlist['m_status'])) $vlist['m_status'] = $this->menuStatus($vlist['m_status']);
        if (isset($vlist['m_affiliation'])) $vlist['m_affiliation'] = $this->menuAffiliation($vlist['m_affiliation']);
        $child_wh = $child_wh ? $child_wh.' AND m_parent_id='.$vlist['m_id'] : ' m_parent_id='.$vlist['m_id'];
        $child_menu = $this->getMenuList($fields, $child_wh);
        if ($child_menu) {
            foreach ($child_menu as &$vchild) {
                $vchild['m_child_menu'] = $this->getMenuList($fields, $child_wh);
            }
        }
        $vlist['m_child_menu'] = $this->getMenuList($fields, $child_wh);
        return $vlist;
    }
}


