<?php
namespace app\common\controller;
use think\Controller;

class Role extends Controller
{
    public static $distributorOneRoleId = 4;//一级分销商角色ID
    public static $distributorTowRoleId = 5;//二级分销商角色ID
    public static $distributorThirdRoleId = 6;//三级分销商角色ID
    //获取分销商角色索引ID
    public function getDistributorRoleIdList() {
        return [self::$distributorOneRoleId,self::$distributorTowRoleId,self::$distributorThirdRoleId];
    }

    //添加角色
    public function addRole($data = '', $reSql = false) {
        if (!$data || !is_array($data)) return false;
        return db('role')->fetchSql($reSql)->insert($data) ? 'ok' : 0;
    }
    //编辑角色
    public function editRole($where, $data = '', $reSql = false) {
        if (!$data || !is_array($data)) return false;
        return db('role')->where($where)->fetchSql($reSql)->update($data) ? 'ok' : 0;
    }
    //获取角色信息
    public function getRoleInfo($where = "", $fields = "*", $reSql = false) {
        return db('role')->where($where)->field($fields)->fetchSql($reSql)->find();
    }
    //获取角色某一字段信息的值
    public function getRoleInfoByField($where = "", $field = "r_id", $reSql = false) {
        $res = db('role')->where($where)->field($field)->fetchSql($reSql)->find();
        return $res ? $res[$field] : '';
    }
    //获取角色列表信息
    public function getRoleList($where = "", $fields = "*", $order= 'r_id DESC', $reSql = false) {
        $res = db('role')->where($where)->field($fields)->order($order)->fetchSql($reSql)->select();
        if (!$res) return false;
        $userCom = controller('common/user');
        foreach ($res as &$vList) {
            $vList['r_status_str'] = $vList['r_status'] == 1 ? '启动' : '关闭';
            $vList['r_adduser'] = $userCom->getUserInfoByField($vList['r_adduid'],'u_name');
        }
        return $res;
    }
}