<?php
namespace app\common\controller;
use think\Controller;
use think\facade\Env;

/**
 * 公共函数
 */
class Pub extends Controller
{

	public function initialize()
	{
		parent::initialize();
	}


    /*
     * ===================================================================================
     *
     * 功能部分
     *
     * ===================================================================================
     */
	//退出登录
    public function logout($session_name = 'userInfo')
    {
        session($session_name, NULL);
        return reAjaxMsg(1,'已安全退出！');
    }
    //图片上传：$id索引ID；$typeName模块名称（即图片保存位置文类好的目录，自定义）；$fieldImg图片路径保存的字段
    public function gotoUploadImg($id, $typeName, $fieldImg)
    {
        if (!$typeName || !$fieldImg) return reAjaxMsg(0,'上传条件不具备！');
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('uploadImg');
        if(!$file) return reAjaxMsg(0,'您未选择任何文件上传！');

        $path = Env::get('root_path')  . 'upload'. DIRECTORY_SEPARATOR . $typeName.'/';
        $info = $file->validate(['size'=>105678000,'ext'=>'jpg,png,gif,mp3'])->move($path,$typeName.'_'.time().getRandNen(6));
        //上传成功
        if($info){
            $imgName = $info->getSaveName();//获取图片名称
            $imgName = '/upload/'. $typeName.'/'.$imgName;
            if ($id) {//编辑
                $newData = [$fieldImg=>$imgName];
                switch ($typeName) {
                    case 'userinfo':
                        $oldImgPath = db('user_ic')->where('c_id='.$id)->value($fieldImg);
                        $result = db('user_ic')->where('c_id='.$id)->update($newData);
                        break;
                    case 'service':
                        $oldImgPath = db('service')->where('s_id='.$id)->value($fieldImg);
                        $result = db('service')->where('s_id='.$id)->update($newData);
                        break;
                    case 'xcxbanner':
                        $oldImgPath = db('config')->where('c_id='.$id)->value($fieldImg);
                        $result = db('config')->where('c_id='.$id)->update($newData);
                        break;
                    case 'yuyin':
                        $oldImgPath = db('distributor_yuyin')->where('y_id='.$id)->value($fieldImg);
                        $result = db('distributor_yuyin')->where('y_id='.$id)->update($newData);
                        break;
                    case 'information_type':
                        $oldImgPath = db('information_type')->where('t_id='.$id)->value($fieldImg);
                        $result = db('information_type')->where('t_id='.$id)->update($newData);
                        break;
                    case 'user_information':
                        $result = '';
                        break;
                    default:
                        return reAjaxMsg(0,'上传类型模块有误！');
                        break;
                }
                //如果编辑不成功，则删除已上传的图片
                if (!empty($result)) {
                    delFile($imgName);
                    return reAjaxMsg(0,$result);
                }
                //删除旧图片
                if (!empty($oldImgPath)) {
                    $imgPath = Env::get('root_path').substr($oldImgPath,1);
                    if ($imgPath) {
                        delFile($imgPath);
                    }
                }
            } else {//新增
                /*if ($typeName == 'xcxbanner') {
                    $res_id = db('config')->insertGetId([$fieldImg=>$imgName]);
                    return $res_id ? reAjaxMsg(1,'上传成功！',['imgId'=>$res_id,'imgUrl'=>$imgName]) : reAjaxMsg(0,'上传失败！');
                }*/
            }
            return reAjaxMsg(1,$imgName);
        }
        return reAjaxMsg(0,'上传异常，请尝试小图片上传！'.$file->getError());
    }
    //删除图片
    public function gotoDelUploadImg($id, $typeName, $fieldImg)
    {
        if (!$id) return reAjaxMsg(0,'缺少索引ID！');
        $newData = array($fieldImg => '');
        switch ($typeName) {
            case 'article'://文章
                $mod = model('Article');
                $oldImgPath = $mod->getInformationInfoById($id, $fieldImg);
                $result = $mod->editInformationInfo('a_id='.$id,$newData);
                break;
            case 'bannerpc'://PC端广告轮播图
                $mod = model('Banner');
                $oldImgPath = $mod->getBannerFieldInfo($id,$fieldImg);
                $newData['b_status'] = -1;
                $result = $mod->upBanner($id,$newData);
                break;
            case 'bannerwap'://WAP端广告轮播图
                $mod = model('Banner');
                $oldImgPath = $mod->getBannerFieldInfo($id,$fieldImg);
                $newData['b_status'] = -1;
                $result = $mod->upBanner($id,$newData);
                break;
            case 'goodsbrand'://商品品牌logo
                $mod = model('GoodsBrand');
                $oldImgPath = $mod->getBrandFieldInfo($id,$fieldImg);
                $newData['b_status'] = -1;
                $result = $mod->upBrand($id,$newData);
                break;
            default:
                return reAjaxMsg(0,'删除类型模块有误！');
                break;
        }
        if (!$oldImgPath) return reAjaxMsg(0,'该图片数据有异常！');
        if ($result == 'ok'){
            delFile(Env::get('root_path').substr($oldImgPath,1));
            return reAjaxMsg(1,'图片删除成功！');
        } else {
            return reAjaxMsg(0,'图片删除失败！'.$result);
        }
    }
    //发送验证码 推送
    public function sendCode(){
        $acc_num = input('acc_number','');
        $code = input('code','');

        //根据副相伴号获取主相伴号的手机


    }
    //转换成详细地址
    public function getAddress($province='',$city='',$area='',$town = '',$address='')
    {
        $province = $province ? db('address')->where('s_id='.$province)->value('s_name'):'';
        $city = $city ? db('address')->where('s_id='.$city)->value('s_name'):'';
        $area = $area ? db('address')->where('s_id='.$area)->value('s_name') : '';
        $town = $town ? db('address')->where('s_id='.$town)->value('s_name') : '';
        return $province.$city.$area.$town.$address;
    }
    //获取子集地区列表
    public function getChildAreaList($parentId)
    {
        return db('address')->where('s_parent_id='.$parentId)->cache('addressInfo'.$parentId)->select();
    }
    //获取省份/特市/特区数据
    public function getProvince()
    {
        $res = db('address')->where('s_parent_id=0')->select();
        return $res;
    }
    //检测副相伴号是否过期
    public function checkUserComboValid($child_acc_num)
    {
        $res = db('user_combo')->field('c_endtime,c_child_acc_nums')->select();
        $data = '';
        foreach ($res as $v) {
            if (in_array($child_acc_num,explode(',',$v['c_child_acc_nums']))) {
                $data = $v['c_endtime'];
                break;
            }
        }
        return $data ? ($data > myTime() ? 'ok' : '该相伴号套餐已过期！') : '该相伴号未注册！';
    }

}