<?php

namespace app\common\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Cache;

class User extends controller
{
    private $userAccount = 'userAccount';
    private $userId = 'userId';
    private $userPassword = 'userPassword';
    private $userImg = 'userImg';
    private $userPhone = 'userPhone';
    public static $userTypeId = 1;//个人
    public static $adminTypeId = 2;//管理员
    public static $procurementTypeId = 3;//企业
    public $withdrawApplyingTypeId = 1; //提现申请中
    public $withdrawApplyPassTypeId = 2; //提现审核通过
    public $withdrawApplyNoPassTypeId = 3; //提现拒绝
    private $role = 'userRole';

    //用户类型
    public function userType($id)
    {
        $user_type = array(
            self::$userTypeId        => '普通会员',
            self::$adminTypeId       => '管理员',
            self::$procurementTypeId => '企业',
        );
        return $user_type[$id];
    }

    //用户状态
    public function userStatus($id)
    {
        $user_status = array(
            1 => '启动',
            2 => '暂停',
        );
        return $user_status[$id];
    }

    //用户提现状态状态
    public function userWithdrawStatus($id)
    {
        $user_withdraw_status = array(
            $this->withdrawApplyingTypeId    => '申请中',
            $this->withdrawApplyPassTypeId   => '审核通过',
            $this->withdrawApplyNoPassTypeId => '拒绝申请',
        );
        return $user_withdraw_status[$id];
    }

    //用户登录
    public function login($account, $password, $type = 1)
    {
        $account = trim($account);
        $password = trim($password);
        if ($account == '' && $password == '') return lang('index_public_8');//'用户名或者密码不能为空';

        switch ($type) {
            //普通会员
            case self::$userTypeId:
                $session_name = 'userInfo';
                break;
            //管理员
            case self::$adminTypeId:
                $session_name = 'adminInfo';
                $this->userAccount = 'adminAccount';
                $this->userPassword = 'adminPassword';
                $this->userId = 'adminId';
                $this->userImg = 'adminImg';
                $this->userPhone = 'adminPhone';
                break;
            default:
                $session_name = 'userInfo';
                break;
        }

        //特批后台管理员以用户名登录
        if (!checkPhone($account)) {
            $where = 'u_name="' . $account . '" AND u_status=1 AND u_type=' . $type;
            $account = db('User')->where($where)->value('u_phone');
            if (!$account) return '该用户不存在！';
        }

        $password = pswCAM($account, $password);

//       var_dump($password,$account);
        $userInfo = session($session_name);
        if ($userInfo && isset($userInfo[$this->userAccount]) && isset($userInfo[$this->userPassword])) {
            if ($userInfo[$this->userAccount] == $account && $userInfo[$this->userPassword] == $password) return 'ok';
            // return 'ok';
        }

//        $infor = model('User')->getUserInfoByWhere('u_phone="' . $account . '" AND u_status=1 AND u_password="' . $password . '" ', 'u_id,u_name,u_uheadimg,u_phone,u_role');
//
            $infor = model('User')->getUserInfoByWhere('u_phone="'.$account.'" AND u_status=1  ', 'u_id,u_name,u_uheadimg,u_phone,u_role');
        if ($infor) {
            //登录成功
            $userData = array(
                $this->userAccount  => $account,
                $this->userPassword => $password,
                $this->userId       => $infor['u_id'],
                $this->userPhone    => $infor['u_phone'],
                $this->userImg      => $infor['u_uheadimg'] ? $infor['u_uheadimg'] : '/login_1.png',
                $this->role         => $infor['u_role']
            );
            session($session_name, $userData);

            //添加用户登录日志
            $data = array(
                'l_uid'        => $infor['u_id'],
                'l_uip'        => getIp(),
                'l_utype'      => $type,
                'l_login_time' => myTime(),
            );
            db('user_login_log')->insert($data);
            return 'ok';
        }
        return lang('该账号不存在或密码有误！');//'该手机账号不存在或密码有误！';
    }

    //便捷登录 1微信， 2QQ
    public function speedyLogin($userInfo, $type = 1)
    {
        if ($type == 1) {
            $openid = $userInfo['openid'];
            if (!$sysUserInfo = db('user')->where('openid="' . $openid . '"')->find()) {
                $u_phone = getRandNen(11);
                $u_uheadimg = $userInfo['headimgurl'];
                $role = 0;
                $userInfo['nickname'] = $this->filterEmoji($userInfo['nickname']);
                $newData = [
                    'openid'     => $openid,
                    'u_name'     => $userInfo['nickname'],
                    'u_nick'     => $userInfo['nickname'],
                    'u_phone'    => $u_phone,
                    'u_password' => '',
                    'u_weixin'   => json_encode($userInfo, JSON_UNESCAPED_UNICODE),
                    'u_type'     => 1,
                    'u_status'   => 1,
                    'u_uheadimg' => $userInfo['headimgurl'],
                    'u_ip'       => getIp(),
                    'u_addtime'  => myTime(),
                    'u_unionid'  => isset($userInfo['unionid']) ? $userInfo['unionid'] : '',
                    'u_sex'      => $userInfo['sex'],
                    'u_role'     => $role
                ];
                if (!$u_id = db('user')->insert($newData, false, true)) {
                    return '[01]授权异常！';
                }
            } else {
                $u_id = $sysUserInfo['u_id'];
                $u_phone = $sysUserInfo['u_phone'];
                $role = $sysUserInfo['u_role'];
                $u_uheadimg = $sysUserInfo['u_uheadimg'] ? $sysUserInfo['u_uheadimg'] : $userInfo['headimgurl'];
            }
            //登录成功
            $userData = array(
                $this->userAccount  => $userInfo['nickname'],
                $this->userPassword => '',
                $this->userId       => $u_id,
                $this->userPhone    => $u_phone,
                $this->userImg      => $u_uheadimg ? $u_uheadimg : '/public/static/images/logo.png',
                $this->role         => $role
            );
            session('userInfo', $userData);
            //添加用户登录日志
            $data = array(
                'l_uid'        => $u_id,
                'l_uip'        => getIp(),
                'l_utype'      => $type,
                'l_login_time' => myTime(),
            );
            db('user_login_log')->insert($data);
            return 'ok';
        }
        return '[02]授权失败！';

    }

    //便捷登录 1微信， 2QQ
    public function speedyAliPayLogin($userInfo, $type = 3)
    {
        $openid = $userInfo['user_id'];
        if (!$sysUserInfo = db('user')->where('openid="' . $openid . '"')->find()) {
            $u_phone = getRandNen(11);
            $u_uheadimg = $userInfo['avatar'];
            $role = 0;
            $userInfo['nick_name'] = $this->filterEmoji($userInfo['nick_name']??'');
            $newData = [
                'openid'     => $openid,
                'u_name'     => $userInfo['nick_name'],
                'u_nick'     => $userInfo['nick_name'],
                'u_phone'    => $u_phone,
                'u_password' => '',
                'u_weixin'   => json_encode($userInfo, JSON_UNESCAPED_UNICODE),
                'u_type'     => 1,
                'u_status'   => 1,
                'u_uheadimg' => $userInfo['avatar'],
                'u_ip'       => getIp(),
                'u_addtime'  => myTime(),
                'u_unionid'  => isset($userInfo['unionid']) ? $userInfo['unionid'] : '',
                'u_sex'      => 1,
                'u_role'     => $role
            ];
            if (!$u_id = db('user')->insert($newData, false, true)) {
                return '[01]授权异常！';
            }
        } else {
            $u_id = $sysUserInfo['u_id'];
            $u_phone = $sysUserInfo['u_phone'];
            $role = $sysUserInfo['u_role'];
            $u_uheadimg = $sysUserInfo['u_uheadimg'] ? $sysUserInfo['u_uheadimg'] : $userInfo['headimgurl'];
        }
        //登录成功
        $userData = array(
            $this->userAccount  => $userInfo['nick_name']??'',
            $this->userPassword => '',
            $this->userId       => $u_id,
            $this->userPhone    => $u_phone,
            $this->userImg      => $u_uheadimg ? $u_uheadimg : '/public/static/images/logo.png',
            $this->role         => $role
        );
        session('userInfo', $userData);
        //添加用户登录日志
        $data = array(
            'l_uid'        => $u_id,
            'l_uip'        => getIp(),
            'l_utype'      => $type,
            'l_login_time' => myTime(),
        );
        db('user_login_log')->insert($data);
        return 'ok';
    }

    //过滤微信emoji字符
    public function filterEmoji($str)
    {
        $str = preg_replace_callback('/./u',
            function (array $match) {
                return strlen($match[0]) >= 4 ? '' : $match[0];
            },
            $str);
        return $str;
    }

    //根据用户ID获取用户所有信息
    public function getUserAllInfoById($uid = '', $expire = 86400, $reSql = false)
    {
//        if (!$uid) return false;
//        if (!$expire) cache('userList', NULL);
//        if ($reSql) return db('user')->fetchSql(true)->where('u_id', $uid)->find();
        $userInfo = db('user')->where('u_id', $uid)->find();
        return $userInfo;
    }

    //根据自定义条件获取用户信息
    public function getUserOneInfoByWhere($where = '', $fields = '*', $reSql = false)
    {
        if (!$where) return false;
        return model('User')->getUserInfoByWhere($where, $fields, $reSql);
    }

    //获取用户某一个字段信息
    public function getUserInfoByField($userId, $field, $reSql = false)
    {
        $res = db('user')->field($field)->fetchSql($reSql)->where('u_id=' . $userId)->find();
        return $res ? $res[$field] : '';
    }

    //根据自定义条件，获取用户列表
    public function getUserListByWhere($fields = '*', $where = '', $expire = '86400', $reSql = false)
    {
        if (!$expire) cache('userList', NULL);
        return db('user')->cache('userList', $expire)->fetchSql($reSql)->field($fields)->where($where)->select();
    }

    //根据自定义条件，获取用户列表（信息过滤）
    public function getUserListInfoByWhere($fields = '*', $where = '', $expire = '86400', $reSql = false)
    {
        $result = $this->getUserListByWhere($fields, $where, $expire, $reSql);
        if ($reSql) return $result;
        if ($result) {
            $orderCom = controller('common/order');
            foreach ($result as &$vList) {
                if (isset($vList['u_type'])) $vList['u_type'] = $this->userType($vList['u_type']);
                if (isset($vList['u_status'])) $vList['u_status'] = $this->userStatus($vList['u_status']);
                if (empty($vList['u_name']) && isset($vList['u_phone'])) $vList['u_name'] = $vList['u_phone'];
                $vList['u_order_money'] = isset($vList['u_id']) ? sprintf('%0.2f', $this->getUserAmountMoney($vList['u_id'], 'a_done_total_money')) : 0;
                $vList['u_order_num'] = isset($vList['u_id']) ? $orderCom->getDoneOrderNum($vList['u_id']) : 0;
                $vList['u_account'] = isset($vList['u_id']) ? $this->getUserAmountMoney($vList['u_id']) : 0;
            }
        }
        return $result;
    }

    //手机号码设置中间四位数字隐藏
    public function resetUserPhone($phone)
    {
        if (!checkPhone($phone)) return false;
        return substr($phone, 0, 4) . "****" . substr($phone, -3);
    }

    //添加会员充值
    public function addUserRecharge($data)
    {
        if (empty($data['r_phone'])) return '用户手机号码不可为空！';
        $u_phone = $data['r_phone'];
        //获取充值者ID
        $userInfo = $this->getUserOneInfoByWhere('u_phone=' . $u_phone, 'u_id');
        if (empty($userInfo['u_id'])) return '用户不存在！';
        $uid = $userInfo['u_id'];
        $r_money = $data['r_money'];
        $data['r_uid'] = $uid;

        $userAmount = $this->getUserAmount($uid);
        $userRecharge = db('user_recharge');
        $userRecharge->startTrans();
        $orderCom = controller('common/order');
        try {
            //添加用户账户余额
            if ($userAmount) {
                $new_money = bcadd($r_money, $userAmount['a_money'], 3);
                $accountData = array('a_money' => $new_money);
                db('user_amount')->where('a_uid=' . $uid)->update($accountData);
            } else {
                $accountData = array(
                    'a_money' => $r_money,
                    'a_uid'   => $uid,
                );
                db('user_amount')->insert($accountData);
            }

            //添加用户账户流水记录
            $accountRecordsData = array(
                'a_uid'        => $uid,
                'a_money'      => $r_money,
                'a_explain'    => '管理员充值',
                'a_type'       => $orderCom->amountRecordsChongzhi,//类型【1充值，2区块买卖，3佣金，4商品买卖】
                'a_last_money' => $userAmount ? $userAmount['a_money'] : 0,//变动前金额
                'a_addtime'    => myTime()
            );
            db('user_amount_records')->insert($accountRecordsData);

            //添加客户充值记录
            $userRecharge->insert($data);

            // 提交事务
            $userRecharge->commit();
            return 'ok';
        } catch (\Exception $e) {
            // 回滚事务
            $userRecharge->rollback();
            return '充值失败，失败原因：' . $e->getMessage();
        }
    }

    //获取会员充值分页列表
    public function getUserRechargePage($where = '', $fields = '*', $totalPage = 2, $query = [], $order = '', $reSql = false)
    {
        if ($reSql) return db('user_recharge')->fetchSql($reSql)->field($fields)->where($where)->order($order)->paginate($totalPage, false, $query);
        return db('user_recharge')->fetchSql($reSql)->field($fields)->where($where)->order($order)->paginate($totalPage, false, $query)->each(function ($vlist) {
            return $this->changeUserRechargeData($vlist);
        });
    }

    public function changeUserRechargeData($vlist)
    {
        $vlist['r_name'] = $this->getUserOneInfoByWhere('u_id=' . $vlist['r_uid'], 'u_name')['u_name'];
        $vlist['r_add_name'] = $this->getUserOneInfoByWhere('u_id=' . $vlist['r_adduid'], 'u_name')['u_name'];
        return $vlist;
    }

    //添加用户银行信息
    public function addUserBankInfo($data, $reSql = false)
    {
        return db('user_bank')->fetchSql($reSql)->insert($data) ? 'ok' : '添加失败！';
    }

    //编辑用户银行信息
    public function editUserBankInfo($where, $data, $reSql = false)
    {
        return db('user_bank')->where($where)->fetchSql($reSql)->update($data) ? 'ok' : '编辑失败！';
    }

    //获取用户银行信息
    public function getUserBankInfo($where = '', $fields = '*', $reSql = false)
    {
        return db('user_bank')->where($where)->field($fields)->fetchSql($reSql)->find();
    }

    //获取用户账户余额等信息
    public function getUserAmount($uid, $reSql = false)
    {
        return db('user_amount')->where('a_uid=' . $uid)->fetchSql($reSql)->find();
    }

    //获取用户账户某一字段值等信息
    public function getUserAmountMoney($uid, $field = 'a_money', $reSql = false)
    {
        $res = db('user_amount')->where('a_uid=' . $uid)->fetchSql($reSql)->find();
        if ($reSql) return $res;
        return $res ? $res[$field] : 0;
    }

    //统计用户佣金
    public function countUserYongjing($where, $reSql = false)
    {
        $orderCom = controller('common/order');
        $where = is_array($where) ? array_merge($where, ['a_type' => ['=', $orderCom->amountRecordsYongjin]]) : $where . ' AND a_type=' . $orderCom->amountRecordsYongjin;
        return db('user_amount_records')->where($where)->fetchSql($reSql)->sum('a_money');
    }

    //获取用户最大可提现金额
    public function getUserMaxCanWithdrawMoney($uid)
    {
        $account = $this->getUserAmount($uid);
        return $account ? $account['a_money'] : 0;
    }

    //获取用户最小可提现金额
    public function getUserMinCanWithdrawMoney()
    {
        return 100;
    }

    //获取用户提现手续费
    public function getUserWithdrawServiceCharge($money)
    {
        return bcmul(0.05, $money, 2);
    }

    //用户提现申请
    public function addUserWithdrawMoney($uid, $money)
    {
        if (trim($money) < 0 || !is_numeric($money)) return '提现金额有误！';
        $userInfo = $this->getUserAllInfoById($uid);
        if (!$userInfo) return '该用户不存在！';
        //获取用户总余额
        $account = $this->getUserAmount($uid);
        $user_total_account = $account ? $account['a_money'] : 0;
        if (!$user_total_account) return '该用户余额为0！';
        if ($user_total_account < $money) return '该用户余额不足！';

        if (!$this->getUserBankInfo('b_uid=' . $uid)) {
            return '请添加银行信息！';
        }

        $w_service_charge = $this->getUserWithdrawServiceCharge($money);//手续费
        $reality_money = bcsub($money, $w_service_charge, 2);//实际到账金额

        //更改用户账户余额
        $new_money = bcsub($user_total_account, $money, 2);//总余额-提现金额
        $accountData = array('a_money' => $new_money);
        //添加用户账户流水记录
        $orderCom = controller('common/order');
        $accountRecordsData = array(
            'a_uid'        => $uid,
            'a_money'      => bcmul($money, -1, 2),
            'a_explain'    => '用户提现',
            'a_type'       => $orderCom->amountRecordsWithdraw,//类型【1充值，2区块买卖，3佣金，4商品买卖,5提现】
            'a_last_money' => $user_total_account,//变动前金额
            'a_addtime'    => myTime()
        );
        //添加用户提现信息
        $newData = array(
            'w_uid'            => $uid,
            'w_uname'          => $userInfo['u_name'],
            'w_money'          => $reality_money,
            'w_service_charge' => $w_service_charge,
            'w_total_money'    => $money,
            'w_addtime'        => myTime(),
            'w_status'         => $this->withdrawApplyingTypeId,
        );

        $withdraw = db('user_withdraw');
        $withdraw->startTrans();
        try {
            //添加用户提现信息
            db('user_amount')->where('a_uid=' . $uid)->update($accountData);
            //添加用户账户流水记录
            db('user_amount_records')->insert($accountRecordsData);
            //添加用户提现信息
            $withdraw->insert($newData);
            // 提交事务
            $withdraw->commit();
            return 'ok';
        } catch (\Exception $e) {
            // 回滚事务
            $withdraw->rollback();
            return '提现申请失败，失败原因：' . $e->getMessage();
        }
    }

    //用户提现分页列表
    public function getUserWithdrawMoneyPage($where = '', $fields = '*', $totalPage = 2, $query = [], $order = '', $reSql = false)
    {
        if ($reSql) return db('user_withdraw')->fetchSql($reSql)->field($fields)->where($where)->order($order)->paginate($totalPage, false, $query);
        return db('user_withdraw')->fetchSql($reSql)->field($fields)->where($where)->order($order)->paginate($totalPage, false, $query)->each(function ($vlist) {
            return $this->changeUserWithdrawMoneyData($vlist);
        });
    }

    public function changeUserWithdrawMoneyData($vlist)
    {
        $vlist['w_uphone'] = !empty($vlist['w_uid']) ? $this->getUserOneInfoByWhere('u_id=' . $vlist['w_uid'], 'u_phone')['u_phone'] : '';
        $vlist['w_douser_name'] = !empty($vlist['w_douid']) ? $this->getUserOneInfoByWhere('u_id=' . $vlist['w_douid'], 'u_name')['u_name'] : '';
        $vlist['w_status_str'] = $this->userWithdrawStatus($vlist['w_status']);
        return $vlist;
    }

    //获取用户提现信息 单条
    public function getUserWithdraw($where = '', $fields = '*', $reSql = false)
    {
        $res = db('user_withdraw')->where($where)->field($fields)->fetchSql($reSql)->find();
        if ($res && !$reSql) {
            $bankInfo = $this->getUserBankInfo('b_uid=' . $res['w_uid']);
            $res['bankInfo'] = $bankInfo ? $bankInfo : '';
            $res['u_phone'] = $this->getUserInfoByField($res['w_uid'], 'u_phone');
        }
        return $res;
    }

    //获取用户某一字段提现信息
    public function getUserWithdrawInfoByField($w_id, $field)
    {
        $res = db('user_withdraw')->field($field)->where('w_id=' . $w_id)->find();
        return $res ? $res[$field] : '';
    }

    //审核用户提现状态
    public function checkUserWithdrawMoneyStatus($where, $data, $reSql = false)
    {
        return db('user_withdraw')->where($where)->fetchSql($reSql)->update($data) ? 'ok' : '操作失败！';
    }

    //获取用户推荐人的信息列表
    public function getUserRecommend($where, $fields = '*', $reSql = false)
    {
        $userRecommend = $this->getUserListInfoByWhere($fields, $where, 0);
        if (!$userRecommend) return $userRecommend;
        foreach ($userRecommend as &$vList) {
            $vList['u_phone'] = resetUserPhone($vList['u_phone']);
            $vList['u_name'] = resetUserPhone($vList['u_name']);
            $childList = $this->getUserListInfoByWhere($fields, 'u_inviter_uid="' . $vList['u_id'] . '"', 0);
            if ($childList) {
                foreach ($childList as &$vChildList) {
                    $vChildList['u_phone'] = resetUserPhone($vChildList['u_phone']);
                    $vChildList['u_name'] = resetUserPhone($vChildList['u_name']);
                    $res = $this->getUserListInfoByWhere($fields, 'u_inviter_uid="' . $vChildList['u_id'] . '"', 0);
                    if ($res) {
                        foreach ($res as &$vRes) {
                            $vRes['u_phone'] = resetUserPhone($vRes['u_phone']);
                            $vRes['u_name'] = resetUserPhone($vRes['u_name']);
                        }
                    }
                    $vChildList['childList'] = $res;
                }
            }
            $vList['childList'] = $childList;
        }
        return $userRecommend;
    }

    //获取用户明细分页数据
    public function getUserAccountPage($where = '', $fields = '*', $totalPage = 2, $query = [], $order = '', $reSql = false)
    {
        if ($reSql) return db('user_amount_records')->fetchSql($reSql)->field($fields)->where($where)->order($order)->paginate($totalPage, false, $query);
        return db('user_amount_records')->fetchSql($reSql)->field($fields)->where($where)->order($order)->paginate($totalPage, false, $query)->each(function ($vlist) {
            return $this->changeUserAccountData($vlist);
        });
    }

    public function changeUserAccountData($vlist)
    {
        $vlist['a_uphone'] = !empty($vlist['a_uid']) ? $this->getUserOneInfoByWhere('u_id=' . $vlist['a_uid'], 'u_phone')['u_phone'] : '';
        $vlist['a_uname'] = !empty($vlist['a_uid']) ? $this->getUserOneInfoByWhere('u_id=' . $vlist['a_uid'], 'u_name')['u_name'] : '';
        return $vlist;
    }

    //获取用户账户明细总和
    public function getUserAccountRecordsFieldSum($where = '', $reSql = false)
    {
        $res = db('user_amount_records')->where($where)->fetchSql($reSql)->field('a_money')->select();
        if ($reSql) return $res;
        if ($res) {
            $sum = 0;
            foreach ($res as $v) {
                if ($v['a_money'] < 0) $v['a_money'] = bcmul($v['a_money'], -1, 3);
                $sum = bcadd($sum, $v['a_money'], 3);
            }
            $res = $sum;
        } else {
            $res = 0;
        }
        return $res;
    }

    //获取用户充值总和
    public function getUserRechargeFieldSum($where = '', $reSql = false)
    {
        return db('user_recharge')->where($where)->fetchSql($reSql)->sum('r_money');
    }

    //获取用户提现总和
    public function getUserWithdrawFieldSum($where = '', $reSql = false)
    {
        return db('user_withdraw')->where($where)->fetchSql($reSql)->sum('w_money');
    }

    //获取用户上级信息
    public function getUserReferrerInfo($uid, $field = '*')
    {
        $userInfo = db('user')->where('u_inviter_uid > 0 AND u_id=' . $uid)->field('u_inviter_uid')->find();
        if (!$userInfo) {
            return false;
        }
        return db('user')->where('u_id=' . $userInfo['u_inviter_uid'])->field($field)->find();
    }

    //获取用户上级UID
    public function getUserReferrerUid($uid)
    {
        $userInfo = db('user')->where('u_inviter_uid > 0 AND u_id=' . $uid)->field('u_inviter_uid')->find();
        if (!$userInfo) {
            return 0;
        }
        return $userInfo ? $userInfo['u_inviter_uid'] : 0;
    }

    //计算交易手续费
    public function countDealServiceCharge($money = '', $type = 'sell', $level = 'self')
    {
        if (!$money) return false;
        $service_charge = config('service_charge');
        $percentage = $service_charge[$type][$level];
        return bcmul($percentage, $money, 3);
    }

    //获取用户账号明细 消费明细
    public function getAccountDetailPageList($fields = '*', $where = '', $page = 1, $totalPage = 2, $order = '', $reSql = false)
    {
        return db('user_amount_records')->field($fields)->fetchSql($reSql)->where($where)->order($order)->page($page, $totalPage)->select();
    }

    //统计所有会员总余额
    public function countUserTotalAmountMoney($where = '')
    {
        $where = $where ? $where . ' AND a_uid != 0' : 'a_uid != 0';
        return db('user_amount')->where($where . ' AND a_uid != 0')->sum('a_money');
    }


}


