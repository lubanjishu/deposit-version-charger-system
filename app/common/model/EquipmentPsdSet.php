<?php
/**
 * 密码小时设置列表
 * User: jund
 * Date: 2019/1/27
 * Time: 23:31
 */

namespace app\common\model;


use think\Model;

class EquipmentPsdSet extends Model
{
    protected $table = 'de_equipment_psd_set';
    protected $pk = 'p_id';
}