<?php

namespace app\common\model;

use think\Model;
use think\facade\Request;

class User extends Model
{

    // 设置当前模型对应的完整数据表名称
    protected $table = 'de_user';
    protected $pk = 'u_id';
    private $allowFields = [
        'u_name','u_name','u_name','u_name','u_name','u_name','u_name','u_name','u_name','u_name','u_name','u_name','u_name','u_name'
    ];

    public static $userTypeId = 1;//普通会员
    public static $adminTypeId = 2;//管理员
    public static $procurementTypeId = 3;//企业
    public static $userStatusNormalId = 1;//正常
    public static $userStatusStopId = 2;//暂停
    public static $userStatusAuditId = 3;//审核中

    /*
     *  =================================================================================
     *                              用户信息操作
     *  =================================================================================
     */
    //用户类型
    public function userType($id){
        $user_type = array(
            self::$userTypeId=>'普通会员',
            self::$adminTypeId=>'管理员',
            self::$procurementTypeId=>'企业会员',
        );
        return $user_type[$id];
    }
    //用户状态
    public function userStatus($id){
        $user_status = array(
            self::$userStatusNormalId => '启动',
            self::$userStatusStopId => '暂停',
            self::$userStatusAuditId => '审核中',
        );
        return $user_status[$id];
    }
    //添加用户信息
    public function addUser($data, $isGetId = false, $reSql = false)
    {
        if (!$data || !is_array($data)) return '数据有误！';
        if ($this->checkUserInfoBeUsed('u_phone="'.$data['u_phone'].'"') != 'ok') {
            return '该手机已存在！';
        }
        if ($this->checkUserInfoBeUsed('u_name="'.$data['u_name'].'"') != 'ok') {
            return '该账号名称已存在！';
        }

        $mytime = myTime();
        $data['u_ip'] = Request::instance()->ip();
        $data['u_addtime'] = $mytime;

        $res = $this->allowField($this->allowFields)->create($data);
        if ($reSql) return $res;
        return $isGetId ? $res->u_id : 'ok';
    }
    //更新用户信息
    public function upUser($uid, $data, $cache = 86400, $reSql = false)
    {
        if (!$uid || !$data || !is_array($data)) return '缺少数据！';

        //判断修改的手机号是否被他人所注册，是则返回
        if (!empty($data['u_phone']) && $this->checkUserInfoBeUsed('u_phone="'.$data['u_phone'].'" AND u_id !='.$uid) != 'ok') {
            return '该手机已存在！';
        }
        //禁止改动用户ID
        if (!empty($data['u_id'])) return '禁止改动用户ID';

        $where = 'u_id='.$uid;
        //获取用户旧数据
        $oldData = $this->getUserInfoByWhere($where);
        //开始更新数据
        $res = $this->where($where)->cache('userInfo'.$uid,$cache)->fetchsql($reSql)->update($data);
        if ($res){
            //获取用户新数据
            $newData = $this->getUserInfoByWhere($where);
            //添加编辑用户日志
            $this->addEditUserInfoLog($uid,$oldData,$newData);
            //重置用户session数据
            $this->resetUserSessionInfo($uid,$data);
            return 'ok';
        } else {
            return '编辑失败！';
        }
    }
  public function getUserIdByPhone($phone){
    $reSql = false;
  return  $this->where(['u_phone'=>$phone])->select();
  }
  public function getPhoneByUsername($username){
    $reSql = false;
  return  $this->where(['u_name'=>$username])->field('u_phone')->fetchsql($reSql)->find();
  }
    //获取用户个人信息 单条
    public function getUserInfoByWhere($where, $fields = '*', $reSql = false)
    {
        return $this->where($where)->field($fields)->fetchsql($reSql)->find();
    }
    //获取用户个人某一字段信息
    public function getUserInfoByField($where, $field, $reSql = false)
    {
        $res = $this->getUserInfoByWhere($where, $field, $reSql);
        return $res ? $res[$field] : '';
    }
    //获取用户分页信息
    public function getUserPage($where, $fields = '*', $pageNum = 10, $order = 'u_addtime DESC', $query = [], $reSql = false)
    {
        $res = $this->where($where)->field($fields)->order($order)->fetchsql($reSql)->paginate($pageNum,false,$query)->each(
            function($vlist)
            {
                $vlist['u_type_name'] = isset($vlist['u_type']) ? $this->userType($vlist['u_type']) : '';
                if (isset($vlist['u_status'])) $vlist['u_status'] = $this->userStatus($vlist['u_status']);
                if (empty($vlist['u_name'])) $vlist['u_name'] = $vlist['u_nick'] ? $vlist['u_nick'] : $vlist['u_phone'];
            });
        return $res;
    }

    //检测用户信息（手机号或账户）是否被注册
    public function checkUserInfoBeUsed($where) {
        return $this->where($where)->count() != 0 ? '' : 'ok';
    }

    //添加编辑用户日志
    public function addEditUserInfoLog($uid,$oldData,$newData) {
        if (!$uid || empty($oldData) || empty($newData)) return false;
        $data = array(
            'l_uid'=>$uid,
            'l_username'=>$this->getUserInfoByField('u_id='.$uid,'u_name'),
            'l_olddata'=>json_encode($oldData,JSON_UNESCAPED_UNICODE),
            'l_newdata'=>json_encode($newData,JSON_UNESCAPED_UNICODE),
            'l_addtime'=> myTime(),
        );
        return model('UserLog')->save($data);
    }
    //重置用户session数据
    public function resetUserSessionInfo($uid,$data)
    {
        if (!session('?userInfo') && session('userInfo.userId') != $uid) return false;
        if (isset($data['u_name'])) session('userInfo.userName',$data['u_name']);
        if (isset($data['u_phone'])) session('userInfo.userPhone',$data['u_phone']);
        if (isset($data['u_uheadimg'])) session('userInfo.userImg',$data['u_uheadimg']);
    }

    /**
     * [isExistsOpenId openid是否存在]
     * @param  [string]  $openid [openid]
     * @return boolean         [返回布尔值]
     */
    public static function isExistsOpenId($openid) {
        return self::where('openid',$openid)->find();
    }

    public function distributor() {
        return $this->hasMany('app\common\model\Distributor','d_user_id');
    }

    /*
     *  =================================================================================
     *                              用户信息关联部分
     *  =================================================================================
     */

    //关联用户修改日志
    public function userLog($fields = '*')
    {
        return $this->hasMany('UserLog','l_uid')->bind($fields);
    }
    //关联用户评论
    public function userComment($fields = '*')
    {
        return $this->hasMany('UserComment','a_uid')->bind($fields);
    }
    //关联用户地址
    public function userAddress($fields = '*') 
    {
        return $this->hasMany('UserAddress','a_uid')->bind($fields);
    }
    //关联用户登录日志
    public function userLoginLog()
    {
        return $this->hasMany('UserLoginLog','l_uid');
    }
    //关联用户账户财产
    public function userAccount($fields = '*')
    {
        return $this->hasOne('UserAccount','a_uid')->bind($fields);
    }
    //关联用户企业
    public function userCompany()
    {
        return $this->hasOne('Company','c_uid');
    }

}