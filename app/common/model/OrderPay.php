<?php
/**
 * 订单
 * User: Skuya
 * Date: 2018/8/14
 * Time: 15:26
 */

namespace app\common\model;

use think\Exception;
use think\Model;
use app\common\model\Distributor as DistributorModel;
use app\common\WeChat\Payment;
use think\Db;
use app\common\model\DistributorAccount as DistributorAccountModel;
use app\common\model\DistributorAccountDetail as DistributorAccountDetailModel;
use app\common\model\Config as ConfigModel;
use app\common\model\DistributorEquipment as DistributorEquipmentModel;
use app\common\model\EquipmentPassword as EquipmentPasswordModel;
use app\common\model\DistributorPrecentage as DistributorPrecentageModel;
use app\common\model\EquipmentLastPsd as EquipmentLastPsdModel;
use app\common\model\EquipmentPsdSet as EquipmentPsdSetModel;
use think\facade\Env;

class OrderPay extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'de_order_pay';
    protected $pk = 'o_id';
    protected $autoWriteTimestamp = true;

    const TYPE_ONE = 1;
    const TYPE_TWO = 2;
    const TYPE_THREE = 3;

    //订单状态
    const STATUS_OK = 1;
    const STATUS_USER = -1;
    const STATUS_SALES = -2;
    const STATUS_PLATFORM = 0;

    /**
     * [createOutTradeNo 订单编号]
     * @return [string] [订单编号]
     */
    public static function createOutTradeNo()
    {

        return time() . rand(1000, 9999);
    }

    public static function getEquTimeLong($value)
    {
        $configInfo = db('config')->where('c_id=1')->find();
        switch ($value) {
            case 1:
                $type = $configInfo['c_describe1'];
                break;
            case 2:
                $type = $configInfo['c_describe2'];
                break;
            case 3:
                $type = $configInfo['c_describe3'];
                break;
        }
        return $type;
    }

    /**
     * 创建待支付单据
     * @param unknown $type 1一级类型，2二级类型，3三级类型
     * @param unknown $pay_money
     */
    public static function createPayment($type, $disEquipment, $userInfo)
    {
        if (!$type || empty($disEquipment) || empty($userInfo)) {
            return false;
        }

        Db::startTrans();
        try {
            $money = db('cash_pledge')->where("id =1")->find();
            $data = array();
            $data['o_number'] = self::createOutTradeNo($type);
            $data['o_type'] = $type;
            $data['o_price'] = ConfigModel::getSystemPrice($type);
            $data['o_dis_id'] = $disEquipment['d_dis_id'];
            $data['o_dis_name'] = $disEquipment['d_dis_name'];
            $data['o_dis_phone'] = $disEquipment['d_dis_principal_phone'];
            $data['o_dis_equipment_id'] = $disEquipment['d_id'];    //分销商设备索引ID
            $data['o_dis_equipment_number'] = $disEquipment['d_equipment_number'];
            $data['o_equipment_password'] = '';
            $data['o_uid'] = $userInfo->u_id;
            $data['o_uname'] = $userInfo->u_nick;
            $data['o_uphone'] = $userInfo->u_phone;
            $data['o_equipment_address'] = DistributorModel::find($disEquipment['d_dis_id'])['d_addresss'];
            $data['yajin'] = 6;
            $data = self::create($data);
            //log_result('dede.txt','$data:'.json_encode($data,JSON_UNESCAPED_UNICODE));
            if ($data) {
                $d = [
                    'dis_id'  => $disEquipment['d_dis_id'],
                    'user_id' => db('distributor')->where('d_id=' . $disEquipment['d_dis_id'])->value('d_user_id')
                ];
                //log_result('dede.txt','$d:'.json_encode($d,JSON_UNESCAPED_UNICODE));
                //没有分销商帐户信息 则添加
                $model = DistributorAccountModel::where($d)->find();
                if (empty($model)) DistributorAccountModel::create($d);
                //log_result('dede.txt','$model:'.json_encode($model,JSON_UNESCAPED_UNICODE));
            }

            if ($userInfo->u_id == 252 || $userInfo->u_id == 228) {
                $data->o_price = 0.01;
            }
            $result = false;
            if (!empty($data)) {
                $body = '共享充电';
                $notifyUrl = config('notifyUrl');
                $result = Payment::miniPay($body, $data->o_number, $data->o_price, $notifyUrl, $userInfo);
            }
            //log_result('dede.txt','$result:'.json_encode($result,JSON_UNESCAPED_UNICODE));
            if ($result) {
                Db::commit();
                return $result;
            }
            Db::rollback();
        } catch (\Exception $e) {
            Db::rollback();
            //log_result('dede.txt','$e:'.$e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * [handlePaySuccess 支付回调后数据处理]
     * @param  [array] $result [数据]
     * @return [bool]         [description]
     */
    public static function handlePaySuccess($result)
    {
        //log_result(Env::get('root_path').'upload/dede.txt','1111');
        if (empty($result) || !is_array($result)) {
            return '数据有误';
        }
        $out_trade_no = $result['out_trade_no'];
        Db::startTrans();
        try {
            $model = new self();
            $model = $model::where('o_number', $out_trade_no)->find();
            if ($model->pay_status == 1) {
                return '已支付';
            }
            $model->o_line_number = $result['transaction_id'];
            $model->pay_status = 1; //支付成功状态
            $model->o_paytime = $_SERVER['REQUEST_TIME'];
            $model->o_starttime = $_SERVER['REQUEST_TIME']; //开始计时的时间
            //$model->o_equipment_pwd_prefix = self::handlePwdRule($model->o_dis_equipment_id);
            //结束时间
            $timer = ConfigModel::typeTransfer($model->o_type);
            $model->o_endtime = strtotime("+" . $timer . ' hours');
            $result = $model->save();
            //log_result(Env::get('root_path').'upload/dede.txt','fefe:::'.json_encode($result,JSON_UNESCAPED_UNICODE));
            if ($result) {
                //获取一线的分销商ID
                $disId = $model->o_dis_id;
                //获取上级分销商ID
                $parentDisId = DistributorModel::where('d_id=' . $disId)->value('d_parent_id');
                //如果有上级分销商，则获取上上级分销商ID
                $pParentDisId = $parentDisId ? DistributorModel::where('d_id=' . $parentDisId)->value('d_parent_id') : '';
                //return $disId.':'.$parentDisId.':'.$pParentDisId;

                //如果有上级分销商，获取抽成比例:首先获取上级分销商是否对自己设置独立的抽成，如果没有则获取上级设置的统一抽成
                //计算上级抽成金额以及自己所有的收益
                //如果没有上级分销商，则说明分销商上级就是平台
                $money = 0;//一线分销商所获的收益
                $parentMoney = 0;//一线分销商的上级分销商的抽成金额
                $pParentMoney = 0;//一线分销商的上上级分销商的抽成金额
                $systemMoney = 0;//平台抽成金额
                $o_price = $model->o_price;
                if ($parentDisId) {//存在上级分销商时
                    //获取上级分销商是否对自己设置独立的抽成
                    $precentageInfo = self::getPrecentageInfo($disId, $parentDisId);
                    //如果上级没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                    if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上级没有设置抽成比例，或抽成设置为零时
                        //获取上上级分销商的抽成比例
                        if ($pParentDisId) {//存在上上级分销商时
                            $precentageInfo = self::getPrecentageInfo($parentDisId, $pParentDisId);
                            //如果上上级分销商没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                            if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上上级分销商没有设置或设置为零时，获取平台的抽成比例
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = empty($precentageInfo['percentage']) ? $o_price : bcsub($o_price, $systemMoney, 2);
                            } else {//上上级分销商设置了，则上上级分销商抽成比例
                                $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = bcsub($o_price, $pParentMoney, 2);
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $pParentMoney, 2);
                                $pParentMoney = bcsub($pParentMoney, $systemMoney, 2);
                            }
                        } else {//不存在上上级分销商时，直接获取平台的抽成比例
                            $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                            //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                            $systemMoney = empty($precentageInfo['percentage']) ? $o_price : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                            $money = bcsub($o_price, $systemMoney, 2);
                        }
                    } else {//上级设置抽成比例时
                        $parentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $money = bcsub($o_price, $parentMoney, 2);
                        //如果有上上级分销商，在上级抽成金额的基础上，计算上上级对上级的抽成金额
                        if ($pParentDisId) {
                            $precentageInfo = self::getPrecentageInfo($parentDisId, $pParentDisId);
                            //如果上上级分销商没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                            if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上上级分销商没有设置或设置为零时，获取平台的抽成比例
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $parentMoney, 2);
                                $parentMoney = bcsub($parentMoney, $systemMoney, 2);
                            } else {//上上级分销商设置了，则计算上上级分销商抽成金额
                                $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $parentMoney, 2);
                                $parentMoney = bcsub($parentMoney, $pParentMoney, 2);
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $pParentMoney, 2);
                                $pParentMoney = bcsub($pParentMoney, $systemMoney, 2);
                            }
                        } else {//如果没有上上级分销商时，获取平台抽成比例
                            $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                            $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $parentMoney, 2);
                            $parentMoney = bcsub($parentMoney, $systemMoney, 2);
                        }
                    }
                } else {//不存在上级分销商时，也不会存在上上级分销商了，说明只存在平台了
                    $precentageInfo = self::getPrecentageInfo($disId, 0);
                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                    $money = bcsub($o_price, $systemMoney, 2);

                }
                //处理一线分销商账户余额
                if ($money && !self::setDisAccount($disId, 0, $money, DistributorAccountDetailModel::OPRATION_WAY_ENEARNING, $out_trade_no)) Db::rollback();
                //处理上级分销商佣金,账户
                if ($parentDisId && $parentMoney && !self::setDisAccount($parentDisId, $disId, $parentMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $out_trade_no)) Db::rollback();
                //处理上上级分销商佣金,账户
                $parentDisId = $parentMoney ? $parentDisId : $disId;
                if ($pParentDisId && $pParentMoney && !self::setDisAccount($pParentDisId, $parentDisId, $pParentMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $out_trade_no)) Db::rollback();
                //处理平台得到的佣金,账户
                $pParentDisId = $pParentMoney ? $pParentDisId : ($parentMoney ? $parentDisId : $disId);
                if ($systemMoney && !self::setDisAccount(0, $pParentDisId, $systemMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $out_trade_no)) Db::rollback();
                Db::commit();
                return $result;
            }
            Db::rollback();
        } catch (\Exception $e) {
            Db::rollback();
            echo $e->getMessage();
        }
    }

    /**
     * [handlePaySuccess 支付回调后数据处理]
     * @param  [array] $result [数据]
     * @return [bool]         [description]
     */
    public static function handleOrderPay1($orderNumber, $onlineNumber)
    {
        if (!$orderNumber || !$onlineNumber) return '数据有误';
        Db::startTrans();
        try {
            $model = new self();
            $model = $model::where('o_number', $orderNumber)->find();
            $eNumber = $model->o_dis_equipment_number;
            $psdId = $model->o_psd_set_id;
            $eDisId = $model->o_dis_id;
            $psd_value = $model->o_equipment_pwd;
            if ($model->pay_status == 1) return '已支付';
            $model->o_line_number = $onlineNumber;
            $model->pay_status = 1; //支付成功状态
            $model->o_paytime = $_SERVER['REQUEST_TIME'];
            $model->o_starttime = $_SERVER['REQUEST_TIME']; //开始计时的时间
            //$model->o_equipment_pwd_prefix = self::handlePwdRule($model->o_dis_equipment_id);
            //结束时间
            $p_time = EquipmentPsdSetModel::where('p_id=' . $model->o_type)->value('p_time');
            $model->o_endtime = strtotime("+" . $p_time . ' hours');
            $result = $model->save();
            //log_result('de.txt','fefe:::'.json_encode($result,JSON_UNESCAPED_UNICODE));
            if ($result) {
                //更新密码
                $lastPsdWhere = 'p_equipments_number="' . $eNumber . '" AND p_psd_set_id=' . $psdId . ' AND p_dis_id=' . $eDisId;
                if (EquipmentLastPsdModel::where($lastPsdWhere)->count() > 0) {
                    if (!EquipmentLastPsdModel::where($lastPsdWhere)->update(['p_psd_value' => $psd_value])) {
                        Db::rollback();
                        return false;
                    }
                } else {
                    $newData = [
                        'p_dis_id'            => $eDisId,
                        'p_psd_set_id'        => $psdId,
                        'p_psd_value'         => $psd_value,
                        'p_equipments_number' => $eNumber,
                        'p_addtime'           => myTime()
                    ];
                    $equipmentModle = new EquipmentLastPsdModel();
                    if (!$equipmentModle->insert($newData)) {
                        Db::rollback();
                        return false;
                    }
                }

                //获取上级分销商ID
                $parentDisId = DistributorModel::where('d_id=' . $eDisId)->value('d_parent_id');
                //如果有上级分销商，则获取上上级分销商ID
                $pParentDisId = $parentDisId ? DistributorModel::where('d_id=' . $parentDisId)->value('d_parent_id') : '';

                //如果有上级分销商，获取抽成比例:首先获取上级分销商是否对自己设置独立的抽成，如果没有则获取上级设置的统一抽成
                //计算上级抽成金额以及自己所有的收益
                //如果没有上级分销商，则说明分销商上级就是平台
                $money = 0;//一线分销商所获的收益
                $parentMoney = 0;//一线分销商的上级分销商的抽成金额
                $pParentMoney = 0;//一线分销商的上上级分销商的抽成金额
                $systemMoney = 0;//平台抽成金额
                $o_price = $model->o_price;
                if ($parentDisId) {//存在上级分销商时
                    //获取上级分销商是否对自己设置独立的抽成
                    //log_result('de.txt',1111111);
                    $precentageInfo = $precentageInfo1 = self::getPrecentageInfo($eDisId, $parentDisId);
                    //如果上级没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                    if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上级没有设置抽成比例，或抽成设置为零时
                        //log_result('de.txt',2222222);
                        //获取上上级分销商的抽成比例
                        if ($pParentDisId) {//存在上上级分销商时
                            $precentageInfo = self::getPrecentageInfo($parentDisId, $pParentDisId);
                            //如果上上级分销商没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                            if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上上级分销商没有设置或设置为零时，获取平台的抽成比例
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = empty($precentageInfo['percentage']) ? $o_price : bcsub($o_price, $systemMoney, 2);
                            } else {//上上级分销商设置了，则计算上上级分销商抽成比例
                                $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = bcsub($o_price, $pParentMoney, 2);
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $pParentMoney, 2);
                                $pParentMoney = bcsub($pParentMoney, $systemMoney, 2);
                            }
                        } else {//不存在上上级分销商时，直接获取平台的抽成比例
                            //log_result('de.txt',777777);
                            $precentageInfo = self::getPrecentageInfo(0, $parentDisId);//获取上级对所有下级设置的佣金比例
                            if (($precentageInfo1 && empty($precentageInfo1['percentage'])) || empty($precentageInfo['percentage'])) {//上级没有对所有下级设置佣金比例,或上级没有对对于的下级设置为零时
                                $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = empty($precentageInfo['percentage']) ? $o_price : bcsub($o_price, $systemMoney, 2);
                            } else {//上级有对所有下级设置佣金比例
                                $parentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = bcsub($o_price, $parentMoney, 2);
                                $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $parentMoney, 2);
                                $pParentMoney = bcsub($pParentMoney, $systemMoney, 2);
                            }
                        }
                    } else {//上级设置抽成比例时
                        $parentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $money = bcsub($o_price, $parentMoney, 2);
                        //如果有上上级分销商，在上级抽成金额的基础上，计算上上级对上级的抽成金额
                        if ($pParentDisId) {
                            $precentageInfo = self::getPrecentageInfo($parentDisId, $pParentDisId);
                            //如果上上级分销商没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                            if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上上级分销商没有设置或设置为零时，获取平台的抽成比例
                                //log_result('de.txt',10101010);
                                $precentageInfo = self::getPrecentageInfo(0, $pParentDisId);//上上级设置所有下级的抽成比例
                                if (empty($precentageInfo['percentage'])) {//上上级没有对所有下级抽成比例
                                    $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);//获取平台的抽成比例
                                    //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $parentMoney, 2);
                                    $parentMoney = bcsub($parentMoney, $systemMoney, 2);
                                } else {//上上级有对所有下级抽成比例
                                    $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $parentMoney, 2);
                                    $parentMoney = bcsub($parentMoney, $pParentMoney, 2);
                                    $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                    //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $pParentMoney, 2);
                                    $pParentMoney = bcsub($pParentMoney, $systemMoney, 2);
                                }
                            } else {//上上级分销商设置了，则计算上上级分销商抽成金额
                                $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $parentMoney, 2);
                                $parentMoney = bcsub($parentMoney, $pParentMoney, 2);
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $pParentMoney, 2);
                                $pParentMoney = bcsub($pParentMoney, $systemMoney, 2);
                            }
                        } else {//如果没有上上级分销商时，计算平台对上级的抽佣
                            $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                            $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $parentMoney, 2);
                            $parentMoney = bcsub($parentMoney, $systemMoney, 2);
                        }
                    }
                } else {//不存在上级分销商时，也不会存在上上级分销商了，说明只存在平台了，计算平台对上级的抽佣
                    $precentageInfo = self::getPrecentageInfo($eDisId, 0);
                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                    $money = bcsub($o_price, $systemMoney, 2);
                }
                //处理一线分销商账户余额
                if ($money && $money > 0.00 && !self::setDisAccount($eDisId, 0, $money, DistributorAccountDetailModel::OPRATION_WAY_ENEARNING, $orderNumber)) {
                    Db::rollback();
                    return false;
                }
                //处理上级分销商佣金,账户
                if ($parentDisId && $parentMoney && $parentMoney > 0.00 && !self::setDisAccount($parentDisId, $eDisId, $parentMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
                    Db::rollback();
                    return false;
                }
                //处理上上级分销商佣金,账户
                $parentDisId = $parentMoney ? $parentDisId : $eDisId;
                if ($pParentDisId && $pParentMoney && $pParentMoney > 0.00 && !self::setDisAccount($pParentDisId, $parentDisId, $pParentMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
                    Db::rollback();
                    return false;
                }
                //处理平台得到的佣金,账户
                $pParentDisId = $pParentMoney ? $pParentDisId : ($parentMoney ? $parentDisId : $eDisId);
                if ($systemMoney && $systemMoney > 0.00 && !self::setDisAccount(0, $pParentDisId, $systemMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
                    Db::rollback();
                    return false;
                }
                //              log_result('de.txt',190190190);
                //处理用户押金的金额
                if ($model->yajin > 0) {
                    Db::name('user')
                        ->where("u_id = '$model->o_uid'")
                        ->data(['u_money' => $model->yajin])
                        ->update();


                    //                 log_result('de.txt',200200200);
                    //写入押金日志
                    $data = [
                        'o_line_number'          => $model->o_line_number,
                        'o_number'               => $orderNumber,
                        'o_price'                => $model->yajin,
                        'o_type'                 => $model->o_type,
                        'o_dis_id'               => $model->o_dis_id,
                        'o_dis_name'             => $model->o_dis_name,
                        'o_dis_phone'            => $model->o_dis_phone,
                        'o_dis_equipment_id'     => $model->o_dis_equipment_id,
                        'o_dis_equipment_number' => $model->o_dis_equipment_number,
                        'o_uid'                  => $model->o_uid,
                        'o_uname'                => $model->o_uname,
                        'o_uphone'               => $model->o_uphone,
                        'o_paytime'              => $model->o_paytime
                    ];
                    Db::name('yajin_pay')->insert($data);


                }
                Db::commit();
                return $result;
            }
            Db::rollback();
        } catch (\Exception $e) {
            Db::rollback();
            return $e->getMessage();
        }
    }

    public static function handleOrderPay($orderNumber, $onlineNumber)
    {
        if (!$orderNumber || !$onlineNumber) return '数据有误';
        Db::startTrans();
        try {
            $model = new self();
            $model = $model::where('o_number', $orderNumber)->find();
            $eNumber = $model->o_dis_equipment_number;
            $psdId = $model->o_psd_set_id;
            $eDisId = $model->o_dis_id;
            $psd_value = $model->o_equipment_pwd;
            if ($model->pay_status == 1) return '已支付';
            $model->o_line_number = $onlineNumber;
            $model->pay_status = 1; //支付成功状态
            $model->o_paytime = $_SERVER['REQUEST_TIME'];
            $model->o_starttime = $_SERVER['REQUEST_TIME']; //开始计时的时间
            //$model->o_equipment_pwd_prefix = self::handlePwdRule($model->o_dis_equipment_id);
            //结束时间
            $pinfo = EquipmentPsdSetModel::where('p_id=' . $model->o_type)->find();


            if (strpos($pinfo['p_text'], '分') !== false) {
                $model->o_endtime = strtotime("+" . $pinfo['p_time'] . ' minute');
            } else {
                $model->o_endtime = strtotime("+" . $pinfo['p_time'] . ' hours');
            }


            $result = $model->save();

            if ($result) {
                //更新密码
                $lastPsdWhere = 'p_equipments_number="' . $eNumber . '" AND p_psd_set_id=' . $psdId . ' AND p_dis_id=' . $eDisId;
                if (EquipmentLastPsdModel::where($lastPsdWhere)->count() > 0) {

                    if (!EquipmentLastPsdModel::where($lastPsdWhere)->update(['p_psd_value' => $psd_value])) {

                        //   log_result('pay.txt','getLastSql:::'.Db::getLastSql());
                        //   Db::rollback();
                        //   return false;
                    }
                } else {
                    $newData = [
                        'p_dis_id'            => $eDisId,
                        'p_psd_set_id'        => $psdId,
                        'p_psd_value'         => $psd_value,
                        'p_equipments_number' => $eNumber,
                        'p_addtime'           => myTime()
                    ];
                    $equipmentModle = new EquipmentLastPsdModel();
                    if (!$equipmentModle->insert($newData)) {
                        Db::rollback();
                        return false;
                    }
                }

                //获取上级分销商ID
                $parentDisId = DistributorModel::where('d_id=' . $eDisId)->value('d_parent_id');
                //如果有上级分销商，则获取上上级分销商ID
                $pParentDisId = $parentDisId ? DistributorModel::where('d_id=' . $parentDisId)->value('d_parent_id') : '';

                //如果有上级分销商，获取抽成比例:首先获取上级分销商是否对自己设置独立的抽成，如果没有则获取上级设置的统一抽成
                //计算上级抽成金额以及自己所有的收益
                //如果没有上级分销商，则说明分销商上级就是平台
                $money = 0;//一线分销商所获的收益
                $parentMoney = 0;//一线分销商的上级分销商的抽成金额
                $pParentMoney = 0;//一线分销商的上上级分销商的抽成金额
                $systemMoney = 0;//平台抽成金额
                $o_price = $model->o_price;
                if ($parentDisId) {//存在上级分销商时
                    //获取上级分销商是否对自己设置独立的抽成
                    $precentageInfo = $precentageInfo1 = self::getPrecentageInfo($eDisId, $parentDisId);
                    //如果上级没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                    if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上级没有设置抽成比例，或抽成设置为零时
                        //获取上上级分销商的抽成比例
                        if ($pParentDisId) {//存在上上级分销商时
                            $precentageInfo = self::getPrecentageInfo($parentDisId, $pParentDisId);
                            //如果上上级分销商没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                            if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上上级分销商没有设置或设置为零时，获取平台的抽成比例
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = empty($precentageInfo['percentage']) ? $o_price : bcsub($o_price, $systemMoney, 2);
                            } else {//上上级分销商设置了，则计算上上级分销商抽成比例
                                $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);//平台对
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = bcsub($o_price, bcadd($systemMoney, $pParentMoney, 2), 2);
                            }
                        } else {//不存在上上级分销商时，直接获取平台的抽成比例
                            $precentageInfo = self::getPrecentageInfo(0, $parentDisId);//获取上级对所有下级设置的佣金比例
                            if (($precentageInfo1 && empty($precentageInfo1['percentage'])) || empty($precentageInfo['percentage'])) {//上级没有对所有下级设置佣金比例,或上级没有对对于的下级设置为零时
                                $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                                //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = empty($precentageInfo['percentage']) ? $o_price : bcsub($o_price, $systemMoney, 2);
                            } else {//上级有对所有下级设置佣金比例
                                $parentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = bcsub($o_price, bcadd($systemMoney, $parentMoney, 2), 2);
                            }
                        }
                        log_result('pay.txt', '3333333333');
                    } else {//上级设置抽成比例时
                        $parentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        //如果有上上级分销商，在上级抽成金额的基础上，计算上上级对上级的抽成金额
                        if ($pParentDisId) {
                            $precentageInfo = self::getPrecentageInfo($parentDisId, $pParentDisId);
                            //如果上上级分销商没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                            if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上上级分销商没有设置或设置为零时，获取平台的抽成比例
                                $precentageInfo = self::getPrecentageInfo(0, $pParentDisId);//上上级设置所有下级的抽成比例
                                if (empty($precentageInfo['percentage'])) {//上上级没有对所有下级抽成比例
                                    $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);//获取平台的抽成比例
                                    /*//如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100,2), $parentMoney, 2);
                                    $parentMoney = bcsub($parentMoney, $systemMoney, 2);*/
                                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                    $money = bcsub($o_price, bcadd($systemMoney, $parentMoney, 2), 2);
                                } else {//上上级有对所有下级抽成比例
                                    $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                    $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                    //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                    $money = bcsub($o_price, bcadd($systemMoney, bcadd($pParentMoney, $parentMoney, 2), 2), 2);
                                }
                            } else {//上上级分销商设置了，则计算上上级分销商抽成金额
                                $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                                $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                                $money = bcsub($o_price, bcadd($systemMoney, bcadd($pParentMoney, $parentMoney, 2), 2), 2);
                            }
                        } else {//如果没有上上级分销商时，计算平台对上级的抽佣
                            $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                            $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                            $money = bcsub($o_price, bcadd($systemMoney, $parentMoney, 2), 2);
                        }

                        log_result('pay.txt', '44444444444');
                    }
                } else {//不存在上级分销商时，也不会存在上上级分销商了，说明只存在平台了，计算平台对上级的抽佣
                    $precentageInfo = self::getPrecentageInfo($eDisId, 0);
                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                    $money = bcsub($o_price, $systemMoney, 2);
                }
                //处理一线分销商账户余额
                if ($money && $money > 0.00 && !self::setDisAccount($eDisId, 0, $money, DistributorAccountDetailModel::OPRATION_WAY_ENEARNING, $orderNumber)) {
                    Db::rollback();

                    log_result('pay.txt', '555555555555');
                    return false;
                }
                //处理上级分销商佣金,账户
                if ($parentDisId && $parentMoney && $parentMoney > 0.00 && !self::setDisAccount($parentDisId, $eDisId, $parentMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
                    Db::rollback();

                    log_result('pay.txt', '+6666666666');
                    return false;
                }
                //处理上上级分销商佣金,账户
                $parentDisId = $parentMoney ? $parentDisId : $eDisId;
                if ($pParentDisId && $pParentMoney && $pParentMoney > 0.00 && !self::setDisAccount($pParentDisId, $parentDisId, $pParentMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
                    Db::rollback();

                    log_result('pay.txt', '77777777777777');
                    return false;
                }
                //处理平台得到的佣金,账户
                $pParentDisId = $pParentMoney ? $pParentDisId : ($parentMoney ? $parentDisId : $eDisId);
                if ($systemMoney && $systemMoney > 0.00 && !self::setDisAccount(0, $pParentDisId, $systemMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
                    Db::rollback();
                    log_result('pay.txt', '88888888888888');
                    return false;
                }


                Db::commit();
                return $result;
            }
            Db::rollback();
        } catch (\Exception $e) {
            Db::rollback();
            return $e->getMessage();
        }
    }

    public static function handleYajinTypeMoney($orderInfo)
    {
        $eDisId = $orderInfo['o_dis_id'];
        $orderNumber = $orderInfo['o_number'];
        //获取上级分销商ID
        $parentDisId = DistributorModel::where('d_id=' . $eDisId)->value('d_parent_id');
        //如果有上级分销商，则获取上上级分销商ID
        $pParentDisId = $parentDisId ? DistributorModel::where('d_id=' . $parentDisId)->value('d_parent_id') : '';

        //如果有上级分销商，获取抽成比例:首先获取上级分销商是否对自己设置独立的抽成，如果没有则获取上级设置的统一抽成
        //计算上级抽成金额以及自己所有的收益
        //如果没有上级分销商，则说明分销商上级就是平台
        $money = 0;//一线分销商所获的收益
        $parentMoney = 0;//一线分销商的上级分销商的抽成金额
        $pParentMoney = 0;//一线分销商的上上级分销商的抽成金额
        $systemMoney = 0;//平台抽成金额
        $o_price = $orderInfo['o_price'];
        if ($parentDisId) {//存在上级分销商时
            //获取上级分销商是否对自己设置独立的抽成
            $precentageInfo = $precentageInfo1 = self::getPrecentageInfo($eDisId, $parentDisId);
            //如果上级没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
            if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上级没有设置抽成比例，或抽成设置为零时
                //获取上上级分销商的抽成比例
                if ($pParentDisId) {//存在上上级分销商时
                    $precentageInfo = self::getPrecentageInfo($parentDisId, $pParentDisId);
                    //如果上上级分销商没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                    if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上上级分销商没有设置或设置为零时，获取平台的抽成比例
                        $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                        //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                        $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $money = empty($precentageInfo['percentage']) ? $o_price : bcsub($o_price, $systemMoney, 2);
                    } else {//上上级分销商设置了，则计算上上级分销商抽成比例
                        $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);//平台对
                        $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $money = bcsub($o_price, bcadd($systemMoney, $pParentMoney, 2), 2);
                    }
                } else {//不存在上上级分销商时，直接获取平台的抽成比例
                    $precentageInfo = self::getPrecentageInfo(0, $parentDisId);//获取上级对所有下级设置的佣金比例
                    if (($precentageInfo1 && empty($precentageInfo1['percentage'])) || empty($precentageInfo['percentage'])) {//上级没有对所有下级设置佣金比例,或上级没有对对于的下级设置为零时
                        $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                        //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                        $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $money = empty($precentageInfo['percentage']) ? $o_price : bcsub($o_price, $systemMoney, 2);
                    } else {//上级有对所有下级设置佣金比例
                        $parentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                        $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $money = bcsub($o_price, bcadd($systemMoney, $parentMoney, 2), 2);
                    }
                }
            } else {//上级设置抽成比例时
                $parentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                //如果有上上级分销商，在上级抽成金额的基础上，计算上上级对上级的抽成金额
                if ($pParentDisId) {
                    $precentageInfo = self::getPrecentageInfo($parentDisId, $pParentDisId);
                    //如果上上级分销商没有设置抽成比例，或抽成设置为零，则获取平台的抽成比例
                    if (!$precentageInfo || empty($precentageInfo['percentage'])) {//上上级分销商没有设置或设置为零时，获取平台的抽成比例
                        $precentageInfo = self::getPrecentageInfo(0, $pParentDisId);//上上级设置所有下级的抽成比例
                        if (empty($precentageInfo['percentage'])) {//上上级没有对所有下级抽成比例
                            $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);//获取平台的抽成比例
                            /*//如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                            $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100,2), $parentMoney, 2);
                            $parentMoney = bcsub($parentMoney, $systemMoney, 2);*/
                            $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                            $money = bcsub($o_price, bcadd($systemMoney, $parentMoney, 2), 2);
                        } else {//上上级有对所有下级抽成比例
                            $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                            $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                            //如平台也没有设置抽成比例或者设置为0，则一线分销商直接等价获取收益，否则按最总抽成比例折算收益
                            $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                            $money = bcsub($o_price, bcadd($systemMoney, bcadd($pParentMoney, $parentMoney, 2), 2), 2);
                        }
                    } else {//上上级分销商设置了，则计算上上级分销商抽成金额
                        $pParentMoney = bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $precentageInfo = self::getPrecentageInfo($pParentDisId, 0);
                        $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                        $money = bcsub($o_price, bcadd($systemMoney, bcadd($pParentMoney, $parentMoney, 2), 2), 2);
                    }
                } else {//如果没有上上级分销商时，计算平台对上级的抽佣
                    $precentageInfo = self::getPrecentageInfo($parentDisId, 0);
                    $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
                    $money = bcsub($o_price, bcadd($systemMoney, $parentMoney, 2), 2);
                }

            }
        } else {//不存在上级分销商时，也不会存在上上级分销商了，说明只存在平台了，计算平台对上级的抽佣
            $precentageInfo = self::getPrecentageInfo($eDisId, 0);
            $systemMoney = empty($precentageInfo['percentage']) ? 0 : bcmul(bcdiv($precentageInfo['percentage'], 100, 2), $o_price, 2);
            $money = bcsub($o_price, $systemMoney, 2);
        }
        //处理一线分销商账户余额
        if ($money && $money > 0.00 && !self::setDisAccount($eDisId, 0, $money, DistributorAccountDetailModel::OPRATION_WAY_ENEARNING, $orderNumber)) {
            throw new Exception("押金订单分销出错");
        }
        //处理上级分销商佣金,账户
        if ($parentDisId && $parentMoney && $parentMoney > 0.00 && !self::setDisAccount($parentDisId, $eDisId, $parentMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
            throw new Exception("押金订单分销出错");
        }
        //处理上上级分销商佣金,账户
        $parentDisId = $parentMoney ? $parentDisId : $eDisId;
        if ($pParentDisId && $pParentMoney && $pParentMoney > 0.00 && !self::setDisAccount($pParentDisId, $parentDisId, $pParentMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
            throw new Exception("押金订单分销出错");
        }
        //处理平台得到的佣金,账户
        $pParentDisId = $pParentMoney ? $pParentDisId : ($parentMoney ? $parentDisId : $eDisId);
        if ($systemMoney && $systemMoney > 0.00 && !self::setDisAccount(0, $pParentDisId, $systemMoney, DistributorAccountDetailModel::OPRATION_WAY_YONGJIN, $orderNumber)) {
            throw new Exception("押金订单分销出错");
        }
    }

    //获取一线上级设置的抽成比例
    public static function getPrecentageInfo($disId, $parentDisId)
    {
        //获取平台是否独立设置了一线的分销商
        $precentageInfo = DistributorPrecentageModel::where('dis_id=' . $parentDisId . ' AND asgd_dis_id=' . $disId)->find();
        if ($parentDisId == 0 && empty($precentageInfo['percentage'])) $precentageInfo = DistributorPrecentageModel::where('dis_id=0 AND asgd_dis_id=0')->find();//获取平台设置的全局抽成比例
        //if (!$precentageInfo) $precentageInfo = DistributorPrecentageModel::where('dis_id='.$parentDisId)->find();//没有设置一线的分销商，则获取平台设置所有的抽成比例
        return $precentageInfo;
    }

    //设置分销商的账户
    public static function setDisAccount($disId, $targetId, $money, $opration_way, $orderNumber)
    {
        //入帐分销商个人帐户表
        $disData = DistributorAccountModel::where('dis_id=' . $disId)->order('id', 'DESC')->find();
        $user_id = DistributorModel::where('d_id=' . $disId)->value('d_user_id');
        //没有数据时，添加;有数据时，更新
        if (!empty($disData)) {
            $data = array(
                'income'  => bcadd($disData->income, $money, 2),
                'balance' => bcadd($disData->balance, $money, 2)
            );
            if (!$disData['user_id'] && $disData['user_id'] != 0) $data['user_id'] = $user_id;
            if (!DistributorAccountModel::where('dis_id', $disId)->update($data)) return false;
        } else {
            $data = array(
                'dis_id'  => $disId,
                'user_id' => $user_id ? $user_id : 0,
                'income'  => $money,
                'balance' => $money,
            );
            if (!DistributorAccountModel::create($data)) return false;
        }
        $target_dis_name = DistributorModel::where('d_id=' . $targetId)->value('d_name');
        //入帐分销商明细表
        $details = array(
            'dis_id'          => $disId,
            'money'           => $money,
            'op_way'          => $opration_way,
            'target_dis_id'   => $targetId,
            'target_dis_name' => $target_dis_name ? $target_dis_name : '消费者',
            'create_time'     => $_SERVER['REQUEST_TIME'],
            'op_type'         => DistributorAccountDetailModel::MONEY_TYPE_OPRATION_ADD,
            'number'          => $orderNumber
        );
        return DistributorAccountDetailModel::create($details);
    }

    /**
     * [handlePwdRule 处理密码规则]
     * @param  [int] $dis_id     [分销商id]
     * @param  [int] $dis_equipment_id     [分销商设备id]
     * @return [string]             [完整密码]
     */
    public static function handlePwdRule($o_dis_equipment_id)
    {
        $model = DistributorEquipmentModel::find($o_dis_equipment_id);

        if (empty($model)) {
            $originpwd = EquipmentPasswordModel::find(1);
            $newPrefix = $originpwd->p_prefix;
        } else {

            if (empty($model->d_equipment_pwd_prefix)) {
                $originpwd = EquipmentPasswordModel::find(1);
                $newPrefix = $originpwd->p_prefix;
            } else {
                $prefix = $model->d_equipment_pwd_prefix;  //密码前缀

                $equ = EquipmentPasswordModel::where('p_prefix', $prefix)->find();   //通过旧的前缀去查找新的前缀
                $id = $equ->p_id;

                $p_model = DB::query("SELECT p_id from de_equipment_possword WHERE p_id = ( SELECT p_id FROM de_equipment_possword WHERE p_id > {$id} ORDER BY p_id ASC LIMIT 1 )");
                if (empty($p_model)) {
                    $newEqu = '';
                } else {
                    $id = $p_model[0]['p_id'];
                    $newEqu = EquipmentPasswordModel::find($id);
                }

                if (empty($newEqu)) {
                    $originpwd = EquipmentPasswordModel::find(1);
                    $newPrefix = $originpwd->p_prefix;
                } else {
                    $newPrefix = $newEqu->p_prefix;
                }
            }
        }

        //保存新的前缀
        //$model->d_equipment_pwd_prefix = $newPrefix;
        db('distributor_equipment')->where('d_id=' . $o_dis_equipment_id)->update(['d_equipment_pwd_prefix' => $newPrefix]);
        return $newPrefix;

    }
}
