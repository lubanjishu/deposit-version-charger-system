<?php
/**
 * 广告类
 * User: jund
 * Date: 2019/3/28
 * Time: 23:23
 */

namespace app\common\model;


use think\Model;

class DistributorBanner extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'de_distributor_banner';
    protected $pk = 'b_id';
}
