<?php
/**
 * 设备最后用过的密码列表
 * User: jund
 * Date: 2019/1/27
 * Time: 21:16
 */

namespace app\common\model;
use think\Model;

class EquipmentLastPsd extends Model
{
    protected $table = 'de_equipment_last_psd';
    protected $pk = 'p_id';
}