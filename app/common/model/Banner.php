<?php
/**
 * 广告轮播图管理
 * User: jund
 * Date: 2018/7/18
 * Time: 11:34
 */
namespace app\common\model;
use think\Model;
class Banner extends Model
{
    protected $table = 'de_banner';
    protected $pk = 'b_id';
    public static $bannerTypePcId = 1;//pc端广告轮播图
    public static $bannerTypeWapId = 2;//wap端广告轮播图
    public function addBanner($data, $isGetId = false, $reSql = false)
    {
        $data['b_addtime'] = myTime();
        $res = $this->create($data);
        if ($reSql) return $res;
        return $res ? ($isGetId ? $res->b_id : 'ok') : '';
    }
    //编辑广告轮播图片信息
    public function upBanner($bid, $data, $expire = 86400, $reSql = false)
    {
        $res = $this->where('b_id=' . $bid)->fetchsql($reSql)->update($data);
        if ($res && !$reSql) {
            //$this->setArticleCache();
        }
        return $res ? 'ok' : '';
    }
    //根据索引ID获取数据 单条
    public function getBannerInfoById($b_id, $fields = '*', $reSql = false)
    {
        return $this->where('b_id='.$b_id)->field($fields)->fetchSql($reSql)->find();
    }
    //根据索引ID获取某一字段数据 单条
    public function getBannerFieldInfo($b_id, $field = 'b_img', $reSql = false)
    {
        $res = $this->getBannerInfoById($b_id, $field, $reSql);
        return $res ? $res[$field] : '';
    }
    //获取广告轮播图
    public function getBannerList($where, $fields = '*', $order = 'b_addtime DESC', $reSql = false)
    {
        return $this->where($where)->field($fields)->order($order)->fetchSql($reSql)->select();
    }

}