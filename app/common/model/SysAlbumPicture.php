<?php
/**
 * 商城商品图片类
 * User: jund
 * Date: 2018/10/24
 * Time: 15:40
 */

namespace app\common\model;


use think\Model;

class SysAlbumPicture extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'sys_album_picture';
    protected $pk = 'pic_id';
}