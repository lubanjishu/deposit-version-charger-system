<?php

namespace app\common\model;

use think\Model;


class DistributorAccountDetail extends Model
{

	// 设置当前模型对应的完整数据表名称
    protected $table = 'de_distributor_account_detail';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;

    CONST OPRATION_WAY_ENEARNING = 1;	//收益
    CONST OPRATION_WAY_GETCASH = 2;		//提现
    CONST OPRATION_WAY_YONGJIN = 3;		//佣金
    CONST OPRATION_WAY_FANGHUAN = 4;		//返还佣金
    CONST OPRATION_WAY_SHOUXUFEI = 5;		//平台手续费
    CONST OPRATION_WAY_TUIKUAN = 6;		//退款

    CONST MONEY_TYPE_OPRATION_ADD = 1;	//加
    CONST MONEY_TYPE_OPRATION_MINUS = 2;  //减
}
