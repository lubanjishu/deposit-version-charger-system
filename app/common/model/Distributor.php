<?php

namespace app\common\model;

use think\Model;
class Distributor extends model
{
	
	// 设置当前模型对应的完整数据表名称
    protected $table = 'de_distributor';
    protected $pk = 'd_id';
	
	public function user() {
		return $this->hasOne('app\common\model\User','u_id','d_user_id');
	}
}
