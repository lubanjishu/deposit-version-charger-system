<?php
/**
 * Created by PhpStorm.
 * User: jund
 * Date: 2019/1/24
 * Time: 0:56
 */

namespace app\common\model;
use think\Model;

class Equipments extends Model
{

    protected $table = 'de_equipments';
    protected $pk = 'e_id';
    //protected $autoWriteTimestamp = true;

    CONST DISTRIBUTED = 2;
    CONST UNDISTRIBUTED = 1;
    CONST DELETEDISTRIBUTED = -1;
}