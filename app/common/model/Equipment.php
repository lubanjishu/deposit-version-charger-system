<?php

/**
 * 佣金表
 */
namespace app\common\model;
use think\Model;


class Equipment extends Model
{
	
	protected $table = 'de_equipment';
    protected $pk = 'e_id';
    //protected $autoWriteTimestamp = true;
    
    CONST DISTRIBUTED = 2;
    CONST UNDISTRIBUTED = 1;
    CONST DELETEDISTRIBUTED = -1;
}