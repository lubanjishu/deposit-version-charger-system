<?php

/**
 * 订单
 * User: Skuya
 * Date: 2018/8/14
 * Time: 15:26
 */

namespace app\common\model;

use think\Model;
use app\common\model\OrderPay as OrderPayModel;
class DistributorAccount extends Model
{
	
	// 设置当前模型对应的完整数据表名称
    protected $table = 'de_distributor_account';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;

    /**
     * [handleDistributorIncome 处理各分销商的当天收益及累计收益]
     * @param  [int] $disEquipment_id [分销商id]
     * @return [array]                  [description]
     */
    public static function handleDistributorIncome($disEquipment_id) {
	
    	/*if ( empty($disEquipment_id) ) {
    		return [0,0];
    	}*/
    	//我的收益
		$disAccount = self::where('dis_id',$disEquipment_id)->field('income')->find(); //累计收益
		$accumulativeIncome = 0;
		if ( !empty($disAccount) )
			$accumulativeIncome = $disAccount->income;	//累计收益
		$start = strtotime(date('Y-m-d').' 00:00:00');
		$end = strtotime(date('Y-m-d').' 23:59:59');
		$current_days_incomes = OrderPayModel::where('o_dis_id',$disEquipment_id)->where('o_paytime','between',array($start,$end))->field('o_price')->select();
		$current_days_income = 0;
		if ( !empty($current_days_incomes) ) {
			foreach ($current_days_incomes as $key => $value) {
				$current_days_income += $value['o_price'];
			}
		}
		return array(
			$current_days_income,$accumulativeIncome
		);	
    }
    
    /**
     * [handle30dayIncome 30天内累计收益]
     * @param  [type] $disEquipment_id [分销商id]
     * @return [type]                  [description]
     */
    public static function handle30dayIncome($disEquipment_id) {
		$start = $_SERVER['REQUEST_TIME'];
		$end = strtotime(" $start -30 day");
		$model = OrderPayModel::where('o_dis_id',$disEquipment_id)->where('o_paytime','between',array($end,$start))->field('o_price')->select();
		$distributor30DayIncome = 0;
		if ( !empty($model) ) {
			foreach ($model as $key => $value) {
				$distributor30DayIncome += $value['o_price'];
			}
		}
		return $distributor30DayIncome;
    }
}