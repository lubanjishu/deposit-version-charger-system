<?php
/**
 * 资讯分类
 * User: jund
 * Date: 2018/9/4
 * Time: 18:13
 */

namespace app\common\model;


use think\Model;

class InformationType extends Model
{
    protected $table = 'de_information_type';
    protected $pk = 't_id';
}