<?php
/**
 * 商城用户类
 * User: jund
 * Date: 2018/9/21
 * Time: 11:11
 */

namespace app\common\model;


use think\Model;

class SysUser extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'sys_user';
    protected $pk = 'uid';
}