<?php
/**
 * 订单
 * User: Skuya
 * Date: 2018/8/14
 * Time: 15:26
 */

namespace app\common\model;

use think\Model;
use app\common\model\Distributor as DistributorModel;
use app\common\WeChat\Payment;
use think\Db;
use app\common\model\DistributorAccount as DistributorAccountModel;
use app\common\model\DistributorAccountDetail as DistributorAccountDetailModel;
use app\common\model\Config as ConfigModel;
use app\common\model\DistributorEquipment as DistributorEquipmentModel;
use app\common\model\EquipmentPassword as EquipmentPasswordModel;
use app\common\model\DistributorPrecentage as DistributorPrecentageModel;
use app\common\model\EquipmentLastPsd as EquipmentLastPsdModel;
use app\common\model\EquipmentPsdSet as EquipmentPsdSetModel;
use think\facade\Env;

/**
 * Class OrderPay
 * @package app\common\model
 * @property $o_id;
 * @property $o_line_number;
 * @property $o_number;
 * @property $o_price;
 * @property $o_type;
 * @property $o_dis_id;
 * @property $o_dis_name;
 * @property $o_dis_phone;
 * @property $o_dis_equipment_id;
 * @property $o_dis_equipment_number;
 * @property $o_uid;
 * @property $pay_status;
 * @property $o_uname;
 * @property $o_paytime;
 * @property $o_endtime;
 * @property $o_psd_set_describe;
 * @property $o_uphone;
 * @property $o_isrefund;
 * @property $o_refund_money;
 */
class OrderPayYajin extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'de_yajin_pay';
    protected $pk = 'o_id';
    protected $autoWriteTimestamp = true;

    /**
     * [createOutTradeNo 订单编号]
     * @return [string] [订单编号]
     */
    public static function createOutTradeNo()
    {

        return time() . rand(1000, 9999);
    }


    public static function handleOrderPayYajin($orderNumber, $onlineNumber)
    {
        if (!$orderNumber || !$onlineNumber) return '数据有误';
        $model = new self();
        $orderInfo = $model->where(['o_number' => $orderNumber])->find();
        log_result('zhifu3.txt', '$result::' . json_encode($orderInfo));

        /**
         * @var $orderInfo OrderPay
         */
        if ($orderInfo->pay_status == 1) {
            return '已支付';
        }
        try{
            Db::startTrans();
            //更新订单数据
            \db('yajin_pay')->where(['o_number' => $orderNumber])->update([
                'o_line_number' => $onlineNumber,
                'pay_status'    => 1,
                'o_paytime'     => time()
            ]);
            //更新用户余额
            $data = \db('user')->where('u_id', $orderInfo->o_uid)->inc('yajin_money', $orderInfo->o_price)->update();

//            log_result('zhifu3.txt', '$result::5' . json_encode($data));
            Db::commit();
        }catch (\Throwable $throwable){
            Db::rollback();
            throw $throwable;
        }

    }

}
