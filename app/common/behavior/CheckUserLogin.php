<?php
namespace app\common\behavior;

use think\Controller;
class CheckUserLogin
{
    use \traits\controller\Jump;//类里面引入jump;类
    //绑定到CheckAuth标签，可以用于检测Session以用来判断用户是否登录
    public function run($params){
        $userCom = controller('common/user');
        $url = '/index.php/login/index';
        switch ($params) {
            case $userCom::$adminTypeId:
                $session_name = 'adminInfo';
                $userName = 'adminAccount';
                $userId = 'adminId';
                $userPassword = 'adminPassword';
                $url = '/myadmin.php/login/index';
                break;
            case $userCom::$userTypeId:
                $session_name = 'userInfo';
                $userName = 'userAccount';
                $userId = 'userId';
                $userPassword = 'userPassword';
                break;
            case $userCom::$procurementTypeId:
                $session_name = 'userInfo';
                $userName = 'userAccount';
                $userId = 'userId';
                $userPassword = 'userPassword';
                break;
            default:
                $session_name = 'userInfo';
                $userName = 'userAccount';
                $userId = 'userId';
                $userPassword = 'userPassword';
                break;
        }
        $userInfo = session($session_name);
        if (empty($userInfo[$userName]) || empty($userInfo[$userId]) || empty($userInfo[$userPassword])) {
            session($session_name,null);
            $this->redirect($url, $params, 3, '页面跳转中~');
            exit;
        }
    }
}
