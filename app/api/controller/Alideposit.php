<?php
/**
 * 阿里免押金demo
 * User: will
 * Date: 2021/4/22
 */

namespace app\api\controller;

use think\facade\Request;
use think\Db;
use think\Controller;
use think\facade\Cache;
use think\facade\Env;

require './vendor/alipay-sdk-php-all-master/aop/AopClient.php';
require './vendor/alipay-sdk-php-all-master/aop/request/AlipayFundAuthOrderAppFreezeRequest.php';
require './vendor/alipay-sdk-php-all-master/aop/request/AlipayTradePayRequest.php';

class Alideposit extends Controller
{
    /**
     * @var \AopClient
     */
    private $aop;

    /**
     * @return [type] [description]
     */
    public function initialize()
    {
        parent::initialize();
        $config = config('alipayApp');
        $aop = new \AopCertClient();
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->alipayrsaPublicKey = $aop->getPublicKey(Env::get('root_path') . 'upload/ali/alipayCertPublicKey_RSA2.crt');
        $aop->appCertSN = $aop->getCertSN(Env::get('root_path') . 'upload/ali/appCertPublicKey_2021002117684078.crt');
        $aop->alipayRootCertSN = $aop->getRootCertSN(Env::get('root_path') . 'upload/ali/alipayRootCert.crt');
        $aop->rsaPrivateKey = $config['merchant_private_key'];
        $aop->isCheckAlipayPublicCert = true;
        $aop->appId = $config["app_id"];
        $aop->signType = 'RSA2';
        $aop->postCharset = 'utf-8';
        $aop->format = 'json';
        $this->aop= $aop;
    }

    //（资金授权）业务
    // $ordersn  唯一订单号
    // $msg      订单内容
    // $money    订单交易金额
    public function frozenorder($ordersn, $msg, $money)
    {
        if (!$ordersn || !$msg || !$money) {
            return json_encode(reAjaxMsg(0, '交易参数不完整！'));
        }
        try {
            $orderdata = [
                "out_request_no"      => $ordersn,//商户本次资金操作的请求流水号，用于标示请求流水的唯一性
                "out_order_no"        => $ordersn,//商户授权资金订单号
                "order_title"         => $msg,//实际消费订单说明
                "product_code"        => "PRE_AUTH_ONLINE",
                'amount'              => $money,//需要冻结的金额
                "payee_logon_id"      => "youmikejivip@163.com",
                'extra_param'         => '{"category":"RENT_SHARABLE_CHARGERS","serviceId":"2021080600000000000086312500"}',
//                'enable_pay_channels' => '[{"payChannelType":"CREDITZHIMA"},{"payChannelType":"PCREDIT_PAY"},{"payChannelType":"MONEY_FUND"}]',
            ];
            $config = config('alipayApp');

            $request = new \AlipayFundAuthOrderAppFreezeRequest();
            $request->setBizContent(json_encode($orderdata));
            //die(json_encode($orderdata));
            $request->setNotifyUrl($config['notify_url_yajin_frozen']);
            $result = $this->aop->sdkExecute($request);
            //return json_encode(["id"=>0,"msg"=>$result,"order_no"=>$ordersn ]);
            return (["id" => 1, "result" => $result, "order_no" => $ordersn]);
        } catch (\Exception $e) {
            db("order_pay")->where(['o_number' => $ordersn])->delete();
        }
    }

    // $controller->create_pay_order( $orderInfo["o_number"] , $orderInfo["o_line_number"] , $orderInfo["o_psd_set_describe"] ,  $totalprice  ,$userappid   );
    // 交易创建并支付
    // $order_sn     我平台 订单号
    // $auth_no      阿里平台返回的  订单号
    // $body         订单交易说明
    // $total_amount 交易金额
    // $buyer_id     用户在阿里平台的唯一id
    public function create_pay_order($order_sn = false, $auth_no = false, $body = false, $total_amount = false, $buyer_id = false)
    {
        if (!$order_sn || !$auth_no || !$body || !$total_amount || !$buyer_id) {
            return json_encode(["code" => 1, "msg" => "交易参数不完整"]);
        }
        $orderdata = [
            "out_trade_no"      => $order_sn, /** 商户订单号，商户自定义，需保证在商户端不重复，如：20150320010101001 **/
            "auth_no"           => $auth_no, /** 支付授权码，取值于支付宝APP内付钱-付款码-条码值，如:28447322****549889 **/
            "product_code"      => "PRE_AUTH_ONLINE", /** 销售产品码，固定值:FACE_TO_FACE_PAYMENT **/
            "total_amount"      => $total_amount, /** 订单金额，精确到小数点后两位 **/
            "subject"           => "充电宝租借", /** 订单标题 **/
            "body"              => $body, /** 订单描述 **/
            "buyer_id"          => $buyer_id,
            "seller_id"         => "2088041241503251",
            "auth_confirm_mode" => "COMPLETE"
        ];
        $config = config('alipayApp');
        $request = new \AlipayTradePayRequest();
        $request->setBizContent(json_encode($orderdata));
        $request->setNotifyUrl($config['notify_url_yajin_frozen']);
        $result = $this->aop->execute($request);
        $result = json_decode(json_encode($result, JSON_UNESCAPED_UNICODE), 1);
        $resultmsg = $result["alipay_trade_pay_response"]["msg"];
        if (strtolower($resultmsg) == "success") {
            return true;
        } else {
            throw new \Exception($result["alipay_trade_pay_response"]["sub_msg"]);
            return false;
        }
    }


    /*交易创建并支付接口*/
    public function create_pay_demo()
    {
        $request = new \AlipayTradePayRequest();
        $testdata = [
            "out_trade_no"      => "al2021042809152166532116", /** 商户订单号，商户自定义，需保证在商户端不重复，如：20150320010101001 **/
            "auth_no"           => "2021042810002001290519580080", /** 支付授权码，取值于支付宝APP内付钱-付款码-条码值，如:28447322****549889 **/
            "product_code"      => "PRE_AUTH_ONLINE", /** 销售产品码，固定值:FACE_TO_FACE_PAYMENT **/
            "total_amount"      => "0.01", /** 订单金额，精确到小数点后两位 **/
            "subject"           => "充电宝租借", /** 订单标题 **/
            "body"              => "订单描述", /** 订单描述 **/
            "buyer_id"          => "2088122815906291",
            "seller_id"         => "2088631897006677",
            "auth_confirm_mode" => "COMPLETE"
        ];
        $request->setBizContent(json_encode($testdata));
        $request->setNotifyUrl("https://test.mszn1.com/api.php/notifyurl/notifyurlpay");
        $result = $this->aop->execute($request);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }


    /*预授权操作查询接口*/
    public function select()
    {
        $request = new \AlipayFundAuthOperationDetailQueryRequest();
        $testdata = [
            /** 支付宝资金授权订单号，在授权冻结成功时返回参数中获得  **/
            "auth_no"        => "2020071610002001180567235807",
            /** 商户的授权资金订单号，与支付宝的授权资金订单号不能同时为空  **/
            "out_order_no"   => "20200716900335",
            /** 商户的授权资金操作流水号，与支付宝的授权资金操作流水号不能同时为空  **/
            "operation_id"   => "20200716604057851805",
            /** 支付宝的授权资金操作流水号  **/
            "out_request_no" => "22007169660955"
        ];
        $request->setBizContent($testdata);
        $result = $this->aop->execute($request);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /*支付宝订单信息同步接口*/
    public function synchro_info()
    {
        $this->aop->postCharset = 'GBK';
        $request = new \AlipayTradeOrderinfoSyncRequest();
        $testdata = [
            "trade_no"        => "2018061021001004680073956707",//支付宝交易号
            "orig_request_no" => "HZ01RF001",//[可选]原始业务请求单号。如对某一次退款进行履约时，该字段传退款时的退款请求号
            "out_request_no"  => "HZ01RF001",//标识一笔交易多次请求，同一笔交易多次信息同步时需要保证唯一
            "biz_type"        => "CREDIT_AUTH",//交易信息同步对应的业务类型，具体值与支付宝约定； 信用授权场景下传CREDIT_AUTH 信用代扣场景下传CREDIT_DEDUCT
            "order_biz_info"  => json_encode(["status" => "COMPLETE"])
            /*[可选]商户传入同步信息，具体值要和支付宝约定；用于芝麻信用租车、单次授权等信息同步场景，格式为json格式。
             * COMPLETE： 同步用户已履约适用场景：发起扣款后，芝麻生成待履约记录，如果用户通过其他方式完成订单支付，请反馈该状态，芝麻将完结待履约记录对用户形成一条良好履约记录；
             * CLOSED：   同步履约已取消适用场景：发起扣款后，芝麻生成待履约记录，如果发现该笔扣款无效需要取消，请反馈该状态，芝麻将取消用户待履约记录；
             * VIOLATED： 用户已违约适用场景：如果用户在约定时间（具体根据行业约定，有一定宽限期）内未完成订单支付，反馈该状态，芝麻将对用户记录一条负面记录，请谨慎使用；
             */
        ];
        $request->setBizContent(json_encode($testdata));
        $result = $this->aop->execute($request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (!empty($resultCode) && $resultCode == 10000) {
            echo "成功";
        } else {
            echo "失败";
        }
    }


    /*资金授权解冻接口*/
    public function thaw()
    {
        $request = new \AlipayFundAuthOrderUnfreezeRequest();
        $testdata = [
            /** 支付宝资金授权订单号 **/
            "auth_no"        => "2020071610002001180567327929",
            /** 商户本次资金操作的请求流水号，同一商户每次不同的资金操作请求，商户请求流水号不能重复 **/
            "out_request_no" => "220071696609505001",
            /** 本次操作解冻的金额，单位为：元（人民币），精确到小数点后两位 **/
            "amount"         => "0.01",
            /** 商户对本次解冻操作的附言描述 **/
            "remark"         => "2020-07期解冻0.01元",
            /** 选填字段，信用授权订单，针对信用全免订单，传入该值完结信用订单，形成芝麻履约记录  **/
            "extra_param"    => json_encode(["unfreezeBizInfo" => ["bizComplete" => "true"]])
        ];

        $request->setBizContent(json_encode($testdata));
        $result = $this->aop->execute($request);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /*交易查询接口*/
    public function select_order()
    {
        $request = new \AlipayTradeQueryRequest();
        $testdata = [
            /** 注：交易号（TradeNo）与订单号（OutTradeNo）二选一传入即可，如果2个同时传入，则以交易号为准 **/
            /** 支付接口传入的商户订单号。如：2020061601290011200000140004 **/
            "out_trade_no" => "2020061601290011200000140004",
        ];
        $request->setBizContent(json_encode($testdata));
        $result = $this->aop->execute($request);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /*交易退款接口*/
    public function select_refund()
    {
        $request = new  \AlipayTradeRefundRequest();
        $testdata = [
            /** 注：交易号（TradeNo）与订单号（OutTradeNo）二选一传入即可，如果2个同时传入，则以交易号为准 **/
            /** 支付接口传入的商户订单号。如：2020061601290011200000140004 **/
            "out_trade_no"   => "2020061601290011200000140004",
            /** 退款金额，退款总金额不能大于该笔订单支付最大金额 **/
            "refund_amount"  => "0.01",
            /** 如需部分退款，则此参数必传，且每次请求不能重复，如：202006230001 **/
            "out_request_no" => "202006230001"
        ];
        $request->setBizContent(json_encode($testdata));
        $result = $this->aop->execute($request);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
