<?php
/**
 * 阿里免押金demo
 * User: will
 * Date: 2021/4/22
 */

namespace app\api\controller;

use app\common\model\OrderPay;
use think\facade\Request;
use think\Db;
use think\Controller;
use think\facade\Cache;
use think\facade\Env;


class Crontab extends Controller
{
    public function updateOrder1()
    {
        $errorMsg = '';
        //待完成订单
        $orderList = db('order_pay')->where('pay_status=1 AND o_endtime <=' . time())->where('isYajinEnd', 0)->where('orderType', 1)->all();
        foreach ($orderList as $orderInfo) {
            $disId = $orderInfo['o_dis_id'];
            try {
                Db::startTrans();
                //扣除押金
                //判断用了多久
                $hour = ceil((time() - $orderInfo['create_time']) / 3600);
                //获取押金配置项
                $yajinInfo = db('yajin_config')->where('dis_id', $disId)->find();
                $money = ($yajinInfo['hourMoney'] * $hour);
                if ($money >= $yajinInfo['maxMoney']) {
                    $money = $yajinInfo['maxMoney'];
                }
                //扣除用户押金
                $data = \db('user')->where('u_id', $orderInfo['o_uid'])->dec('yajin_money', $money)->update();
                //更新订单为已结算
                db('order_pay')->where('o_id', $orderInfo['o_id'])->update(['isYajinEnd' => 1, 'o_price' => $money, 'o_endtime' => time(), 'o_paytime' => time(), 'update_time' => time(), 'o_line_number' => $orderInfo['o_number']]);
                $orderInfo = db('order_pay')->where('o_id', $orderInfo['o_id'])->find();

                //计算分佣
                OrderPay::handleYajinTypeMoney($orderInfo);
                Db::commit();
            } catch (\Throwable $throwable) {
                Db::rollback();
                $errorMsg .= $throwable->getMessage() . PHP_EOL;
            }
        }
        echo $errorMsg;
    }

    public function updateOrder2()
    {
        $errorMsg='';
        //待完成订单
        $orderList = db('order_pay')->where('pay_status=1 AND o_endtime <=' . time())->where('isYajinEnd', 0)->where('orderType', 2)->all();
        foreach ($orderList as $orderInfo) {
            $disId = $orderInfo['o_dis_id'];
            //扣除押金
            //判断用了多久
            $hour = ceil((time() - $orderInfo['create_time']) / 3600);
            //获取押金配置项
            $yajinInfo = db('yajin_config')->where('dis_id', $disId)->find();
            $money = ($yajinInfo['hourMoney'] * $hour);
            if ($money >= $yajinInfo['maxMoney']) {
                $money = $yajinInfo['maxMoney'];
            }
            try {
                Db::startTrans();
                $userappid = db("user")->field("openid")->where(["u_id" => $this->userId])->find()["openid"];
                //交易创建并支付
                $controller = controller("api/alideposit");
                $bool = $controller->create_pay_order($orderInfo["o_number"], $orderInfo["o_line_number"], $orderInfo["o_psd_set_describe"], $money, $userappid);
                if ($bool) {
                    //更新订单为已结算
                    db('order_pay')->where('o_id', $orderInfo['o_id'])->update(['isYajinEnd' => 1, 'o_price' => $money, 'o_endtime' => time(), 'update_time' => time()]);
                    $orderInfo = db('order_pay')->where('o_id', $orderInfo['o_id'])->find();
                    //计算分佣
                    OrderPay::handleYajinTypeMoney($orderInfo);
                }
                $where["o_number"] = $orderInfo["o_number"];
                $bool = db("order_pay")->where($where)->field("isYajinEnd")->find()["isYajinEnd"];
                if (!$bool) {
                    Db::rollback();
                }
                Db::commit();
            } catch (\Throwable $throwable) {
                Db::rollback();
                $errorMsg .= $throwable->getMessage() . PHP_EOL;
            }
        }
        echo $errorMsg;
    }
}
