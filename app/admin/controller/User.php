<?php

namespace app\admin\controller;

use think\Model;

class User extends Base
{
    private $userCom = '';
    private $userRole = '';
    public function initialize()
    {
        parent::initialize();
        $this->userCom = controller('common/user');
        $this->userRole = session('adminInfo.userRole');
    }

    /*
     * ===================================================================================
     *
     * 页面部分
     *
     * ===================================================================================
     */
    //添加用户页面
    public function addUser(){
        $where = 'r_status != -1 AND r_id != 4 AND r_id != 5 AND r_id != 6';
        $roleList = controller('common/role')->getRoleList($where);//获取角色
        $userInfo = array(
            'u_id'=>'',
            'u_name'=>'',
            'u_nick'=>'',
            'u_phone'=>'',
            'u_password'=>'',
            'u_email'=>'',
            'u_type'=>'',
            'u_status'=>'',
            'u_role'=>'',
        );
        $this->assign("userInfo",$userInfo);
        $this->assign("roleList",$roleList);
        $this->assign('form_url','user/gotoAddUser');
        $this->assign('check_account_url','user/gotoCheckAccountBeUsed');
        return $this->fetch();
    }
    //会员编辑页面
    public function editUser(){
        $u_id = input('u_id') ? input('u_id') : '';
        if (!$u_id) {
            return $this->error('缺少用户ID');
        }
        $where = 'r_status != -1 AND r_id != 4 AND r_id != 5 AND r_id != 6';
        $roleList = controller('common/role')->getRoleList($where);//获取角色
        $userInfo = $this->userCom->getUserOneInfoByWhere('u_id='.$u_id,'*');
        $this->assign("roleList",$roleList);
        $this->assign("userInfo",$userInfo);
        $this->assign('form_url','user/gotoEditUser');
        $this->assign('check_account_url','user/gotoCheckAccountBeUsed');
        return $this->fetch('add_user');
    }
    //会员列表页面
    public function userList(){
        $phone = input('phone','');
        $status = input('status','');

        $where = $phone ? 'u_type !=2 AND u_phone="'.$phone.'"': 'u_type !=2';
        $where = $status ? $where.' AND u_status='.$status : $where;
        if(in_array($this->userDisType, [1,2,3])){
            $where .= ' and u_adduid='.$this->userId;
        }

        $userList = model('User')->getUserPage($where,'*', 10,'u_addtime DESC',['phone'=>$phone,'u_status'=>$status]);
        $page = $userList->render();
        $userList = $userList->isEmpty() ? '' : $userList;

        $this->assign("userList",$userList);
        $this->assign("page",$page);
        $this->assign("phone",$phone);
        return $this->fetch();
    }
    //会员充值列表页面
    public function userRechargeList(){
        $uid = input('u_id',0);
        $quantum = input('quantum','all');
        $beginDate = input('beginDate','');
        $endDate = input('endDate','');
        //$where = $uid ? 'o_uid='.$uid : '';
        $where = '';
        if ($quantum != 'all') {
            if ($quantum == 'byDate') {
                if ($beginDate) $where = 'r_addtime >= "'.$beginDate.' 00:00:00"';
                if ($endDate) $where = $beginDate ? $where.' AND r_addtime <= "'.$endDate.' 23:59:59"' : 'r_addtime <= "'.$endDate.' 23:59:59"';
            } else {
                $dateData = getDateQuantum($quantum);
                $where = 'r_addtime >= "'.$dateData['beginTime'].'" AND r_addtime <= "'.$dateData['endTime'].'"';
            }
        }
        $total_price = $this->userCom->getUserRechargeFieldSum($where);
        $userRechargeList = $this->userCom->getUserRechargePage($where,'*', 20, ['quantum'=>$quantum,'beginDate'=>$beginDate,'endDate'=>$endDate], 'r_addtime DESC');
        $page = $userRechargeList->render();
        $this->assign("userRechargeList",$userRechargeList);
        $this->assign("page",$page);
        $this->assign("beginDate",$beginDate);
        $this->assign("endDate",$endDate);
        $this->assign("total_price",$total_price);
        return $this->fetch();
    }
    //会员充值页面
    public function addUserRecharge(){
        $this->assign("form_url",'user/gotoAddUserRecharge');
        return $this->fetch();
    }
    //管理员列表页面
    public function administratorList(){
        $phone = input('phone','');
        $where = $phone ? 'u_type=2 AND u_role != 4  AND u_role != 5  AND u_role != 6 AND u_phone="'.$phone.'"': 'u_type=2 AND u_role != 4  AND u_role != 5  AND u_role != 6 ';
        $userList = model('User')->getUserPage($where,'*', 20,'u_addtime DESC',['phone'=>$phone]);
        $page = $userList->render();
        $this->assign("userList",$userList);
        $this->assign("page",$page);
        $this->assign("phone",$phone);
        return $this->fetch();
    }
    //获取用户提现列表页面
    public function userWithdrawList(){
        $quantum = input('quantum','all');
        $beginDate = input('beginDate','');
        $endDate = input('endDate','');
        $userPhone = input('userPhone','');
        $where = '';
        if ($quantum != 'all') {
            if ($quantum == 'byDate') {
                if ($beginDate) $where = 'w_addtime >= "'.$beginDate.' 00:00:00"';
                if ($endDate) $where = $beginDate ? $where.' AND w_addtime <= "'.$endDate.' 23:59:59"' : 'w_addtime <= "'.$endDate.' 23:59:59"';
            } else {
                $dateData = getDateQuantum($quantum);
                $where = 'w_addtime >= "'.$dateData['beginTime'].'" AND w_addtime <= "'.$dateData['endTime'].'"';
            }
        }
        if ($userPhone) {
            $userInfo = $this->userCom->getUserOneInfoByWhere('u_phone="'.$userPhone.'"','u_id');
            if ($userInfo) {
                $where = $where ? $where.' AND w_uid='.$userInfo['u_id'] : 'w_uid='.$userInfo['u_id'];
            }
        }
        $total_price = $this->userCom->getUserWithdrawFieldSum($where);
        $userWithdrawList = $this->userCom->getUserWithdrawMoneyPage($where,'*', 20, ['quantum'=>$quantum,'beginDate'=>$beginDate,'endDate'=>$endDate,'userPhone'=>$userPhone], 'w_addtime DESC');
        $page = $userWithdrawList->render();
        $this->assign("userWithdrawList",$userWithdrawList);
        $this->assign("gotoDoUserWithdrawUrl",'user/gotoDoUserWithdraw');
        $this->assign("page",$page);
        $this->assign("beginDate",$beginDate ? $beginDate : '');
        $this->assign("endDate",$endDate ? $endDate : '');
        $this->assign("userPhone",$userPhone ? $userPhone : '');
        $this->assign("total_price",$total_price);
        return $this->fetch();
    }
    //用户交易查询 订单列表
    public function userOrderList(){
        $quantum = input('quantum','all');
        $beginDate = input('beginDate','');
        $endDate = input('endDate','');
        $uid = input('u_id',0);
        $where = $uid ? 'o_uid='.$uid : '';
        if ($quantum != 'all') {
            if ($quantum == 'byDate') {
                if ($beginDate) $where = 'o_addtime >= "'.$beginDate.' 00:00:00"';
                if ($endDate) $where = $beginDate ? $where.' AND o_addtime <= "'.$endDate.' 23:59:59"' : 'o_addtime <= "'.$endDate.' 23:59:59"';
            } else {
                $dateData = getDateQuantum($quantum);
                $where = 'o_addtime >= "'.$dateData['beginTime'].'" AND o_addtime <= "'.$dateData['endTime'].'"';
            }
        }
        $orderCom = controller('common/order');
        $total_num = $orderCom->getOrderFieldSum('o_number',$where);//总数量
        $total_price = $orderCom->getOrderFieldSum('o_total_money',$where);//总金额
        $total_done_num = $orderCom->getOrderFieldSum('o_done_total_number',$where);//成交总数量
        $total_done_price = $orderCom->getOrderDonePriceSum($where);//成交总金额
        $total_buy_done_num = $orderCom->getOrderFieldSum('o_done_total_number',$where ? $where.' AND o_type = 2' : 'o_type = 2');//买入成交总数量
        $total_buy_done_price = $orderCom->getOrderDonePriceSum($where ? $where.' AND o_type = 2' : 'o_type = 2');//买入成交总金额
        $total_sell_done_num = $orderCom->getOrderFieldSum('o_done_total_number',$where ? $where.' AND o_type = 1' : 'o_type = 1');//卖出成交总数量
        $total_sell_done_price = $orderCom->getOrderDonePriceSum($where ? $where.' AND o_type = 1' : 'o_type = 1');//卖出成交总金额
        $orderList = $orderCom->getOrderPage('*', $where, 10, ['quantum'=>$quantum,'u_id'=>$uid,'beginDate'=>$beginDate,'endDate'=>$endDate], 'o_addtime DESC');
        $page = $orderList->render();
        $this->assign("orderList",$orderList);
        $this->assign("total_num",$total_num);
        $this->assign("total_price",$total_price);
        $this->assign("total_done_num",$total_done_num);
        $this->assign("total_done_price",$total_done_price);
        $this->assign("total_buy_done_num",$total_buy_done_num);
        $this->assign("total_buy_done_price",$total_buy_done_price);
        $this->assign("total_sell_done_num",$total_sell_done_num);
        $this->assign("total_sell_done_price",$total_sell_done_price);
        $this->assign("beginDate",$beginDate);
        $this->assign("endDate",$endDate);
        $this->assign("page",$page);
        return $this->fetch();

    }
    //用户明细
    public function userAccountList() {
        $where = '';
        $quantum = input('quantum','all');
        $beginDate = input('beginDate','');
        $endDate = input('endDate','');
        if ($quantum != 'all') {
            if ($quantum == 'byDate') {
                if ($beginDate) $where = 'a_addtime >= "'.$beginDate.' 00:00:00"';
                if ($endDate) $where = $beginDate ? $where.' AND a_addtime <= "'.$endDate.' 23:59:59"' : 'a_addtime <= "'.$endDate.' 23:59:59"';
            } else {
                $dateData = getDateQuantum($quantum);
                $where = 'a_addtime >= "'.$dateData['beginTime'].'" AND a_addtime <= "'.$dateData['endTime'].'"';
            }
        }
        $accountList = $this->userCom->getUserAccountPage($where,'*', 50, ['quantum'=>$quantum,'beginDate'=>$beginDate,'endDate'=>$endDate], 'a_id DESC');
        $page = $accountList->render();
        $total_price = $this->userCom->getUserAccountRecordsFieldSum($where);//总金额
        $this->assign('accountList',$accountList);
        $this->assign("page",$page);
        $this->assign("beginDate",$beginDate);
        $this->assign("endDate",$endDate);
        $this->assign("total_price",$total_price);
        return $this->fetch();
    }
    //查看用户提现详情记录
    public function viewUserWithdraw() {
        $w_id = input('w_id',0);
        $withdrawInfo = controller('common/user')->getUserWithdraw('w_id='.$w_id);
        $this->assign('withdrawInfo',$withdrawInfo);
        return $this->fetch();
    }
    //会员登录日志
    public function userLoginLog()
    {
        $userPhone = input('phone');
        $userWhere = $userPhone ? 'u_phone="'.$userPhone.'"' : '';
        $where = [
            'userWhere' => $userWhere,
            'where' => '',
        ];
        $loginLogPage = model('UserLoginLog')->getUserLoginLogPage($where,'*',10);
        $page = $loginLogPage->render();
        $loginLogPage = $loginLogPage->isEmpty() ? '' : $loginLogPage;
        $this->assign('loginLogPage',$loginLogPage);
        $this->assign('page',$page);
        $this->assign('userPhone',$userPhone);
        return $this->fetch();
    }
    //查看会员信息
    public function viewUser()
    {
        $type = input('type','');
        $uid = input('u_id','');
        $acc_num_type = input('acc_num_type','');
        if (!$uid) {
            $this->error('缺少用户ID');
        }
        if (!$type) {
            $this->error('缺少类型');
        }
        $with = $type == 1 ? 'userIc' : 'userCompany';
        $userMod = model('user');
        $res = $userMod->with($with)->field('*')->where('u_id='.$uid.' AND u_type='.$type)->find($uid);
        $this->assign('info',$res->toArray());
        $this->assign('acc_num_type',$acc_num_type);
        $this->assign('dotype','status');
        $this->assign('gotoSetInfoUrl',url('user/gotoEditUser'));
        return $this->fetch();
    }
    //审核更新手机列表
    public function auditChangeMobile()
    {
        $m_status = input('m_status','');
        $m_acc_num = input('m_acc_num','');

        $where = $m_acc_num ? 'm_acc_num="'.$m_acc_num.'"' : '';
        $where = $m_status ? ($where ? $where.' AND m_status='.$m_status : 'm_status='.$m_status) : $where;
        $resultList = model('UserChangeMobile')->where($where)->order('m_addtime DESC')->paginate(20,false,['m_status'=>$m_status,'m_acc_num'=>$m_acc_num])->each(
            function($vlist)
            {
                $status_arr = [1=>'审核中',2=>'审核通过',3=>'拒绝审核'];
                $vlist['m_status_name'] = isset($vlist['m_status']) ? $status_arr[$vlist['m_status']] : '';
            });
        $page = $resultList->render();
        $resultList = $resultList->isEmpty() ? '' : $resultList;
        $this->assign('page',$page);
        $this->assign('resultList',$resultList);
        $this->assign('m_acc_num',$m_acc_num);
        $this->assign('m_status',$m_status);
        return $this->fetch();
    }

    /*
     * ===================================================================================
     *
     * 功能部分
     *
     * ===================================================================================
     */

    //会员充值
    public function gotoAddUserRecharge() {
        $r_phone = input('post.r_phone',0);//用户手机号码
        $r_money = input('post.r_money',0);//充值金额

        if (!$r_phone) {
            return reAjaxMsg(0,'用户手机号码不可以为空！');
        }
        if (!checkPhone($r_phone)){
            return reAjaxMsg(0,$r_phone.'该用户手机号码格式有误！');
        }
        $newData = array(
            'r_phone' => $r_phone,//用户手机号码
            'r_money' => $r_money,//充值金额
            'r_adduid' => $this->userId,//充值人操作人id
            'r_addtime' => myTime(),//添加时间
        );
        $result = $this->userCom->addUserRecharge($newData);
        if ($result == 'ok') {
            return reAjaxMsg(1,'添加成功');
        }
        return reAjaxMsg(0,$result);
    }
    //添加会员（会员注册）
    public function gotoAddUser(){
        $u_name = input('post.u_name') ? trim(input('post.u_name')) : '';//用户名称
        $u_phone = input('post.u_phone') ? trim(input('post.u_phone')) : '';//用户手机号码
        $u_email = input('post.u_email') ? trim(input('post.u_email')) : '';//用户邮箱
        $u_password = input('post.u_password');//用户手密码
        $u_rpassword = input('post.u_rpassword');//用户确认密码
        $u_status = input('post.u_status');//用户状态【1启动，2暂停】
        $u_type = input('post.u_type');//用户类型【1普通会员，2管理员，3客服】
        $u_address = input('post.u_address');//用户类型【1普通会员，2管理员，3客服】
        $u_role = input('post.u_role');//角色

        if (!$u_name) {
            return reAjaxMsg(0,'用户名称不可以为空！');
        }
        if (!$u_phone) {
            return reAjaxMsg(0,'用户手机号码不可以为空！');
        }
        if (!checkPhone($u_phone)){
            return reAjaxMsg(0,$u_phone.'该用户手机号码格式有误！');
        }
        if (strlen($u_password) < 6 || strlen($u_password) > 15 ){
            return reAjaxMsg(0,'密码长度不对，请输入6~15位！');
        }
        if ($u_password != $u_rpassword){
            return reAjaxMsg(0,'两个密码不一致，请重新输入！');
        }
        $newData = array(
            'u_name' => $u_name,//用户名称
            'u_phone' => $u_phone,//用户手机号码
            'u_email' => $u_email,//用户邮箱
            'u_password' => pswCAM($u_phone,$u_password),//用户手密码
            'u_status' => $u_status,//用户状态【1启动，2暂停】
            'u_type' => $u_type,//用户类型【1普通会员，2管理员，3客服】
            'u_adduid' => $this->userId,//添加人id
            'u_addname' => $this->userName,//添加人姓名
            'u_role' => $u_role,//角色
        );
        $result = model('User')->addUser($newData);
        return $result == 'ok' ? reAjaxMsg(1,'添加成功！') : reAjaxMsg(0,$result);
    }
    //编辑用户信息
    public function gotoEditUser(){
        $u_id = input('post.u_id') ? trim(input('post.u_id')) : '';//用户ID
        $u_name = input('post.u_name') ? trim(input('post.u_name')) : '';//用户名称
        $u_phone = input('post.u_phone') ? trim(input('post.u_phone')) : '';//用户手机号码
        $u_email = input('post.u_email') ? trim(input('post.u_email')) : '';//用户邮箱
        $u_password = input('post.u_password');//用户手密码
        $u_rpassword = input('post.u_rpassword');//用户确认密码
        $u_status = input('post.u_status');//用户状态【1启动，2暂停，3审核】
        $u_type = input('post.u_type');//用户类型【1普通会员，2管理员，3客服】
        $u_role = input('post.u_role');//角色
        $dotype = input('dotype','');//操作类型 默认为空

        if (!$u_id) {
            return reAjaxMsg(0,'缺少用户名ID！');
        }
        if ($dotype == 'status') {
            $newData = array(
                'u_status' => 1,//用户状态【1启动，2暂停，3审核】
            );
            $userInfo = db('user')->where('u_id='.$u_id)->find();
            if (!$userInfo) return reAjaxMsg(0,'缺少用户数据！');
        } else {
            if (!$u_name) {
                return reAjaxMsg(0,'用户名称不可以为空！');
            }
            if (!$u_phone) {
                return reAjaxMsg(0,'用户手机号码不可以为空！');
            }
            if (!checkPhone($u_phone)){
                return reAjaxMsg(0,$u_phone.'该用户手机号码格式有误！');
            }
            if ($u_password) {
                if (strlen($u_password) < 6 || strlen($u_password) > 15 ){
                    return reAjaxMsg(0,'密码长度不对，请输入6~15位！');
                }
                if ($u_password != $u_rpassword){
                    return reAjaxMsg(0,'两个密码不一致，请重新输入！');
                }
            }
            $newData = array(
                'u_name' => $u_name,//用户名称
                'u_phone' => $u_phone,//用户手机号码
                'u_email' => $u_email,//用户邮箱
                'u_status' => $u_status,//用户状态【1启动，2暂停】
                'u_type' => $u_type,//用户类型【1普通会员，2管理员，3客服】
                'u_role' => $u_role,//角色
            );
            if ($u_password) {
                $newData['u_password'] = pswCAM($u_phone,$u_password);//用户手密码
            }
        }

        $result = model('User')->upUser($u_id,$newData);
        return  $result == 'ok' ? reAjaxMsg(1,$dotype == 'status' ? '审核通过！' : '编辑成功！') : reAjaxMsg(0,$result);

    }
    //检测用户名是否被注册
    public function gotoCheckAccountBeUsed(){
        $account = input('post.account');
        $u_id = input('post.u_id');
        if (!trim($account)) {
            return reAjaxMsg(0,'请输入用户名！'.$account);
        }
        $where = $u_id ? 'u_id != '.$u_id.' AND u_name="'.$account.'"' : 'u_name="'.$account.'"';
        return model('User')->checkUserInfoBeUsed($where) == 'ok' ? reAjaxMsg(1,'该用户名未被注册') : reAjaxMsg(0,'该用户名已被注册');
    }
    //审核用户提现
    public function gotoDoUserWithdraw() {
        $w_status = input('post.w_status',0);
        $w_id = input('post.w_id',0);
        if (!$w_id) return reAjaxMsg(0,'缺少提现索引ID');
        if (!in_array($w_status,[$this->userCom->withdrawApplyPassTypeId,$this->userCom->withdrawApplyNoPassTypeId])) {
            return reAjaxMsg(0,'操作状态有误！');
        }
        $newData = array(
            'w_status' => $w_status,
            'w_douid' => $this->userId,
            'w_dotime' => myTime(),
        );
        $result = $this->userCom->checkUserWithdrawMoneyStatus('w_id='.$w_id,$newData);
        if ($result == 'ok') {
            return reAjaxMsg(1,'操作成功！');
        }
        return reAjaxMsg(0,'操作失败！');
    }
    //处理用户提现上传打款图片
    public function gotoWithdrawUploadImg(){
        $w_status = input('w_status',$this->userCom->withdrawApplyPassTypeId);
        $w_id = input('w_id',0);
        if (!$w_id) return reAjaxMsg(0,'缺少提现索引ID');
        if (!in_array($w_status,[$this->userCom->withdrawApplyPassTypeId,$this->userCom->withdrawApplyNoPassTypeId])) {
            return reAjaxMsg(0,'操作状态有误！');
        }

        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('uploadImg');
        $path = ROOT_PATH  . 'upload'. DS . 'withdraw/';
        // 移动到框架应用根目录/public/uploads/ 目录下
        if(!$file) return reAjaxMsg(0,'上传失败2！');

        $info = $file->validate(['size'=>15678000,'ext'=>'jpg,png,gif'])->move($path,$w_id.'_'.time());
        if($info){
            $field = 'w_remittance_img';
            $imgName = $info->getSaveName();//获取图片名称
            $imgName = '/upload/withdraw/'.$imgName;
            $newData = array(
                'w_status' => $w_status,
                'w_douid' => $this->userId,
                'w_dotime' => myTime(),
                'w_remittance_img' => $imgName,
            );
            $bannerInfo = $this->userCom->getUserWithdrawInfoByField($w_id,$field);
            $result = $this->userCom->checkUserWithdrawMoneyStatus('w_id='.$w_id,$newData);
            //删除旧图片
            if (!empty($bannerInfo[$field])) {
                $imgPath = ROOT_PATH.$bannerInfo[$field];
                delFile($imgPath);
            }
            //编辑或添加不成功，则删除已上传的图片
            if ($result != 'ok') {
                delFile(ROOT_PATH.$imgName);
                return reAjaxMsg(0,$result);
            }
            return reAjaxMsg(1,'上传成功！');
        }else{
            return reAjaxMsg(0,$file->getError());
        }
    }
}