<?php
namespace app\admin\controller;

use think\Controller;
use think\facade\Hook;
class Base extends Controller
{
    public $userId = '';
    public $userName = '';
    public $userNick = '';
    public $userPhone = '';
    public $userDisType = 0;//分销商类型 0平台，1一级分销商，2二级分销商，3三级分销商
    public $userDisId = 0;//分销商索引ID
    public $userDisName = '';
    public $userDisInfo = '';
    public $userRoleId = '';
    public function initialize()
    {
        //判断用户是否登录
        Hook::listen('check_user_login',2);
        //获取用户ID
        if (session('?adminInfo')) {
            $this->userId = session('adminInfo.adminId');
        }
        //设置页面标题
        $this->assign('pageTitle',pageTitle(request()->module(),request()->controller().'/'.request()->action()));
        //获取用户信息
        if ($this->userId) {
            $userCom = controller('common/user');
            $userInfo = $userCom->getUserAllInfoById($this->userId);
            if ($userInfo) {
                $distributorRoleIdList = controller('common/role')->getDistributorRoleIdList();//获取第一二三级的角色ID
                $this->userRoleId = db('user')->where('u_id='.$this->userId.' AND u_type=2')->value('u_role');
                $this->userDisInfo = $userInfo['u_disuid'] ? db('distributor')->where('d_user_id='.$userInfo['u_disuid'].' or d_user_id='.$this->userId)->find() : db('distributor')->where('d_user_id='.$this->userId)->find();
                if (!$this->userDisInfo && in_array($this->userRoleId, $distributorRoleIdList)) {
                    session('adminInfo',null);
                    $this->error('您无权操作后台，请联系客服！', '/myadmin.php/login/index', '', 4);
                    exit;
                }
                $this->userName = $userInfo['u_name'];
                $this->userNick = $userInfo['u_nick'];
                $this->userPhone = $userInfo['u_phone'];
                $this->userDisType = empty($this->userDisInfo['d_type']) ? '' : $this->userDisInfo['d_type'];
                $this->userDisName = empty($this->userDisInfo['d_name']) ? '' : $this->userDisInfo['d_name'];
                $this->userDisId = empty($this->userDisInfo['d_id']) ? 0 : $this->userDisInfo['d_id'];
                $this->getOwnDisId($userInfo['u_disuid']);
            }
        }
        $this->assign('mName',$this->userName);//管理员名称
        $this->assign('userDisId',$this->userDisId);//管理员名称
    }
    //获取自己分销商ID
    public function getOwnDisId($disuid)
    {
        //获取自己的分销商ID
        $distributorRoleIdList = controller('common/role')->getDistributorRoleIdList();//获取第一二三级的角色ID
        //$this->userDisId = in_array($this->userRoleId, $distributorRoleIdList) && $disuid ? controller('common/distributor')->getFieldValue('d_user_id='.$disuid,'d_id') : 0;//平台的统一归为0
    }
  
  
  
  public function is_mobile_request(){  
    $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';  
    $mobile_browser = '0';  
    if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))  
        $mobile_browser++;  
    if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))  
        $mobile_browser++;  
    if(isset($_SERVER['HTTP_X_WAP_PROFILE']))  
        $mobile_browser++;  
    if(isset($_SERVER['HTTP_PROFILE']))  
        $mobile_browser++;  
    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));  
    $mobile_agents = array(  
        'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',  
        'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',  
        'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',  
        'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',  
        'newt','noki','oper','palm','pana','pant','phil','play','port','prox',  
        'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',  
        'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',  
        'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',  
        'wapr','webc','winw','winw','xda','xda-' 
        );  
    if(in_array($mobile_ua, $mobile_agents))  
        $mobile_browser++;  
    if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)  
        $mobile_browser++;  
    // Pre-final check to reset everything if the user is on Windows  
    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false)  
        $mobile_browser=0;  
    // But WP7 is also Windows, with a slightly different characteristic  
    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false)  
        $mobile_browser++;  
    if($mobile_browser>0)  
        return true;  
    else
        return false;  
}
  
  
}
