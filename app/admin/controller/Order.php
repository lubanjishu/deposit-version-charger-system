<?php
namespace app\admin\controller;
use app\common\model\Config as ConfigModel;
use app\common\model\Withdraw as WithdrawModel;
use app\common\model\OrderPay as OrderPayModel;
use app\common\model\Distributor as DistributorModel;
use think\Db;
use app\common\model\DistributorAccount as DistributorAccountModel;
use app\common\model\DistributorAccountDetail as DistributorAccountDetailModel;
use app\common\model\UserLog as UserLogModel;
use app\common\model\DistributorPrecentage as DistributorPrecentageModel;
use app\common\model\WithdrawAccount as WithdrawAccountModel;
use app\common\WeChat\PayHtml;
/**
 * 订单管理
 */
class Order extends Base
{
	public $table = '';
	public function initialize()
	{
		parent::initialize();
		$this->table = controller('common/order');
		$this->userRole = session('adminInfo.userRole');
	}

	/**
	 * [list 订单管理列表]
	 * @return [type] [description]
	 */
	public function list() {
		//var_dump(input());die();

		$tradeno = input('tradeno','');
        $disid = input('dis_id','');
		$disname = urldecode(input('disname',''));
		$fromtime = input('fromtime','');
    	$endtime = input('endtime','');
    	$o_uname = urldecode(input('o_uname',''));

        $equipment_number = input('equipment_number','');
		$mobile = $this->is_mobile_request();
		$mor ='';
		if( in_array($this->userDisType, [1,2,3]) ){
		    if ($disname) {
                $disIdList = db('distributor')->where('d_name like "%'.$disname.'%"')->column('d_id');
                $disIds = $disIdList ? implode(',',$disIdList) : '';
            }elseif($disid) {
                $disIds = $disid;
            } else {
                //获取属下分销商索引ID
                $disIds = $this->userDisId;
                $oneDistributorIds = db('distributor')->where('d_parent_id='.$this->userDisId.' AND d_status=1')->column('d_id');//一层属下
                if (!empty($oneDistributorIds)) {
                    $oneDistributorIds = implode(',',$oneDistributorIds);
                    $disIds = $disIds.','.$oneDistributorIds;
                    $towDistributorIds = db('distributor')->where('d_parent_id IN ('.$oneDistributorIds.') AND d_status=1')->column('d_id');
                }
                if (!empty($towDistributorIds)) {
                    $towDistributorIds =  implode(',',$towDistributorIds);
                    $disIds = $disIds.','.$towDistributorIds;
                    $thirdDistributorIds = db('distributor')->where('d_parent_id IN ('.$towDistributorIds.') AND d_status=1')->column('d_id');
                }
                $disIds = !empty($thirdDistributorIds) ? implode(',',$thirdDistributorIds).','.$oneDistributorIds : $disIds;
            }
            $disIds = $disIds ? $disIds : -1;
		    $mor = 'o_dis_id IN ('.$disIds.') AND ';
		} else {
		    $mor = $disid ? $mor .'o_dis_id='.$disid.' AND ' : '';
        }
		$where = $mor.' o_line_number > 0';
        if ($tradeno) $where = $where .' AND o_line_number='.$tradeno;
		if ($fromtime) $where = $where.' AND o_paytime >='.strtotime($fromtime);
	    if ($endtime) $where = $where.' AND o_paytime <='.strtotime($endtime);
	    if ($disname) $where = $where.' AND o_dis_name like "%'.$disname.'%"';
	    if ($o_uname) $where = $where.' AND o_uname like "%'.$o_uname.'%"';
        if ($equipment_number) $where = $where.' AND o_dis_equipment_number like "%'.$equipment_number.'%"';
		$qurest = ['tradeno'=>$tradeno,'fromtime'=>$fromtime,'endtime'=>$endtime,'o_dis_name'=>$disname,'equipment_number'=>$equipment_number,'dis_id'=>$disid];
		$resultList = $this->table->getPageList($where,'*','o_paytime DESC',10,$qurest,function ($e) {
            return $e;
        });

		foreach ($resultList as $key => &$value) {
            /*if ($value['o_psd_set_id']) {
                $type_name = db('distributor_price')->where('p_dis_id='.$value['o_dis_id'].' AND p_psd_set_id='.$value['o_psd_set_id'])->value('p_describe');
                $type_name = $type_name ? $type_name : db('distributor_price')->where('p_dis_id=0 AND p_psd_set_id='.$value['o_psd_set_id'])->value('p_describe');
            } else{
                $type_name = ConfigModel::getDesc($value['o_type']);
            }
            $value['type_name'] = $type_name;*/
			$resultList[$key] = $value;
		}
        $page = $resultList->render();
    	$resultList = $resultList->isEmpty() ? '' : $resultList;
    	$this->assign('resultList',$resultList);
    	$this->assign('page',$page);
        $this->assign('mobile',$mobile);
        $this->assign('fromtime',$fromtime);
        $this->assign('endtime',$endtime);
    	$this->assign('disname',$disname);
    	$this->assign('o_uname',$o_uname);
    	$this->assign('tradeno',$tradeno);
        $this->assign('equipment_number',$equipment_number);
    	return $this->fetch();
	}
	//订单退款
    public function refund(){
     //   if( in_array($this->userDisType, [1,2,3])) return reAjaxMsg(0,'您没权操作！');
        if (!$oNumber = input('oNumber','')) return reAjaxMsg(0,'缺少订单编号！');
        return controller('common/order')->orderRefund($this->userDisType,$this->userDisId,$oNumber);
    }

	/**
	 * [delete 订单删除]
	 * @return [type] [description]
	 */
	public function delete() {
		$id = input('o_id',0);

		$order = OrderPayModel::get($id);
		return $order->delete() ? reAjaxMsg(1,'订单删除成功！') :reAjaxMsg(0,'订单删除失败！');

	}

	//订单详情
	public function detail() {
		$id = input('o_id',0);
		$model = OrderPayModel::find($id);
		if ( !$model )
			return reAjaxMsg(0,'未找到相关数据');
		$this->assign('data',$model);
		return $this->fetch();
	}

	/**
	 * [getWithdraw 提现列表]
	 * @return [type] [description]
	 */
	public function getWithdraw	() {
		//密码验证
		$data = WithdrawAccountModel::where('user_id',$this->userId)->find();
		$password = input('password','');
		if (!empty($data->password)) {
			if (session('withdrawPwd') != $data->password) {
				//session('withdrawPwd',NULL);
				if (!$password) {
					$this->assign('url',url('order/getWithdraw'));
					return $this->fetch('inputWithdrawAccountPwd');
				}
				if ($data->password != pswCAM('',$password)) {
					return reAjaxMsg(0,'请输入正确的密码！');
					//return $this->fetch('inputWithdrawAccountPwd');
				} else {
					session('withdrawPwd',pswCAM('',$password));
					return reAjaxMsg(1,'密码审核通过！');
				}
			}
		}

		$fromtime = input('fromtime','');
    	$endtime = input('endtime','');

		$model = WithdrawModel::order('create_time','DESC'); // mark by yann 提现列表数据（绑定了$this->userDisId，是否为首页可调用的数据？）
		$isSystem = 1;
		if( in_array($this->userDisType, [1,2,3]) ){
            $isSystem = 0;
			//$d_id = db('distributor')->where('d_user_id='.$this->userId)->value('d_id');
            $model->where('dis_id',$this->userDisId);
        }
		if ($fromtime ) $model->where('create_time','> time',$fromtime);
    	if ( $endtime ) $model->where('create_time','<= time',$endtime);
    	$model = $model->paginate(10);
		if ( !$model ) return reAjaxMsg(0,'未找到相关数据');

		foreach ($model as $key => &$value) {
			$value['dis_name'] = DistributorModel::find($value->dis_id)['d_name'];
			$sname = $value->status == 1 ? '待审核':($value->status == -1 ? '审核不通过':'审核成功');
			$value['status_name'] = $sname;
		}


		//当前帐户余额
		$data = DistributorAccountModel::where('user_id',$this->userId)->find();
		$balance = empty($data) ? 0.00 : $data->balance;
		$page = $model->render();
		$this->assign('data',$model);
		$this->assign('page',$page);
		$this->assign('fromtime',$fromtime);
    	$this->assign('endtime',$endtime);
        $this->assign('balance',$balance);
        $this->assign('isSystem',$isSystem);
        $this->assign('withdrawValue',db('config')->where('c_id=1')->value('c_withdraw'));
    	$this->assign('is_auth_status',$this->userRole);
		return $this->fetch();
	}
	//设置提现手续费百分比
	public function gotoSetWithdraw()
    {
        $withDraw = input('withdrawValue','');
        if (!$withDraw && $withDraw !== 0) return reAjaxMsg(0,'请输入提现手续费百分比');
        if (!is_numeric($withDraw)) return reAjaxMsg(0,'提现手续费百分比格式错误');
        if( in_array($this->userDisType, [1,2,3]) ) return reAjaxMsg(0,'您没有权限操作！');
        $oldData = db('config')->where('c_id=1')->find();
        $result = db('config')->where('c_id=1')->update(['c_withdraw'=>$withDraw]);
        if ($result) {
            //保存操作记录
            $dologData = [
                'd_newdata' => json_encode(db('config')->where('c_id=1')->find(),JSON_UNESCAPED_UNICODE),
                'd_olddata' => json_encode($oldData,JSON_UNESCAPED_UNICODE),
                'd_adddisid' => $this->userDisId,
                'd_addtime' => myTime(),
                'd_adduid' => $this->userId,
                'd_addname' => $this->userName,
                'd_type'=>3
            ];
            db('distributor_do_log')->insert($dologData);
        }
        return $result ? reAjaxMsg(1,'设置成功！') : reAjaxMsg(0,'设置失败！');
    }

	/**
	 * [doWithdraw 审核操作]
	 * @return [type] [description]
	 */
	public function doWithdraw() {
	    $way = input('way',0);//审核方式【1线上微信企业付款到零钱，2线下】
		if (!$id = input('id',0)) return reAjaxMsg(0,'缺少提现索引ID！');
		$status = input('status',0);
        $model = WithdrawModel::get($id);
        if ( !$model ) return reAjaxMsg(0,'未找到相关数据');
        if ($model['status']!=1) return reAjaxMsg(0,'提现申请已拒绝或已审核通过！');
        if ($model['status'] == -1) return reAjaxMsg(0,'提现申请已拒绝！');
        if ($model['status'] == 2) return reAjaxMsg(0,'提现申请已审核通过！');
        if ($way == 1 && $status == WithdrawModel::STATUS_SUCCESSED && !$openId = db('user')->where('u_disid='.$model->dis_id.' AND u_disuid='.$model->user_id)->value('openid')) return reAjaxMsg(0,'该分销商未绑定微信！');
        //return reAjaxMsg(0,'正在升级中...请稍后再试'.$status,$model);
        $beforedata = $model;
        $model->way = $way;
        $model->status = $status;
		//对冻结资金及分销商流水作处理
        $target_dis_name = db('distributor')->where('d_id='.$model->dis_id)->value('d_name');
		Db::startTrans();
		try {
            if (!$model->save()) {
                Db::rollback();
                return reAjaxMsg(0,'【01】审核失败！');
            }
            //处理冻结金额释放操作
            if (!WithdrawModel::handelFreezeToAdmin($id,$model->dis_id,$model->get_cash,$status,$this->userId)) {
                Db::rollback();
                return reAjaxMsg(0,'【02】审核失败！');
            }
            if ( $status == WithdrawModel::STATUS_SUCCESSED ) {
                //分销商流水
                $trade_no = $model->number;

                //      log_result('de.txt','trade_no::'.$trade_no);die;
                $list = [
                    [//分销商的
                        'dis_id'=>$model->dis_id,
                        'op_way'=>DistributorAccountDetailModel::OPRATION_WAY_GETCASH,
                        'money' =>$model->get_cash,
                        'op_type'=> DistributorAccountDetailModel::MONEY_TYPE_OPRATION_MINUS,
                        'number' => $trade_no,
                        'target_dis_id'=>0,
                        'target_dis_name'=>'平台',
                    ],
                    [//平台的
                        'dis_id'=>0,
                        'op_way'=>DistributorAccountDetailModel::OPRATION_WAY_SHOUXUFEI,
                        'money' =>$model->prenceage,
                        'op_type'=> DistributorAccountDetailModel::MONEY_TYPE_OPRATION_ADD,
                        'number' => $trade_no,
                        'target_dis_id'=>$model->dis_id,
                        'target_dis_name'=>$target_dis_name,
                    ]
                ];
                $datailModel = new DistributorAccountDetailModel();
                if (!$datailModel->saveAll($list)) {
                    Db::rollback();
                    return reAjaxMsg(0,'【03】审核失败！');
                }
                if ($way == 1) {
                    //微信企业付款到零钱
                    $configData = config('wxJsapiPay');
                    $payHtml = new PayHtml($configData['AppID'],$configData['MchId'],$configData['NotifyUrl'],$configData['ApiKey']);
                    $result = $payHtml->payToWeixin($model->real_take_cash,$trade_no,$configData['AppID'],$configData['MchId'],$configData['ApiKey'],$configData['CertificatePath'],$openId);
                    $result =  json_decode($result,true);
                    if ($result['id'] == 0) {
                        Db::rollback();
                        return reAjaxMsg(0,$result['msg']);
                    }
                }
            }
            Db::commit();
            $afterdata = WithdrawModel::find($id);
            WithdrawModel::handleWithdrawToAdmin($this->userId,$this->userName,$beforedata,$afterdata);
            return reAjaxMsg(1,'操作成功');
		} catch( \Exception $e ) {
			Db::rollback();
			return reAjaxMsg(0,'操作失败'.$e->getMessage());
		}
	}

	/**
	 * [withdrawDel 提现删除]
	 * @return [type] [description]
	 */
	public function withdrawDel() {
		$id = input('id',0);
		$order = WithdrawModel::get($id);

		return $order->delete() ? reAjaxMsg(1,'操作成功！') :reAjaxMsg(0,'操作失败！');
	}

	/**
	 * [finacial 财务统计]
	 * @return [type] [description]
	 */
	public function financial() {
		$disId = input('disId',"0");
        $d_type=input('d_type',"");
        $disname = urldecode(input('disname',''));
        $user_phone = urldecode(input('user_phone',''));
        $fromtime = input('fromtime','');
        $endtime = input('endtime','');

        $where  = 'o_line_number > 0 and o_isrefund=1 ';

        $whereDis = $disname || $user_phone ? 'd_parent_id=0' : '1 = 1 ';  //  $whereDis   de_distributor表
        $equipmentWhere = 'd_status != -1';
        if( in_array($this->userDisType, [1,2,3]) || $disId) {
            $disId = $disId ? $disId : $this->userDisId;
            $whereDis = 'd_parent_id='.$disId;
        }
        /*
        if ($disname && !$disId) $disId = db('distributor')->where('d_name="'.$disname.'"')->value('d_id');
        if ($user_phone && !$disId) $disId = db('distributor')->where('d_user_phone="'.$user_phone.'"')->value('d_id');
        if ($disname) $whereDis = $whereDis.' AND d_name="'.$disname.'"';

       if ($d_type) $whereDis = $d_type.' AND d_type="'.$d_type.'"';

        if ($user_phone) $whereDis = $whereDis.' AND d_user_phone="'.$user_phone.'"';

        if ($fromtime) $whereDis = $whereDis.' AND d_addtime >="'.$fromtime.' 00:00:00"';
        if ($endtime) $whereDis = $whereDis.' AND d_addtime <="'.$endtime.' 23:59:59"';

      $fromtime =strtotime("'.$fromtime.' 00:00:00");
      $endtime =strtotime("'.$fromtime.' 23:59:59");
         if ($fromtime) $where = $where.' AND o_paytime>="'.$fromtime.' "';
        if ($endtime) $where = $where.' AND o_paytime <="'.$endtime.' 23:59:59"';
        $where = $where.' AND o_dis_id='.$disId;*/


        if ($disname && !$disId) {
            $disIds = db('distributor')->where('d_name like "%'.$disname.'%"')->column('d_id');
            $disIds = $disIds ? $disIds : '-9999';
        }
        if ($user_phone && !$disId) {
            $d_id = db('distributor')->where('d_user_phone="'.$user_phone.'"')->value('d_id');
            $disId = $d_id ? $d_id : '-9999';
        }
        if ($disname) $whereDis = $whereDis.' AND d_name like "%'.$disname.'%"';

       if ($d_type) $whereDis = $d_type.' AND d_type="'.$d_type.'"';

        if ($user_phone) $whereDis = $whereDis.' AND d_user_phone="'.$user_phone.'"';

        if ($fromtime) $whereDis = $whereDis.' AND d_addtime >="'.$fromtime.' 00:00:00"';
        if ($endtime) $whereDis = $whereDis.' AND d_addtime <="'.$endtime.' 23:59:59"';

      $fromtime =strtotime("'.$fromtime.' 00:00:00");
      $endtime =strtotime("'.$fromtime.' 23:59:59");
         if ($fromtime) $where = $where.' AND o_paytime>="'.$fromtime.' "';
        if ($endtime) $where = $where.' AND o_paytime <="'.$endtime.' 23:59:59"';
        if (!empty($disIds)) $disIds = !in_array($disId,$disIds) ? '-9999' :  implode(',',$disIds);
        $where = !empty($disIds) ? $where.' AND o_dis_id IN ('.$disIds.')' : $where.' AND o_dis_id='.$disId;

        /* -------------------------------------------------------------------
           总体统计，含下属的 今日订单总金额、累计总金额、设备数量、订单数量、实际收益
        ----------------------------------------------------------------------*/
        if ( $disId) {
            //获取属下分销商索引ID
            $disIdData = [];
            $oneDistributor = db('distributor')->where('d_parent_id='.$disId.' AND d_status=1')->column('d_id');//一层属下
            if ($oneDistributor) {
                $disIdData = $oneDistributor;
                foreach ($oneDistributor as $vDisId) {
                    //二层属下
                    if ($towDistributor = db('distributor')->where('d_parent_id='.$vDisId.' AND d_status=1')->column('d_id')) {
                        $disIdData = array_merge($disIdData,$towDistributor);
                        foreach ($towDistributor as $vTowDisId) {
                            if ($thirdDistributor = db('distributor')->where('d_parent_id='.$vTowDisId.' AND d_status=1')->column('d_id')) {
                                $disIdData = array_merge($disIdData,$thirdDistributor);
                            };//三层属下
                        }
                    }
                }
                $disIdData = array_unique($disIdData);
            }
            //处理属下分销商索引ID，并加上自己的分销商索引ID
            $disIdData = implode(',',$disIdData);
            $disIdData = $disIdData ? $disIdData.','.$disId : $disId;
            $equipmentWhere = 'd_status != -1 AND (d_dis_id IN('.$disIdData.') OR d_dis_id1 IN ('.$disIdData.') OR d_dis_id2 IN('.$disIdData.'))';
            $allWhere = $disIdData ? ' o_isrefund =1  and o_line_number > 0 AND o_dis_id IN ('.$disIdData.')' : $where;
        } else {
            $equipmentWhere = $equipmentWhere.' AND (d_dis_id='.$disId.' OR d_dis_id1='.$disId.' OR d_dis_id2='.$disId.')';
            $allWhere = '  o_isrefund =1  and o_line_number > 0';
        }
        $allData = $this->countTotal($allWhere,'op_way!=2 AND dis_id='.$disId,$equipmentWhere,$disId);

    	/* -------------------------------------------------------------------
    	   统计我的 今日订单总金额、累计总金额、设备数量、订单数量、实际收益
    	----------------------------------------------------------------------*/
    	$myEquipmentWhere = 'd_status != -1  AND (d_dis_id='.$disId.' OR d_dis_id1='.$disId.' OR d_dis_id2='.$disId.')';
    	$myData = $this->countTotal($where,' op_way!=2 and dis_id='.$disId,$myEquipmentWhere,$disId);


        /* -------------------------------------------------------------------
           统计下属 今日订单总金额、累计总金额、设备数量、订单数量、实际收益
        ----------------------------------------------------------------------*/
        $config = ['disId'=>$disId,'disname'=>$disname,'user_phone'=>$user_phone,'fromtime'=>$fromtime,'endtime'=>$endtime];
        $resultList = db('distributor')->field('d_id,d_name,d_type')->where($whereDis)->paginate(10,false,$config)->each(function($value){
            if ($value['d_type'] == 3) { //三级分销商
                $disIdData = [$value['d_id']];
            } elseif ($value['d_type'] == 2) { //二级分销商
                //获取下属三级分销商
                $disIdData = [$value['d_id']];
                if ($towDistributor = db('distributor')->where('d_parent_id='.$value['d_id'].' AND d_status=1')->column('d_id')) {
                    $disIdData = array_unique(array_merge($disIdData,$towDistributor));
                }
            } elseif ($value['d_type'] == 1) {
                //获取下属二级分销商
                $disIdData = [$value['d_id']];
                if ($towDistributor = db('distributor')->where('d_parent_id='.$value['d_id'].' AND d_status=1')->column('d_id')) {
                    $disIdData = array_merge($disIdData,$towDistributor);
                    foreach ($towDistributor as $vTowDisId) {
                        if ($thirdDistributor = db('distributor')->where('d_parent_id='.$vTowDisId.' AND d_status=1')->column('d_id')) {
                            $disIdData = array_merge($disIdData,$thirdDistributor);
                        };//三层属下
                    }
                    $disIdData = array_unique($disIdData);
                }
            }
            $disIdStr = implode(',',$disIdData);
            $equipmentWhere = 'd_status != -1 AND (d_dis_id IN('.$disIdStr.') OR d_dis_id1 IN ('.$disIdStr.') OR d_dis_id2 IN('.$disIdStr.'))';
            $countWhere = 'op_way!=2 AND dis_id='.$value['d_id'];
            $value['countTotal'] = $this->countTotal('o_line_number > 0 AND o_dis_id IN('.$disIdStr.') and o_isrefund =1',$countWhere,$equipmentWhere,$value['d_id']);
            $value['d_type_name'] = $value['d_type'] == 1 ? '渠道' : ($value['d_type'] == 2 ? '代理' : '商家');//订单总数量
            $disAccountInfo = db('distributor_account')->where('dis_id='.$value['d_id'])->find();
            $value['balance'] = $disAccountInfo ? $disAccountInfo['balance'] : 0;
            $value['freeze'] = $disAccountInfo ? $disAccountInfo['freeze'] : 0;
            return $value;
        });
        $page = $resultList->render();
        $resultList = $resultList->isEmpty() ? '' : $resultList;

        $this->assign('resultList',$resultList);
        $this->assign('page',$page);
        $this->assign('myData',$myData);
        $this->assign('allData',$allData);
        $this->assign('disId',$disId);
        $this->assign('fromtime',input('fromtime',''));
        $this->assign('endtime',input('endtime',''));
        $this->assign('disname',$disname);
        $this->assign('user_phone',$user_phone);
        $this->assign('disName',db('distributor')->where('d_id='.$disId)->value('d_name'));
    	return $this->fetch();
	}




  	/**
	 * [finacial 财务统计]
	 * @return [type] [description]
	 */
	public function financialNew() {
		$disId = input('disId',0);
        $d_type=input('d_type',"");
        $disname = urldecode(input('disname',''));
        $user_phone = urldecode(input('user_phone',''));
        $fromtime = input('fromtime','');
        $endtime = input('endtime','');

        $where  = 'o_line_number > 0 and o_isrefund=1 ';

        $whereDis = $disname || $user_phone ? 'd_parent_id=0' : '1 = 1 ';  //  $whereDis   de_distributor表
        $equipmentWhere = 'd_status != -1';
        if( in_array($this->userDisType, [1,2,3]) || $disId) {
            $disId = $disId ? $disId : $this->userDisId;
            $whereDis = 'd_parent_id='.$disId;
        }
        if ($disname && !$disId) $disId = db('distributor')->where('d_name="'.$disname.'"')->value('d_id');
        if ($user_phone && !$disId) $disId = db('distributor')->where('d_user_phone="'.$user_phone.'"')->value('d_id');
        if ($disname) $whereDis = $whereDis.' AND d_name="'.$disname.'"';

       if ($d_type) $whereDis = $d_type.' AND d_type="'.$d_type.'"';

        if ($user_phone) $whereDis = $whereDis.' AND d_user_phone="'.$user_phone.'"';

     //    if ($fromtime) $whereDis = $whereDis.' AND d_addtime >="'.$fromtime.' 00:00:00"';
     //   if ($endtime) $whereDis = $whereDis.' AND d_addtime <="'.$endtime.' 23:59:59"';









      //----------------时间的处理
      $fromtimes =strtotime("'.$fromtime.' 00:00:00");
      $endtimes =strtotime("'.$fromtime.' 23:59:59");
        if ($fromtime) $where = $where.' AND o_paytime>="'.$fromtimes.' "';
        if ($endtime) $where = $where.' AND o_paytime <="'.$endtimes.' "';
      //----------------时间的处理









        $where = $where.' AND o_dis_id='.$disId;

        /* -------------------------------------------------------------------
           总体统计，含下属的 今日订单总金额、累计总金额、设备数量、订单数量、实际收益
        ----------------------------------------------------------------------*/

        if ( $disId=="0") {
            //获取属下分销商索引ID
            $disIdData = [];
            $oneDistributor = db('distributor')->where('d_parent_id='.$disId.' AND d_status=1')->column('d_id');//一层属下
            if ($oneDistributor) {
                $disIdData = $oneDistributor;
                foreach ($oneDistributor as $vDisId) {
                    //二层属下
                    if ($towDistributor = db('distributor')->where('d_parent_id='.$vDisId.' AND d_status=1')->column('d_id')) {
                        $disIdData = array_merge($disIdData,$towDistributor);
                        foreach ($towDistributor as $vTowDisId) {
                            if ($thirdDistributor = db('distributor')->where('d_parent_id='.$vTowDisId.' AND d_status=1')->column('d_id')) {
                                $disIdData = array_merge($disIdData,$thirdDistributor);
                            };//三层属下
                        }
                    }
                }
                $disIdData = array_unique($disIdData);
            }
            //处理属下分销商索引ID，并加上自己的分销商索引ID
            $disIdData = implode(',',$disIdData);
            $disIdData = $disIdData ? $disIdData.','.$disId : $disId;
            $equipmentWhere = 'd_status != -1 AND (d_dis_id IN('.$disIdData.') OR d_dis_id1 IN ('.$disIdData.') OR d_dis_id2 IN('.$disIdData.'))';
            $allWhere = $disIdData ? ' o_isrefund =1  and o_line_number > 0 AND o_dis_id IN ('.$disIdData.')' : $where;
        } else {
            $equipmentWhere = $equipmentWhere.' AND (d_dis_id='.$disId.' OR d_dis_id1='.$disId.' OR d_dis_id2='.$disId.')';
            $allWhere = '  o_isrefund =1  and o_line_number > 0';
        }

        $allData = $this->countTotal($allWhere,'op_way!=2 AND dis_id='.$disId,$equipmentWhere,$disId,$fromtime ,$endtime);

    	/* -------------------------------------------------------------------
    	   统计我的 今日订单总金额、累计总金额、设备数量、订单数量、实际收益
    	----------------------------------------------------------------------*/
    	$myEquipmentWhere = 'd_status != -1  AND (d_dis_id='.$disId.' OR d_dis_id1='.$disId.' OR d_dis_id2='.$disId.')';
    	$myData = $this->countTotal($where,' op_way!=2 and dis_id='.$disId,$myEquipmentWhere,$disId,$fromtime ,$endtime);


        /* -------------------------------------------------------------------
           统计下属 今日订单总金额、累计总金额、设备数量、订单数量、实际收益
        ----------------------------------------------------------------------*/
        $config = ['disId'=>$disId,'disname'=>$disname,'user_phone'=>$user_phone,'fromtime'=>$fromtime,'endtime'=>$endtime];


       //  var_dump($fromtime);
        $resultList = db('distributor')->field('d_id,d_name,d_type')->where($whereDis)->paginate(10,false,$config)->each(function($value){
        $fromtime = input('fromtime','');
        $endtime = input('endtime','');

      //   var_dump($fromtime);
            if ($value['d_type'] == 3) { //三级分销商
                $disIdData = [$value['d_id']];
            } elseif ($value['d_type'] == 2) { //二级分销商
                //获取下属三级分销商
                $disIdData = [$value['d_id']];
                if ($towDistributor = db('distributor')->where('d_parent_id='.$value['d_id'].' AND d_status=1')->column('d_id')) {
                    $disIdData = array_unique(array_merge($disIdData,$towDistributor));
                }
            } elseif ($value['d_type'] == 1) {
                //获取下属二级分销商
                $disIdData = [$value['d_id']];
                if ($towDistributor = db('distributor')->where('d_parent_id='.$value['d_id'].' AND d_status=1')->column('d_id')) {
                    $disIdData = array_merge($disIdData,$towDistributor);
                    foreach ($towDistributor as $vTowDisId) {
                        if ($thirdDistributor = db('distributor')->where('d_parent_id='.$vTowDisId.' AND d_status=1')->column('d_id')) {
                            $disIdData = array_merge($disIdData,$thirdDistributor);
                        };//三层属下
                    }
                    $disIdData = array_unique($disIdData);
                }
            }
            $disIdStr = implode(',',$disIdData);
            $equipmentWhere = 'd_status != -1 AND (d_dis_id IN('.$disIdStr.') OR d_dis_id1 IN ('.$disIdStr.') OR d_dis_id2 IN('.$disIdStr.'))';
            $countWhere = 'op_way!=2 AND dis_id='.$value['d_id'];
            $value['countTotal'] = $this->countTotal('o_line_number > 0 AND o_dis_id IN('.$disIdStr.') and o_isrefund =1',$countWhere,$equipmentWhere,$value['d_id'],$fromtime ,$endtime );
            $value['d_type_name'] = $value['d_type'] == 1 ? '渠道' : ($value['d_type'] == 2 ? '代理' : '网吧');//订单总数量
            return $value;
        });
        $page = $resultList->render();
        $resultList = $resultList->isEmpty() ? '' : $resultList;

              $this->assign('userDisType',$this->userDisType);
        $this->assign('resultList',$resultList);
        $this->assign('page',$page);
        $this->assign('myData',$myData);
        $this->assign('allData',$allData);
        $this->assign('disId',$disId);
        $this->assign('d_type',$d_type);
        $this->assign('fromtime',$fromtime);
        $this->assign('endtime',$endtime);
        $this->assign('disname',$disname);
        $this->assign('user_phone',$user_phone);
        $this->assign('disName',db('distributor')->where('d_id='.$disId)->value('d_name'));
    	return $this->fetch();
	}











	public function countTotal($orderWhere,$countWhere,$equipmentWhere,$disId ,$fromtime ="",$endtime ="" ) {
        $result['orderTotalCount'] = $this->table->getCount($orderWhere);//订单总数量
        $result['allTotal'] = $this->table->getFieldSum($orderWhere,'o_price');//累计总金额，即订单总金额
        if ($disId == 0) {//平台
            $result['equipmentTotalCount'] = db('equipments')->where('e_status=2')->count();//设备总数量
        } else {
            $result['equipmentTotalCount'] = db('distributor_equipments')->where($equipmentWhere)->count();//设备总数量
        }
        //$result['equipmentTotalCount'] = count(array_unique($this->table->getFieldColumn($orderWhere,'o_dis_equipment_id')));//设备总数量
        //今日收益，即今日订单总金额
        $starTime = strtotime(date('Y-m-d',time()).' 00:00:00');
        $endTime = strtotime(date('Y-m-d',time()).' 23:59:59');

        //昨日收益
        $yesterdayStart  =strtotime(date('Y-m-d',strtotime("-1 day")).' 00:00:00');
        $yesterdayEnd  =  strtotime(date('Y-m-d',strtotime("-1 day")).' 23:59:59');

      //时间段的收益
      $fromtimes =strtotime($fromtime." 00:00:00");
      $endtimes =strtotime($endtime." 23:59:59");

        $result['todayAllTotal'] = $this->table->getFieldSum($orderWhere.' AND o_paytime >= "'.$starTime.'" AND o_paytime <= "'.$endTime.'"','o_price');

        $result['yesterdayTotal'] = $this->table->getFieldSum($orderWhere.' AND o_paytime >= "'.$yesterdayStart.'" AND o_paytime <= "'.$yesterdayEnd.'"','o_price');

      	$result['timeTotal'] = $this->table->getFieldSum($orderWhere.' AND o_paytime >= "'.$fromtimes.'" AND o_paytime <= "'.$endtimes.'"','o_price');

//      var_dump( 	$result['timeTotal']);

        $result['todayEquipmentTotalCount'] = count(array_unique($this->table->getFieldColumn($orderWhere.' AND o_paytime >= "'.$starTime.'" AND o_paytime <= "'.$endTime.'"','o_dis_equipment_id')));//今日设备总数量
        //实际收益
        $result['accountAllTotal'] = db('distributor_account_detail')->where($countWhere.' AND status=1')->sum('money');
        //退款金额
        $result['tuikuan'] = db('distributor_account_detail')->where($countWhere.' AND status=1 and op_type =2')->sum('money');

        $result['accountAllTotal'] =round($result['accountAllTotal'] - $result['tuikuan']- $result['tuikuan'],2);


        return $result;
    }

	/**
	 * [equipmentWithDrawApply 分销商后台--分销商提现申请]
	 * @return [type] [description]
	 */
	public function equipmentWithDrawApply() {
		$total = input('cash',0);
		if ( !is_numeric($total) ) return reAjaxMsg(0,'金额格式错误');
		if ( $total < 0.3 ) return reAjaxMsg(0,'提现金额须大于50');

		$model = DistributorAccountModel::where('user_id',$this->userId)->find();
		if ( empty($model) ) return reAjaxMsg(0,'未找到相关数据');
		if ( $model->balance < $total ) return reAjaxMsg(0,'提现金额不可大于余额');

		//计算上级分销商佣金
		$data = DistributorModel::find($model->dis_id);
		if ( empty($data) ) return reAjaxMsg(0,'没有此分销商');

		$flag = WithdrawModel::handleWithdrawFreeze($model->dis_id,0,$total,$total,$this->userId);
		//佣金已经在消费中抽取，无需提现时抽取
		/*if ( empty($data->d_parent_id) ) {
			//没有上级分销商 无佣金
			$flag = WithdrawModel::handleWithdrawFreeze($model->dis_id,0,$total,$total,$this->userId);
		} else {
			//有分销商 有佣金
			$disPrecent = DistributorPrecentageModel::where('dis_id',$data->d_parent_id)->find();

			if ( $data->d_parent_id == $disPrecent->dis_id ) {

				if ( $model->dis_id == $disPrecent->asgd_dis_id ) {
					//该分销商有指定佣金
					$yongjing = $total * ($disPrecent->asgd_precentage / 100);	//上级分销所得[$data->d_parent_id]
				} else {
					//该分销商没有指定佣金 用常规佣金计算
					$yongjing = $total * ($disPrecent->percentage / 100);	//上级分销所得[$data->d_parent_id]
				}
			} else {
				$yongjing = 0;
			}
			$real_take = $total - $yongjing;		//自己所得[$this->disEuqipment['d_dis_id']]
			//提现冻结处理
			$flag = WithdrawModel::handleWithdrawFreeze($model->dis_id,$yongjing,$real_take,$total,$this->userId);
		}*/

		if (is_array($flag)) return reAjaxMsg(0,$flag[0]);
		return $flag ? reAjaxMsg(1,'已提交申请') : reAjaxMsg(0,'提交申请失败');
	}

	//提现帐号设置
	public function setWithdrawAccount() {
		$data = WithdrawAccountModel::where('user_id',$this->userId)->find();
		$password = input('password','');
		if (!empty($data->password)) {
			if (session('withdrawPwd') != $data->password) {
				session('withdrawPwd',NULL);
				if (!$password) {
					$this->assign('url',url('order/setWithdrawAccount'));
					return $this->fetch('inputWithdrawAccountPwd');
				}
				if ($data->password != pswCAM('',$password)) {
					return reAjaxMsg(0,'请输入正确的密码！');
					//return $this->fetch('inputWithdrawAccountPwd');
				} else {
					session('withdrawPwd',pswCAM('',$password));
					return reAjaxMsg(1,'密码审核通过！');
				}
			}
		}
		$this->assign('data',$data);
		return $this->fetch();

	}

	/**
	 * [addWithDrawAccount 添加提现帐号]
	 */
	public function addWithDrawAccount() {
		$b_account = input('b_account',0);
		$b_name = input('b_name','');
		$bs_name = input('bs_name','');
		$bu_name = input('bu_name','');
		$bu_phone = input('bu_phone',0);
		$password = input('password',0);

		if ( !$b_account ) {
			return reAjaxMsg(0,'银行卡号不能为空');
		}
		if ( !checkBank($b_account) ) {
			return reAjaxMsg(0,'银行卡号格式错误');
		}
		if ( !$bs_name )
			return reAjaxMsg(0,'银行名称不能为空');
		if ( !$bu_name )
			return reAjaxMsg(0,'银行支行名秒不能为空');
		if ( !$bu_phone )
			return reAjaxMsg(0,'手机号不能为空');
		if ( !$password )
			return reAjaxMsg(0,'请输入密码！');

		$model = WithdrawAccountModel::where('user_id',$this->userId)->find();

		if ( !empty($model) ) {
			//更新
			$model->bank_account = $b_account;
			$model->bank_name = $b_name;
			$model->bank_sonname = $bs_name;
			$model->user_name = $bu_name;
			$model->user_phone = $bu_phone;
			$model->password = pswCAM('',$password);
			$flag = $model->force()->save();
		} else {

			//添加
			$data = array(
				'user_id' => $this->userId,
				'bank_account'=>$b_account,
				'bank_name'=>$b_name,
				'bank_sonname'=>$bs_name,
				'user_name'=>$bu_name,
				'user_phone'=>$bu_phone,
				'password'=>pswCAM('',$password)
			);
		//	$flag = WithdrawAccountModel::create($data);
                 $flag = db('withdraw_account')->insert($data);
     //     var_dump($flag);die;
		}
		if ( $flag )
			return reAjaxMsg(1,'操作成功');
		return reAjaxMsg(0,'操作失败');

	}



  	public function report() {

        $dis_id = input('dis_id','');//分销商ID
      	$target_dis_name = urldecode(input('target_dis_name',''));

		$fromtime = input('fromtime','');//开始时间
        $endtime = input('endtime','');//结束时间
        $name = urldecode(input('name',''));//分销商名称
        $user_phone = input('user_phone','');//分销商手机号码
        $opway = input('opway','');//类型

        $times = input('times','');  //1昨天 2今天 3本周 4本月 6上周



      	if ($name){
          $dis_info = db('distributor')->where(" d_name like "."'%$name%'")->find();
      	$dis_id =$dis_info['d_id'];
        }
        $where = in_array($this->userDisType, [1,2,3]) ? 'status != -1 AND dis_id='.$this->userDisId : 'status != -1';
      	if ($dis_id) $where .= ' AND status=1 AND dis_id = '.$dis_id;
        if ($target_dis_name) $where .= ' AND target_dis_name like '."'%$target_dis_name%'" ;

        if ($fromtime) $where .= ' AND create_time >='.strtotime($fromtime.' 00:00:00');
        if ($endtime) $where .= ' AND create_time <='.strtotime($endtime.' 23:59:59');

      if($times ==1){
      	$where .= ' AND create_time >='.strtotime('yesterday');
         $where .= ' AND create_time <='.(strtotime(date('Y-m-d')) - 1);

        $fromtime	=  date("Y-m-d",strtotime('yesterday'));
        $endtime	=  date("Y-m-d",strtotime(date('Y-m-d')) - 1);
      }
      if($times ==2){
               $where .= ' AND create_time >='.strtotime(date('Y-m-d'));

        $fromtime	=  date("Y-m-d",strtotime(date('Y-m-d')));
        $endtime  =  date("Y-m-d",strtotime(date('Y-m-d')));

      }
      if($times ==3){
      $beginThisweek = mktime(0,0,0,date('m'),date('d')-date('w')+1,date('y'));

        $where .= ' AND create_time >='.$beginThisweek;

        $fromtime= date("Y-m-d",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y")));
    	$endtime= date("Y-m-d",mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y")));

      }
      if($times ==4){
        $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
		$endThismonth=mktime(23,59,59,date('m'),date('t'),date('Y'));
         $where .= ' AND create_time >='.$beginThismonth;
         $where .= ' AND create_time <='. $endThismonth;
        $fromtime	=  date("Y-m-d",mktime(0, 0 , 0,date("m"),1,date("Y")));
        $endtime	=  date("Y-m-d",mktime(23,59,59,date("m"),date("t"),date("Y")));

      }

      if($times ==5){
        $begin_time = strtotime(date('Y-m-01 00:00:00',strtotime('-1 month')));
        $end_time = strtotime(date("Y-m-d 23:59:59", strtotime(-date('d').'day')));
         $where .= ' AND create_time >='.$begin_time;
         $where .= ' AND create_time <='. $end_time;


      }

      if($times ==6){
          $beginLastweek=mktime(0,0,0,date('m'),date('d')-date('w')+1-7,date('Y'));
          $endLastweek=mktime(23,59,59,date('m'),date('d')-date('w')+7-7,date('Y'));
         $where .= ' AND create_time >='.$beginLastweek;
         $where .= ' AND create_time <='. $endLastweek;

        $fromtime = date("Y-m-d",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1-7,date("Y")));
    	$endtime = date("Y-m-d",mktime(23,59,59,date("m"),date("d")-date("w")+7-7,date("Y")));
      }


        if ($opway) $where .= ' AND op_way ='.$opway;
        $distributorAccountDetailMod = model('distributorAccountDetail');
        $config = ['fromtime'=>$fromtime,'endtime'=>$endtime,'name'=>$name,'user_phone'=>$user_phone];

        $resultList = $distributorAccountDetailMod->where($where)->order('create_time DESC,id DESC')->paginate(20,false,$config)->each(function($vList){
            $op_way = [1=>'收益',2=>'提现',3=>'佣金',4=>'返还佣金',5=>'提现手续费',6=>'退款'];
            $disInfo = DistributorModel::where('d_id='.$vList->dis_id)->find();
            $vList['op_way'] = isset($op_way[$vList['op_way']]) ? $op_way[$vList['op_way']] : '类型有误！';
            $vList['dis_name'] = !empty($disInfo['d_name']) ? $disInfo['d_name'] : '平台';
            $vList['user_name'] = !empty($disInfo['d_user_name']) ? $disInfo['d_user_name'] : '平台';
            $vList['user_phone'] = !empty($disInfo['d_user_phone']) ? $disInfo['d_user_phone'] : '';

            return $vList;
        });



      $distributor = db('distributor')->select();
      foreach($distributor as $val){
              foreach($resultList as $v){
                if($val['d_id']==$v['dis_id']){
                    $v['d_type_name'] = $val['d_type'] == 1 ? '渠道商' : ($val['d_type'] == 2 ? '代理商' : '网吧');//订单总数量
               //   echo  $val['d_type_name'];
                }

     	 }
      }



         //   var_dump($resultList);

			$all_money = $distributorAccountDetailMod->where($where)->sum('money');  //统计全部钱
       		$where .= ' AND status=1 and op_type =2 ';
      		$tui = $distributorAccountDetailMod->where($where)->sum('money');
			$all_money  =$all_money- $tui-$tui;
        $page = $resultList->render();
        $resultList = $resultList->isEmpty() ? '' : $resultList;
        $this->assign('resultList',$resultList);
        $this->assign('page',$page);
        $this->assign('fromtime',$fromtime);
        $this->assign('endtime',$endtime);
        $this->assign('name',$name);
      $this->assign('target_dis_name',$target_dis_name);

      $this->assign('all_money',$all_money);
              $this->assign('dis_id',$dis_id);
        $this->assign('user_phone',$user_phone);
        $this->assign('opway',$opway);
        return $this->fetch();
    }


}
