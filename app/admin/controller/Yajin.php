<?php
namespace app\admin\controller;
use app\common\model\Config as ConfigModel;
use app\common\model\Withdraw as WithdrawModel;
use app\common\model\OrderPay as OrderPayModel;
use app\common\model\Distributor as DistributorModel;
use app\common\WeChat\Refund;
use think\Db;
use app\common\model\DistributorAccount as DistributorAccountModel;
use app\common\model\DistributorAccountDetail as DistributorAccountDetailModel;
use app\common\model\UserLog as UserLogModel;
use app\common\model\DistributorPrecentage as DistributorPrecentageModel;
use app\common\model\WithdrawAccount as WithdrawAccountModel;
use app\common\WeChat\PayHtml;
/**
 * 订单管理
 */
class Yajin extends Base
{
    private $userCom = '';
    private $userRole = '';
    public function initialize()
    {
        parent::initialize();
        $this->userCom = controller('common/user');
        $this->userRole = session('adminInfo.userRole');
    }

    //会员列表页面
    public function list(){
        $phone = input('phone','');
        $userName = input('userName','');
        $status = input('status','');

        $where = $phone ? 'u_type !=2 AND u_phone="'.$phone.'"': 'u_type !=2';
        $where = $status ? $where.' AND u_status='.$status : $where;
        if(in_array($this->userDisType, [1,2,3])){
            $where .= ' and u_adduid='.$this->userId;
        }
        if (!empty($userName)){
            $where.=' and u_name ="'.$userName.'"';
        }

        $userList = model('User')->getUserPage($where,'*', 10,'u_addtime DESC',['phone'=>$phone,'u_status'=>$status]);
        $page = $userList->render();
        $userList = $userList->isEmpty() ? '' : $userList;

        $this->assign("userList",$userList);
        $this->assign("page",$page);
        $this->assign("phone",$phone);
        $this->assign("userName",$userName);
        return $this->fetch();
    }

    public function refund(){
        $uId = input('u_id');
        $userInfo =  db('user')->where('u_id=' . $uId)->find();
        $openId = $userInfo['openid'];
        if (!$openId) return reAjaxMsg(0, '用户数据异常！');
        //获取用户押金
        $money = $userInfo['yajin_money'];
        if ($money <= 0) {
            return reAjaxMsg(0, "当前余额不足,无法退款");
        }
        if (!is_numeric($openId)) {
            $orderNumber = "wx" . myTime('Ymdhis') . getRandNen(8);
        } else {
            $orderNumber = "al" . myTime('Ymdhis') . getRandNen(8);
        }
        Db::startTrans();
        //扣除押金
        \db('user')->where('u_id', $this->userId)->dec('yajin_money', $money)->update();
        if (!is_numeric($openId)) {

            //微信打款
            $configData = config('wxJsapiPay');
            $refund = new Refund($configData['AppID'], $configData['MchId'], $configData['NotifyUrl'], $configData['ApiKey']);
            $result = $refund->payToWeixin2($money, $orderNumber, $configData['AppID'], $configData['MchId'], $configData['ApiKey'], $configData['CertificatePath'], $openId, "押金提现");
            if ($result['id'] == 0) {
                Db::rollback();
                return reAjaxMsg(1, $result['msg']);
            }
        } else {
            //转账
            $config = config('alipayApp');
            $result = (new \app\common\AliPay\AliPay())->transfer($openId, $orderNumber, $money, "押金提现");
            if ($result['id'] == 0) {
                Db::rollback();
                return reAjaxMsg(1, $result['msg']);
            }
        }
        Db::commit();
        return reAjaxMsg(0, "退款成功,到账{$money} 元");
    }
}
