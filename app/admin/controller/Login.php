<?php
namespace app\admin\controller;

use think\Controller;

class Login extends controller
{
	/*========================================================================================
		
											界面部分

	========================================================================================*/
	public function index()
	{
        if (session('?adminInfo')) {
            $this->redirect(url('index/index'));
            exit;
        }
        //设置页面标题
        $this->assign('pageTitle',pageTitle(request()->module(),request()->controller().'/'.request()->action()));
		return $this->fetch();
	}
	/*========================================================================================
		
											功能部分

	========================================================================================*/
	public function gotoLogin(){
        $account = input('account');
        $password = input('password');
        if (!$account) {
            return reAjaxMsg(0,'请输入手机账号！');
        }
        if (!$password) {
            return reAjaxMsg(0,'请输入密码！');
        }
        if (session('?adminInfo.adminAccount')) {
            return reAjaxMsg(0,session('adminInfo.adminAccount').'：该用户已登录！');
        }
        $userCom = controller('common/user');
        $login_resutle = $userCom->login($account,$password, $userCom::$adminTypeId);
        if ($login_resutle != 'ok') {
            return reAjaxMsg(0,$account."：".$login_resutle);
        }
        return reAjaxMsg(1,'已成功登录！');
	}

    //退出登录
    public function logout(){
        session('adminInfo', NULL);
        session('withdrawPwd',NULL);
        return reAjaxMsg(1,'已安全退出！');
    }


}