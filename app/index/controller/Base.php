<?php
namespace app\index\controller;

use think\Controller;

class Base extends Controller
{
    public function initialize()
    {
        $con = request()->controller();
        $cf = $con.'/'.request()->action();
        //获取用户信息
        $this->assign('userInfo',session('?userInfo') ? session('userInfo') : '');
        //设置页面标题
        $this->assign('pageTitle',pageTitle(request()->module(),$cf));
        //设置头部标题
        $this->assign('headerTitle',pageTitle(request()->module(),$cf,false));
        //设置WAP端页脚菜单样式变量
        $this->assign('footerStype',$con);
        //检测用户是否认证
        $this->checkUserAuthentication();
    }
    //检测用户是否认证
    public function checkUserAuthentication(){

        return false;
    }
}