<?php

/*
 * 界面标题设置
 * $
 *
 */

//设置页面名称
function pageTitle($m, $cf, $HCMP = true){
	$cf = strtolower($cf);
    $app_list = array(
        'admin'=> array(
            'index/index'                           =>'系统后台',
            //菜单
            'menu/addmenu'                          =>'添加菜单',
            'menu/editmenu'                         =>'编辑菜单',
            'menu/menulist'                         =>'菜单列表',
            //会员
            'user/adduser'                          =>'添加会员',
            'user/userList'                         =>'会员列表'
        ),
        'index'=> array(
            'index/index'                           =>'首页',
            'login/index'                           =>'登录',
            'login/register'                        =>'会员注册',
            //会员
            'user/index'                            =>'会员中心',
            'user/usermessage'                      =>'我的消息',
            'user/userinfo'                         =>'我的资料',
            'user/edituserheadimg'                  =>'编辑我的头像',
            'user/resetuserphone'                   =>'重设我的手机',
            'user/inputnewphonecode'                =>'新手机验证码',
            'user/resetusername'                    =>'重设我的名称',
            'user/useraddress'                      =>'我的地址信息',
            'user/edituseraddress'                  =>'设置我的地址',
            'user/authentication'                   =>'会员认证',
            //分销商
            'member/index'                          =>'分销商中心',
        ),
        'mobile'=> array(
            'index/index'                           =>'首页',
            'login/index'                           =>'登录',
            'login/register'                        =>'会员注册',
            //会员
            'user/index'                            =>'会员中心',
            'user/usermessage'                      =>'我的消息',
            'user/userinfo'                         =>'我的资料',
            'user/edituserheadimg'                  =>'编辑我的头像',
            'user/resetuserphone'                   =>'重设我的手机',
            'user/inputnewphonecode'                =>'新手机验证码',
            'user/resetusername'                    =>'重设我的名称',
            'user/useraddress'                      =>'我的地址信息',
            'user/edituseraddress'                  =>'设置我的地址',
            'user/authentication'                   =>'会员认证',
            //分销商
            'member/index'                          =>'分销商中心',
            'member/binddistributor'                =>'绑定分销商',
            'member/distributor'                    =>'分销商管理',
            'member/setdistributors'                =>'设置分销商',
            'order/index'                           =>'订单详情',
            'order/financial'                       =>'收益',
        )
    );
    $hcmp_title = $HCMP ? ($m == 'admin' ? '-共享充电线后台管理' : '-共享充电线') : '';
    return isset($app_list[$m][$cf]) ? $app_list[$m][$cf].$hcmp_title : ($m == 'admin' ? '-共享充电线后台管理' : '-共享充电线');
}
